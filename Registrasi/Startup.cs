﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Registrasi.Startup))]
namespace Registrasi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
