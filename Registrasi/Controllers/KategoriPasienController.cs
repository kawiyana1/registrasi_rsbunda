﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Registrasi.Entities;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;


namespace Registrasi.Controllers
{
    [Authorize(Roles = "Registrasi")]
    public class KategoriPasienController : Controller
    {
        // GET: KategoriPasien
        public ActionResult Index()
        {
            return View();
        }

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMmKategoriPasienBermasalah> proses = s.SIMmKategoriPasienBermasalah;
                    proses = proses.Where($"{nameof(SIMmKategoriPasienBermasalah.KategoriMasalah)}.Contains(@0)", filter[0]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PasienBermasalahViewModel>(x));
                    foreach (var x in m)
                    {
                        x.TglInput_View = x.TglInput.ToString("dd/MM/yyyy HH:mm");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new KategoriPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        
                        var m = IConverter.Cast<SIMmKategoriPasienBermasalah>(item);
                        m.KategoriMasalah = item.KategoriMasalah.ToUpper();
                        s.SIMmKategoriPasienBermasalah.Add(m);
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Kategori Pasien Create {m.KategoriMasalah}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            KategoriPasienViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMmKategoriPasienBermasalah.FirstOrDefault(x => x.KategoriMasalah == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<KategoriPasienViewModel>(m);
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post(string id)
        {
            try
            {
                var item = new KategoriPasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var model = s.SIMmKategoriPasienBermasalah.FirstOrDefault(x => x.KategoriMasalah == item.KategoriMasalah);
                        if (model == null) throw new Exception("Data Tidak ditemukan");

                        if (model.KategoriMasalah.ToUpper() != item.KategoriMasalah.ToUpper())
                        {
                            var has = s.SIMmKategoriPasienBermasalah.Where(x => x.KategoriMasalah == item.KategoriMasalah).Count() > 0;
                            if (has) throw new Exception("Nama sudah digunakan");
                        }

                        TryUpdateModel(model);
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Kategori Pasien Edit {model.KategoriMasalah}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== G E T  D A T A
        [HttpGet]
        public string GetEmoticon(string id)
        {
            string emoticon = "";
            using (var s = new SIM_Entities())
            {
                var m = s.SIMmKategoriPasienBermasalah.FirstOrDefault(x => x.KategoriMasalah == id);
                if (m != null)
                {
                    emoticon = m.Emoticon;
                }
            }
            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = emoticon
            });
        }
        #endregion

    }
}