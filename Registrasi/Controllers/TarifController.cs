﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Registrasi.Entities;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Registrasi.Controllers
{
    [Authorize(Roles = "Registrasi")]
    public class TarifController : Controller
    {
        #region ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== T A B L E
        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_Informasi_Tarif> proses = s.Pelayanan_Informasi_Tarif;
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_Tarif.JasaName)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_Tarif.JasaID)}.Contains(@0)", filter[5]);
                    if (filter[1] != null)
                        proses = proses.Where($"{nameof(Pelayanan_Informasi_Tarif.KategoriJasaName)}.Contains(@0)", filter[1]);
                    if (IFilter.F_int(filter[6]) != null)
                        proses = proses.Where($"{nameof(Pelayanan_Informasi_Tarif.KategoriID)}=@0", IFilter.F_int(filter[6]));
                    if (filter[7] != null)
                        proses = proses.Where($"{nameof(Pelayanan_Informasi_Tarif.JenisPasienID)}.Contains(@0)", filter[7]);
                    if (filter[8] != null)
                        proses = proses.Where($"{nameof(Pelayanan_Informasi_Tarif.KelasID)}.Contains(@0)", filter[8]);

                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<InformasiTarifViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Harga_View = x.Harga.Value.ToMoney();
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

    }
}