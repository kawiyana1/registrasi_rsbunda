﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Registrasi.Entities;
using Registrasi.Entities.BPJS;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Registrasi.Controllers
{
    [Authorize(Roles = "Registrasi")]
    public class RegistrasiRIController : Controller
    {
        // GET: RegistrasiRI
        public ActionResult Index()
        {
            string url = ConfigurationManager.AppSettings["UrlBridging"];
            ViewBag.Url = url;
            return View();
        }

        #region ===== T A B L E
        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<VW_Registrasi_WEBREGISTRASI> proses = s.VW_Registrasi_WEBREGISTRASI.Where(x => x.RawatInap == true);
                    if (filter[24] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[22]))
                        {
                            proses = proses.Where("TglReg >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[23]))
                        {
                            proses = proses.Where("TglReg <= @0", DateTime.Parse(filter[23]));
                        }

                    }
                    if (filter[25] == "1")
                        proses = proses.Where(x => x.StatusPeriksa == "Belum");
                    else if (filter[25] == "2")
                        proses = proses.Where(x => x.StatusPeriksa == "Sudah" || x.StatusPeriksa == "Sudah Periksa");
                    else if (filter[25] == "3")
                        proses = proses.Where(x => x.StatusPeriksa == "CO");
                    else if (filter[26] == "True")
                        proses = proses.Where(x => x.DibuatkanSEP == false);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(VW_Registrasi_WEBREGISTRASI.NoReg)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(VW_Registrasi_WEBREGISTRASI.SectionName)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(VW_Registrasi_WEBREGISTRASI.NRM)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(VW_Registrasi_WEBREGISTRASI.NamaPasien)}.Contains(@0)", filter[4]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(VW_Registrasi_WEBREGISTRASI.JenisKerjasama)}.Contains(@0)", filter[5]);
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(VW_Registrasi_WEBREGISTRASI.Alamat)}.Contains(@0)", filter[6]);
                    if (!string.IsNullOrEmpty(filter[14])) proses = proses.Where($"{nameof(VW_Registrasi_WEBREGISTRASI.Nama_Supplier)}.Contains(@0)", filter[14]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<RegistrasiViewModel>(x));
                    foreach (var x in m)
                    {
                        var pasienkhusus = s.Vw_PasienBermasalah.OrderByDescending(z => z.TglInput).FirstOrDefault(z => z.NRM == x.NRM);
                        if (pasienkhusus != null)
                        {
                            x.Emoticon = pasienkhusus.Emoticon;
                        }
                        x.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                        x.JamReg_View = x.JamReg.ToString("HH:mm");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== C R E A T E
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            string url = ConfigurationManager.AppSettings["UrlBridging"];
            ViewBag.Url = url;
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public ActionResult Create_Post()
        {
            var item = new RegistrasiRIInsertViewModel();
            var noReg = "";
            try
            {
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        if (item.JenisPasienKerjasamaID == 2 || item.JenisPasienKerjasamaID == 9)
                        {
                            if (item.NoKartu == null)
                            {
                                ModelState.AddModelError("", "Nomor Kartu tidak boleh kosong");
                                return View(item);
                            }
                            if (item.CompanyID == null)
                            {
                                ModelState.AddModelError("", "Customer Kerja Sama belum dipilih");
                                return View(item);
                            }
                        }

                        var insert = s.RegistrasiMandiri_Registrasi_RI_Insert(
                        item.NRM,
                        item.Dokter_ID,
                        item.SectionID,
                        DateTime.Now,
                        item.NoReservasi,
                        true,
                        "",
                        item.TitipKelas,
                        item.Reg_KelasPelayanan, // KdKelasAsal
                        item.Reg_KelasDitempati,      // KdKelasPelayanan
                        item.Reg_KelasPertanggungan,            // KdKelasPertanggungan
                        item.NoKamar,
                        item.NoBed,
                        "",
                        item.JenisPasienKerjasamaID,
                        item.CompanyID,
                        item.NoKartu,
                        item.Kelas,
                        item.Rujukan,
                        item.DokterPengirim_ID,
                        item.VendorID,
                        item.NoSEP,
                        item.PasienPanjer);

                        noReg = insert.ToList().FirstOrDefault().NoReg;

                        if (noReg == "Pasien Sedang Dirawat")
                        {
                            ModelState.AddModelError("", "Pasien Sedang Dirawat");
                            return View(item);
                        }
                        if (noReg == null)
                        {
                            ModelState.AddModelError("", "NoReg is Niul");
                            return View(item);
                        }

                        var update = s.RegistrasiMandiri_Registrasi_Insert_Update(
                        noReg,
                        item.PasienRS,
                        item.PenanggungNRM,
                        item.PenanggungNama,
                        item.PenanggungAlamat,
                        item.PenanggungHubungan,
                        item.PenanggungPhone,
                        "",
                        item.VendorID,
                        "",
                        item.DokterPengirim_ID,
                        item.PasienAsuransi,
                        item.AsuransiID,
                        item.AssCompany_ID,
                        item.PasienPaket,
                        item.PaketJasa_ID,
                        item.NoKartuAsuransi,
                        item.JenisPasienKerjasamaID,
                        item.CaraDatang,
                        item.ICD_ID,
                        false,
                        item.PasienKTP,
                        item.NoSEP,
                        item.PasienCovid
                        );

                        var rg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noReg);
                        var userWeb = User.Identity.GetUserId();
                        if (rg != null)
                        {
                            rg.Pribadi = item.PasienPribadi;
                            rg.PasienBayi = item.PasienBayi;
                            rg.UserIDWeb = userWeb;
                            s.SaveChanges();
                        }

                        result = new ResultSS(1, update);
                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Registrasi Create {noReg}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    if (result.IsSuccess)
                    {
                        if (!string.IsNullOrEmpty(item.NoSEP))
                        {
                            using (var ss = new BPJSEntities())
                            {
                                var m = ss.BPJStrSEPVClaim.FirstOrDefault(x => x.NoSEP == item.NoSEP);
                                if (m != null)
                                {
                                    m.NoReg = item.NoReg;
                                    m.Status = "SUDAH";
                                    ss.SaveChanges();
                                }
                            }
                        }

                        if (Request.Cookies["AutoPrintLabel"] != null)
                        {
                            if (bool.Parse(Request.Cookies["AutoPrintLabel"].Value))
                            {
                                var x = new ReportController();
                                x.PrintLabel(noReg, int.Parse(Request.Cookies["JumlahPrintLabel"].Value), Request);
                            }
                        }


                        return RedirectToAction("Detail", new { id = noReg });
                    }
                    else
                    {
                        ModelState.AddModelError("", result.Message);
                        return View(item);
                    }
                }
                else
                {
                    return View(item);
                }
            }
            catch (SqlException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(item);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(item);
            }
        }
        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            RegistrasiRIInsertViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var tujuan = s.Registrasi_GetListTujuan.FirstOrDefault(x => x.NoReg == id);
                    var m = s.VW_Registrasi_WEBREGISTRASI.FirstOrDefault(x => x.NoReg == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<RegistrasiRIInsertViewModel>(m);
                    item.PenanggungPhone = m.PenanggungTelp;
                    item.PasienPribadi = m.Pribadi;
                    item.JenisPasienKerjasama = m.JenisKerjasama;
                    item.NamaSection = tujuan.NamaSection;
                    item.Dokter = tujuan.NamaDokter;
                    item.Waktu = tujuan.NamaWaktu;
                    item.Tanggal = tujuan.Tanggal;
                    item.JenisKelamin = m.JenisKelamin == "M" ? "Male" : m.JenisKelamin == "F" ? "Female" : "Other";
                    item.Kelas = m.NamaKelas;
                    item.Reg_KelasDitempati = m.KdKelasAsal;
                    item.KelasPelayanan = m.KelasID;
                    item.Reg_KelasPertanggungan = m.KelasID;
                    item.SectionID = tujuan.SectionID;
                    item.Dokter_ID = tujuan.DokterID;
                    var reg = s.SIMtrRegistrasi.FirstOrDefault(r => r.NoReg == m.NoReg);
                    if(reg != null)
                    {
                        item.Reg_KelasPertanggungan = m.KdKelasPertanggungan;
                        item.Reg_KelasPelayanan = m.KdKelasAsal;
                        item.Reg_KelasDitempati = m.KdKelasAsal;
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
            string url = ConfigurationManager.AppSettings["UrlBridging"];
            ViewBag.Url = url;
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post(string id)
        {
            try
            {
                var item = new RegistrasiRIInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var userWeb = User.Identity.GetUserId();
                        var model = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.NoReg);
                        if (model == null) throw new Exception("Data Tidak ditemukan");
                        model.Rujukan = item.Rujukan;
                        model.Pribadi = item.PasienPribadi;
                        model.VendorID_Referensi = item.VendorID;
                        model.CaraDatang = item.CaraDatang;
                        model.PasienBayi = item.PasienBayi;
                        model.NoSEP = item.NoSEP;
                        model.UserIDWeb = userWeb;
                        TryUpdateModel(model);
                        result = new ResultSS(s.SaveChanges());

                        if (!string.IsNullOrEmpty(item.NoSEP))
                        {
                            using (var ss = new BPJSEntities())
                            {
                                var m = ss.BPJStrSEPVClaim.FirstOrDefault(x => x.NoSEP == item.NoSEP);
                                if (m != null)
                                {
                                    m.NoReg = item.NoReg;
                                    m.Status = "SUDAH";
                                    ss.SaveChanges();
                                }
                            }
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Registrasi Edit {model.NoReg}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E T A I L
        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string id)
        {
            var result = new RegistrasiRIInsertViewModel();
            using (var s = new SIM_Entities())
            {
                var model = s.VW_Registrasi_WEBREGISTRASI.FirstOrDefault(x => x.NoReg == id);
                var tujuan = s.Registrasi_GetListTujuan.FirstOrDefault(x => x.NoReg == id);
                result = IConverter.Cast<RegistrasiRIInsertViewModel>(model);
                result.SectionID = tujuan.SectionID;
                result.Dokter_ID = tujuan.DokterID;
                result.Dokter = tujuan.NamaDokter;
                result.NamaSection = tujuan.NamaSection;
                result.Waktu = tujuan.NamaWaktu;
                result.WaktuPraktek_ID = tujuan.WaktuID;
                result.Tanggal = tujuan.Tanggal;
                result.Tanggal_view = tujuan.Tanggal.ToString("dd/MM/yyyy");
            }

            var item = new SIMmKelas();
            if (result.KelasID == null)
            {
                item.NamaKelas = null;
            }
            else
            {

                using (var service = new SIM_Entities())
                {

                    item = service.SIMmKelas.FirstOrDefault(x => x.KelasID == result.KelasID);
                }

                result.Kelas = item.NamaKelas;
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(result);
            else
                return View(result);
        }

        #endregion

        #region ===== B A T A L

        [HttpPost]
        public string Batal(string id, string alasan)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var user = User.Identity.GetUserId();
                    var batal = s.BatalRegistrasiDenganAsalan(id, true, alasan);
                    result = new ResultSS(1, batal);
                    if (result.IsSuccess == true)
                    {
                        var reg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == id);
                        if (reg != null)
                        {
                            reg.UserIDWeb = user;
                            s.SaveChanges();
                        }
                    }
                }
                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                {
                    Activity = $"Registrasi RI Batal {id}"
                };
                UserActivity.InsertUserActivity(userActivity);

                return JsonHelper.JsonMsgCustom(result, "Dibatalkan");
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}