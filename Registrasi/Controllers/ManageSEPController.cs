﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Registrasi.Entities;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Registrasi.Controllers
{
    public class ManageSEPController : Controller
    {
        // GET: ManageSEP
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg)
        {
            var model = new ManageSEPViewModel();
            model.NoReg = noreg;
            using (var s = new SIM_Entities())
            {
                var regis = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                model.NRM = regis.NRM;
                model.NoKartu = regis.NoAnggota;
                model.SectionID = regis.SectionID;
            }

            string url = ConfigurationManager.AppSettings["UrlBridging"];
            ViewBag.Url = url;

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new ManageSEPViewModel();
                TryUpdateModel(item);

                if (item.NoSEP == null)
                {
                    return JsonHelper.JsonMsgError("No SEP tidak boleh kosong.");
                }

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var regis = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.NoReg);
                        regis.NoSEP = item.NoSEP;
                        result = new ResultSS(s.SaveChanges());
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}