﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Registrasi.Entities;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Registrasi.Controllers
{
    [Authorize(Roles = "Registrasi")]
    public class PasienKhususController : Controller
    {
        // GET: PasienBermasalah
        public ActionResult Index()
        {
            return View();
        }

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Vw_PasienBermasalah> proses = s.Vw_PasienBermasalah;
                    if (filter[24] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[22]))
                        {
                            proses = proses.Where("TglInput >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[23]))
                        {
                            proses = proses.Where("TglInput <= @0", DateTime.Parse(filter[23]).AddDays(1));
                        }
                    }
                    proses = proses.Where($"{nameof(Vw_PasienBermasalah.NRM)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(Vw_PasienBermasalah.NamaPasien)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(Vw_PasienBermasalah.KategoriMasalah)}.Contains(@0)", filter[2]);
                    proses = proses.Where($"{nameof(Vw_PasienBermasalah.DeskripsiMasalah)}.Contains(@0)", filter[3]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PasienBermasalahViewModel>(x));
                    foreach (var x in m)
                    {
                        x.TglInput_View = x.TglInput.ToString("dd/MM/yyyy HH:mm");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new PasienBermasalahViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var dt = s.SIMmDetailMasalahPasien.FirstOrDefault(x => x.NRM == item.NRM && x.KategoriMasalah == item.KategoriMasalah);
                        if (dt != null)
                        {
                            return JsonHelper.JsonMsgInfo("Pasien dengan kategori tersebut sudah ada");
                        }
                        var m = IConverter.Cast<SIMmDetailMasalahPasien>(item);
                        m.TglInput = DateTime.Now;
                        m.UserID = 490;
                        s.SIMmDetailMasalahPasien.Add(m);
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Pasien Khusus Create {m.NRM}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== EDIT
        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string nrm, string kategori)
        {
            var model = new PasienBermasalahViewModel();
            using (var s = new SIM_Entities())
            {
                var dt = s.SIMmDetailMasalahPasien.FirstOrDefault(x => x.NRM == nrm && x.KategoriMasalah == kategori);
                if (dt != null)
                {
                    model = IConverter.Cast<PasienBermasalahViewModel>(dt);
                    var dtReg = s.mPasien.FirstOrDefault(x => x.NRM == nrm);
                    if (dtReg != null)
                    {
                        model.NamaPasien = dtReg.NamaPasien;
                    }
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }


        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post()
        {
            try
            {
                var item = new PasienBermasalahViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var m = s.SIMmDetailMasalahPasien.FirstOrDefault(x => x.NRM == item.NRM && x.KategoriMasalah == item.KategoriMasalah);
                        if (m != null)
                        {
                            m.TglInput = DateTime.Now;
                            m.DeskripsiMasalah = item.DeskripsiMasalah;
                        }
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Pasien Khusus Edit {m.NRM}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== DELETE
        [HttpPost]
        public string Delete(string nrm, string kategori)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMmDetailMasalahPasien.FirstOrDefault(x => x.NRM == nrm && x.KategoriMasalah == kategori);
                    if (m == null) throw new Exception("Data Tidak ditemukan");
                    using (var dbContextTransaction = s.Database.BeginTransaction())
                    {
                        try
                        {
                            s.SIMmDetailMasalahPasien.Remove(m);
                            result = new ResultSS(s.SaveChanges());

                            var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                            {
                                Activity = $"Pasien Khusus delete {nrm} + {kategori}"
                            };
                            UserActivity.InsertUserActivity(userActivity);
                            dbContextTransaction.Commit();
                        }
                        catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                        catch (Exception ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                    }
                }
                return JsonHelper.JsonMsgDelete(result, -1);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

    }
}