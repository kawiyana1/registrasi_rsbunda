﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Registrasi.Entities;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Registrasi.Controllers
{
    public class JadwalKontrolController : Controller
    {
        #region ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Vw_JadwalSuratKontrolPasien> proses = s.Vw_JadwalSuratKontrolPasien;
                    if (filter[24] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[22]))
                        {
                            proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[23]))
                        {
                            proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[23]));
                        }

                    }
                    proses = proses.Where($"{nameof(Vw_JadwalSuratKontrolPasien.NoBukti)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(Vw_JadwalSuratKontrolPasien.NRM)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(Vw_JadwalSuratKontrolPasien.Nama)}.Contains(@0)", filter[2]);
                    proses = proses.Where($"{nameof(Vw_JadwalSuratKontrolPasien.Alamat)}.Contains(@0)", filter[3]);
                    proses = proses.Where($"{nameof(Vw_JadwalSuratKontrolPasien.SectionName)}.Contains(@0)", filter[4]);
                    proses = proses.Where($"{nameof(Vw_JadwalSuratKontrolPasien.Keterangan)}.Contains(@0)", filter[5]);
                    proses = proses.Where($"{nameof(Vw_JadwalSuratKontrolPasien.NamaDOkter)}.Contains(@0)", filter[6]);
                    if (filter[25] == "1")
                        proses = proses.Where(x => x.KedatanganPasien == false);
                    else if (filter[25] == "2")
                        proses = proses.Where(x => x.SudahReservasi == false && x.KedatanganPasien == true);
                    else if (filter[25] == "3")
                        proses = proses.Where(x => x.SudahReservasi == true);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<JadwalKontrolViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== V A L I D A S I

        [HttpPost]
        public string Validasi(string id)
        {
            try
            {
                ResultSS result;

                using (var s = new SIM_Entities())
                {
                    var jadwalkontrol = s.SIMtrFormKontrolPasien.FirstOrDefault(x=> x.NoBukti == id);
                    jadwalkontrol.KedatanganPasien = true;
                    result = new ResultSS(s.SaveChanges());
                }
                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                {
                    Activity = $"JadwalKontrol Validasi {id}"
                };
                UserActivity.InsertUserActivity(userActivity);

                return JsonHelper.JsonMsgCustom(result, "Divalidasi");
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}