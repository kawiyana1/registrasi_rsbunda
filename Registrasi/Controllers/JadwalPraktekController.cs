﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Registrasi.Entities;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Registrasi.Controllers
{
    public class JadwalPraktekController : Controller
    {

        #region ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== T A B L E
        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<JadwalPraktekDokter_GetData> proses = s.JadwalPraktekDokter_GetData.Where(x => x.StatusAktif == true
                    && (x.JadwalCancel == false || (x.JadwalCancel == true && x.DokterID != null))
                    && x.TipePelayanan == "RJ"
                    && x.DokterAktif == true
                    && x.JadwalVerified == true
                    );
                    if (filter[24] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[22]))
                        {
                            proses = proses.Where("Tanggal >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[23]))
                        {
                            proses = proses.Where("Tanggal <= @0", DateTime.Parse(filter[23]));
                        }

                    }
                    proses = proses.Where($"{nameof(JadwalPraktekDokter_GetData.NamaSection)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(JadwalPraktekDokter_GetData.NamaDOkter)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(JadwalPraktekDokter_GetData.SpesialisName)}.Contains(@0)", filter[2]);
                    proses = proses.Where($"{nameof(JadwalPraktekDokter_GetData.Hari)}.Contains(@0)", filter[3]);
                    proses = proses.Where($"{nameof(JadwalPraktekDokter_GetData.Keterangan)}.Contains(@0)", filter[5]);

                    if (!string.IsNullOrEmpty(filter[6]))
                    {
                        int jmlantrian = IFilter.F_int(filter[6]).Value;
                        proses = proses.Where(x=> x.JmlAntrian == jmlantrian);
                    }

                    if (!string.IsNullOrEmpty(filter[7]))
                    {
                        int antriansaatini = IFilter.F_int(filter[7]).Value;
                        proses = proses.Where(x => x.NoAntrianSaatIni == antriansaatini);
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<JadwalPraktekViewModel>(x));
                    foreach(var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                    }

                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

    }
}