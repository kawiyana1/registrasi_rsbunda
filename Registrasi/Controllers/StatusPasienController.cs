﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Registrasi.Entities;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Registrasi.Controllers
{
    [Authorize(Roles = "Registrasi")]
    public class StatusPasienController : Controller
    {
        #region ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Vw_Pelayanan_StatusPasien> proses = s.Vw_Pelayanan_StatusPasien;
                    if (filter[24] != "")
                    {
                        if (!string.IsNullOrEmpty(filter[22]))
                        {
                            proses = proses.Where("TglReservasi == @0", DateTime.Parse(filter[22]).Date);
                        }
                        if (!string.IsNullOrEmpty(filter[23]))
                        {
                            proses = proses.Where("TglReg == @0", DateTime.Parse(filter[23]).Date);
                        }

                    }
                    proses = proses.Where($"{nameof(Vw_Pelayanan_StatusPasien.NRM)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(Vw_Pelayanan_StatusPasien.NamaPasien)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(Vw_Pelayanan_StatusPasien.Alamat)}.Contains(@0)", filter[2]);
                    proses = proses.Where($"{nameof(Vw_Pelayanan_StatusPasien.JenisPasien)}.Contains(@0)", filter[3]);
                    proses = proses.Where($"{nameof(Vw_Pelayanan_StatusPasien.SectionName)}.Contains(@0)", filter[4]);
                    proses = proses.Where($"{nameof(Vw_Pelayanan_StatusPasien.Dokter)}.Contains(@0)", filter[5]);
                    if (filter[25] == "1")
                        proses = proses.Where(x => x.StatusReservasi == "Belum Registrasi");
                    else if (filter[25] == "2")
                        proses = proses.Where(x => (x.StatusReservasi == "Sudah Registrasi" || x.StatusReservasi == "Tanpa Reservasi") && x.StatusPeriksa != "Sudah Periksa" && x.StatusBayar != "Sudah Bayar");
                    else if (filter[25] == "3")
                        proses = proses.Where(x => x.StatusPeriksa == "Sudah Periksa");
                    else if (filter[25] == "4")
                        proses = proses.Where(x => x.StatusBayar == "Sudah Bayar");
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<StatusPasienViewModel>(x));
                    foreach (var x in m)
                    {
                        var tglRev = x.TglReservasi.ToString("dd/MM/yyyy");
                        x.TglReservasi_View = (tglRev == "01/01/1900") ? "" : tglRev;
                        x.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}