﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Registrasi.Entities;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using static Registrasi.Models.BpjsModel;

namespace Registrasi.Controllers
{
    [Authorize(Roles = "Registrasi")]
    public class PasienController : Controller
    {
        #region ====== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ====== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Vw_Pasien> proses = s.Vw_Pasien;
                    proses = proses.Where($"{nameof(Vw_Pasien.NRM)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(Vw_Pasien.NamaPasien)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2]))
                    {
                        proses = proses.Where($"{nameof(Vw_Pasien.NoIdentitas)}.Contains(@0)", filter[2]);
                    }
                    proses = proses.Where($"{nameof(Vw_Pasien.Alamat)}.Contains(@0)", filter[5]);
                    if (!string.IsNullOrEmpty(filter[6]))
                    {
                        proses = proses.Where($"{nameof(Vw_Pasien.Phone)}.Contains(@0)", filter[6]);
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PasienViewModel>(x));
                    foreach (var x in m)
                    {
                        var pasienkhusus = s.Vw_PasienBermasalah.OrderByDescending(z => z.TglInput).FirstOrDefault(z => z.NRM == x.NRM);
                        if (pasienkhusus != null)
                        {
                            x.Emoticon = pasienkhusus.Emoticon;
                        }
                        x.TglLahir_View = x.TglLahir.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ====== C R E A T E
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string ajax)
        {

            var item = new PasienViewModel();
            item.TglInput = DateTime.Today;
            ViewBag.Ajax = ajax;
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new PasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        if(item.typeNRM == "0")
                        {
                            item.NRM = s.GetNRMNew().FirstOrDefault();
                        }
                        else
                        {
                            if (item.NRM != null)
                            {
                                var pasien = s.mPasien.FirstOrDefault(x => x.NRM == item.NRM);
                                if (pasien != null)
                                {
                                    return JsonHelper.JsonMsgInfo("NRM sudah digunakan pasien : " + pasien.NamaPasien, false);
                                }
                            }
                            else
                            {
                                return JsonHelper.JsonMsgInfo("Nomor NRM tidak boleh kosong");
                            }
                        }

                        if(item.TglLahir > DateTime.Today)
                        {
                            return JsonHelper.JsonMsgInfo("Tanggal lahir tidak boleh lebih dari hari ini.");
                        }

                        if (!string.IsNullOrEmpty(item.CompanyID))
                        {
                            var ckid = Int32.Parse(item.CompanyID);
                            var customer = s.VW_CustomerKerjasama.FirstOrDefault(x => x.CustomerKerjasamaID == ckid);
                            if (customer != null)
                            {
                                item.CompanyID = customer.Kode_Customer;
                                item.CustomerKerjasamaID = customer.CustomerKerjasamaID;
                            }
                        }
                        var insert = s.Registrasi_mPasien_Insert(
                            item.NRM,
                            item.NamaPasien,
                            item.NoIdentitas,
                            item.JenisKelamin,
                            item.TglLahir,
                            item.TglLahirDiketahui,
                            item.UmurSaatInput,
                            item.Pekerjaan,
                            item.Alamat,
                            item.PropinsiID,
                            item.KabupatenID,
                            item.KecamatanID,
                            item.DesaID,
                            item.BanjarID,
                            item.Phone,
                            item.Email,
                            item.JenisPasien,
                            item.JenisKerjasamaID,
                            item.AnggotaBaru,
                            item.CustomerKerjasamaID,
                            item.CompanyID,
                            item.NoKartu,
                            item.Klp,
                            item.JabatanDiPerusahaan,
                            item.EtnisID,
                            item.NationalityID,
                            item.PasienVVIP,
                            item.PasienKTP,
                            DateTime.Now,
                            item.UserID,
                            item.CaraDatangPertama,
                            item.DokterID_ReferensiPertama,
                            item.SedangDirawat,
                            item.TglRegKasusKecelakaanBaru,
                            item.NoRegKecelakaanBaru,
                            item.Aktive_Keanggotaan,
                            item.Agama,
                            item.NoKartu,
                            item.NamaAnggotaE,
                            item.GenderAnggotaE,
                            item.TglTidakAktif,
                            item.TipePasienAsal,
                            item.NoKartuAsal,
                            item.NamaPerusahaanAsal,
                            item.PenanggungIsPasien,
                            item.PenanggungNRM,
                            item.PenanggungNama,
                            item.PenanggungAlamat,
                            item.Phone,
                            item.PenanggungKTP,
                            item.PenanggungHubungan,
                            true,
                            item.NamaIbuKandung,
                            item.NonPBI,
                            item.KdKelas,
                            item.Pendidikan,
                            item.Status,
                            item.TempatLahir,
                            item.StatusKawin,
                            item.Golda
                            );
                        result = new ResultSS(1, insert);

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Pasien Create {item.NRM.ToString()}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    result.Data = item.NRM;
                    return new JavaScriptSerializer().Serialize(new ResultStatus()
                    {
                        Data = result.Data,
                        IsSuccess = result.IsSuccess,
                        Message = "Berhasil disimpan",
                        Count = 1,
                        Status = "success"
                    });
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ====== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            PasienViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Vw_Pasien.FirstOrDefault(x => x.NRM == id);
                    var mpasien = s.mPasien.FirstOrDefault(x => x.NRM == id);
                    if (m == null) return HttpNotFound();
                    if (mpasien == null) return HttpNotFound();
                    item = IConverter.Cast<PasienViewModel>(mpasien);
                    item.PropinsiID = m.Propinsi_ID;
                    item.NamaPropinsi = m.Nama_Propinsi;
                    item.KabupatenID = m.Kode_Kabupaten;
                    item.NamaKabupaten = m.Nama_Kabupaten;
                    item.KecamatanID = m.KecamatanID;
                    item.NamaKecamatan = m.KecamatanNama;
                    item.NamaDesa = m.DesaNama;
                    item.NamaPerusahaan = m.NamaPerusahaan;
                    item.Klp = m.Klp;
                    item.Golda = m.GoldaID;
                    item.StatusKawin = m.StatusKawinID;
                    item.NamaPerusahaan = m.Nama_Customer;
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post(string id)
        {
            try
            {
                var item = new PasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        if (!string.IsNullOrEmpty(item.CompanyID))
                        {
                            var customer = s.VW_CustomerKerjasama.FirstOrDefault(x => x.Kode_Customer == item.CompanyID);
                            if (customer != null)
                            {
                                item.CustomerKerjasamaID = customer.CustomerKerjasamaID;
                            }
                        }
                        if (item.NoIdentitas == null) return JsonHelper.JsonMsgError(new Exception("No Identitas tidak boleh kosong"));
                        
                        if (item.Agama == null) return JsonHelper.JsonMsgError(new Exception("Agama tidak boleh kosong"));
                        
                        if (item.StatusKawin == null) return JsonHelper.JsonMsgError(new Exception("Status Kawin tidak boleh kosong"));
                       
                        if (item.JenisKelamin == null) return JsonHelper.JsonMsgError(new Exception("Jenis Kelamin tidak boleh kosong"));

                        if (item.DesaID == null) return JsonHelper.JsonMsgError(new Exception("Desa tidak boleh kosong"));

                        if (item.Pekerjaan == null) return JsonHelper.JsonMsgError(new Exception("Pekerjaan tidak boleh kosong"));

                        var update = s.Registrasi_mPasien_Update(
                            item.NRM,
                            item.NamaPasien,
                            item.NoIdentitas,
                            item.JenisKelamin,
                            item.TglLahir,
                            item.TglLahirDiketahui,
                            item.UmurSaatInput,
                            item.Pekerjaan,
                            item.Alamat,
                            item.PropinsiID,
                            item.KabupatenID,
                            item.KecamatanID,
                            item.DesaID,
                            item.BanjarID,
                            item.Phone,
                            item.Email,
                            item.JenisPasien,
                            item.JenisKerjasamaID,
                            item.AnggotaBaru,
                            item.CustomerKerjasamaID,
                            item.CompanyID,
                            item.NoKartu,
                            item.Klp,
                            item.JabatanDiPerusahaan,
                            item.EtnisID,
                            item.NationalityID,
                            item.PasienVVIP,
                            item.PasienKTP,
                            DateTime.Now,
                            item.UserID,
                            item.CaraDatangPertama,
                            item.DokterID_ReferensiPertama,
                            item.SedangDirawat,
                            item.TglRegKasusKecelakaanBaru,
                            item.NoRegKecelakaanBaru,
                            item.Aktive_Keanggotaan,
                            item.Agama,
                            item.NoKartu,
                            item.NamaAnggotaE,
                            item.GenderAnggotaE,
                            item.TglTidakAktif,
                            item.TipePasienAsal,
                            item.NoKartuAsal,
                            item.NamaPerusahaanAsal,
                            item.PenanggungIsPasien,
                            item.PenanggungNRM,
                            item.PenanggungNama,
                            item.PenanggungAlamat,
                            item.Phone,
                            item.PenanggungKTP,
                            item.PenanggungHubungan,
                            true,
                            item.NamaIbuKandung,
                            item.NonPBI,
                            item.KdKelas,
                            item.Pendidikan,
                            item.Status,
                            item.TempatLahir,
                            item.StatusKawin,
                            item.Golda
                            );
                        result = new ResultSS(1, update);

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Pasien Edit {item.NRM}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== P A S I E N  B E R M A S A L A H
        [HttpGet]
        public ActionResult PasienBermasalah(string id)
        {
            var item = new PasienBermasalahViewModel();
            try
            {
                using (var s = new SIM_Entities())
                {
                    var models = s.Vw_PasienBermasalah.Where(x => x.NRM == id);
                    if (models == null) return HttpNotFound();
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PasienBermasalahViewModel>(x));
                    foreach (var x in m)
                    {
                        x.TglInput_View = x.TglInput.ToString("dd/MM/yyyy HH:mm");
                    }
                    item.PasienBermasalahDetail = new List<PasienBermasalahViewModel>();
                    item.PasienBermasalahDetail = m;
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion

        #region ====== A D D I T I O N A L
        [HttpPost]
        public string ListDetailPekerjaan(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMmPekerjaan> proses = s.SIMmPekerjaan;
                    proses = proses.Where($"{nameof(SIMmPekerjaan.NamaPekerjaan)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<PekerjaanViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListDetailPerusahaan(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<VW_CustomerKerjasama> proses = s.VW_CustomerKerjasama;
                    proses = proses.Where($"{nameof(VW_CustomerKerjasama.JenisKerjasama)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(VW_CustomerKerjasama.Nama_Customer)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<CustomerViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListDetailAnggota(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    decimal customerID = IFilter.F_Decimal(filter[1]).Value;
                    IQueryable<SIMdAnggotaKerjasama> proses = s.SIMdAnggotaKerjasama;
                    proses = proses.Where($"{nameof(SIMdAnggotaKerjasama.Nama)}.Contains(@0)", filter[0]);
                    proses = proses.Where(x => x.CustomerKerjasamaID == customerID);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<AnggotaViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListDetailDokterPertama(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mSupplier> proses = s.mSupplier;
                    proses = proses.Where($"{nameof(mSupplier.Nama_Supplier)}.Contains(@0)", filter[0]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<DokterPertamaViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public string CheckPasien(string nama, string tgllahir)
        {
            using (var service = new SIM_Entities())
            {
                var tgl = new DateTime();
                if (!string.IsNullOrEmpty(tgllahir))
                {
                    tgl = IFilter.F_DateTime(tgllahir).Value;
                }
                else
                {
                    tgl = DateTime.Today;
                }

                var p = service.mPasien.FirstOrDefault(x => x.NamaPasien == nama && x.TglLahir == tgl);
                if (p != null)
                    return JsonConvert.SerializeObject(new ResultSS
                    {
                        IsSuccess = false,
                        Message = "Pasien Sudah Pernah Terdaftar",
                        Data = "PasienTerdaftar"
                    });
            }

            return JsonConvert.SerializeObject(new ResultSS { IsSuccess = true });
        }
        #endregion

        #region ====== U B A H  S T A T U S  P A S I E N
        public string StatusPasien(string id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var y = s.mPasien.FirstOrDefault(x => x.NRM == id);
                        y.SedangDirawat = false;
                        result = new ResultSS(s.SaveChanges());
                    }
                    return JsonHelper.JsonMsgEdit(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== C H E C K  P E R S E R T A  B P J S
        [HttpPost]
        public string CheckPeserta(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                    return JsonHelper.JsonMsgError(new Exception("data harus lengkap"));
                var s = new BpjsSEPService();
                PesertaNoKartuBPJSModel model;
                var item = new PasienViewModel();
                model = s.PesertaNoKartuBPJS(id);
                if (model.response == null)
                    return JsonHelper.JsonMsgError(new Exception("Data Tidak Ditemukan"));
                if (model.metaData.code != "200")
                    return JsonHelper.JsonMsgError(new Exception(model.metaData.message));
                if (model.response.peserta.statusPeserta.keterangan != "AKTIF")
                    return JsonHelper.JsonMsgError(new Exception(model.response.peserta.statusPeserta.keterangan));

                using (var service = new SIM_Entities())
                {
                    var kelas = service.SIMmKelas.FirstOrDefault(x => x.MappingBPJS == model.response.peserta.hakKelas.kode);
                    if (kelas != null)
                    {
                        item.KdKelas = kelas.KelasID;
                    }

                    var customer = service.VW_CustomerKerjasamaAnggota.FirstOrDefault(x => x.KelasID == item.KdKelas);
                    if (customer != null)
                    {
                        item.CompanyID = customer.CustomerID.ToString();
                        item.KodePerusahaan = customer.Kode_Customer;
                        item.NamaPerusahaan = customer.Nama_Customer + " - " + customer.NamaKelas;
                        item.CustomerKerjasamaID = customer.CustomerKerjasamaID;
                    }

                }
                item.TglLahir_View = model.response.peserta.tglLahir.ToString("yyyy-MM-dd");
                item.NamaPasien = model.response.peserta.nama;
                item.NoAnggotaE = model.response.peserta.noKartu;
                item.NamaAnggotaE = model.response.peserta.nama;
                item.NoIdentitas = model.response.peserta.nik;
                item.GenderAnggotaE = model.response.peserta.sex == "L" ? "M" : model.response.peserta.sex == "P" ? "F" : "O";
                item.JenisKelamin = model.response.peserta.sex == "L" ? "M" : model.response.peserta.sex == "P" ? "F" : "O";

                var result = new
                {
                    IsSuccess = true,
                    Data = item
                };
                return JsonConvert.SerializeObject(result);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        public string CekNomorAnggota(string id)
        {
            var s = new SIM_Entities();
            var na = s.SIMdAnggotaKerjasama.Where(x => x.NoAnggota == id);
            if (na.Count() > 0)
            {
                return new JavaScriptSerializer().Serialize(new ResultStatus()
                {
                    Data = "1"
                });
            }
            else
            {
                return new JavaScriptSerializer().Serialize(new ResultStatus()
                {
                    Data = "0"
                });
            }
        }
        #endregion

        #region ===== E L I G I B I L I T A S  P A S I E N
        [HttpGet]
        [ActionName("Eligibilitas")]
        public ActionResult Eligibilitas_Get(string ajax)
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }
        [HttpPost]
        [ActionName("Eligibilitas")]
        public string Eligibilitas_Post(string id, string tipe)
        {
            try
            {
                if (string.IsNullOrEmpty(id)) return JsonHelper.JsonMsgError(new Exception("data harus lengkap"));

                var s = new BpjsSEPService();
                var item = new PasienViewModel();

                if(tipe == "NIK")
                {
                    var model = new PesertaNIKModel();
                    model = s.PesertaNIK(id);

                    if (model.response == null)
                        return JsonHelper.JsonMsgError(new Exception("Data Tidak Ditemukan"));
                    if (model.metaData.code != "200")
                        return JsonHelper.JsonMsgError(new Exception(model.metaData.message));

                    using (var service = new SIM_Entities())
                    {
                        var kelas = service.SIMmKelas.FirstOrDefault(x => x.MappingBPJS == model.response.peserta.hakKelas.kode);
                        if (kelas != null)
                        {
                            item.KdKelas = kelas.NamaKelas;
                        }

                        var customer = service.VW_CustomerKerjasamaAnggota.FirstOrDefault(x => x.KelasID == item.KdKelas);
                        if (customer != null)
                        {
                            item.CompanyID = customer.Kode_Customer;
                            item.NamaPerusahaan = customer.Nama_Customer;
                            item.CustomerKerjasamaID = customer.CustomerKerjasamaID;
                        }

                    }
                    item.Status = model.response.peserta.statusPeserta.keterangan;
                    item.TglLahir_View = model.response.peserta.tglLahir.ToString("yyyy-MM-dd");
                    item.NamaPasien = model.response.peserta.nama;
                    item.NoAnggotaE = model.response.peserta.noKartu;
                    item.NamaAnggotaE = model.response.peserta.nama;
                    item.NoIdentitas = model.response.peserta.nik;
                    item.GenderAnggotaE = model.response.peserta.sex == "L" ? "M" : model.response.peserta.sex == "P" ? "F" : "O";
                    item.JenisKelamin = model.response.peserta.sex == "L" ? "M" : model.response.peserta.sex == "P" ? "F" : "O";
                }
                else
                {
                    var model = new PesertaNoKartuBPJSModel();
                    model = s.PesertaNoKartuBPJS(id);

                    if (model.response == null)
                        return JsonHelper.JsonMsgError(new Exception("Data Tidak Ditemukan"));
                    if (model.metaData.code != "200")
                        return JsonHelper.JsonMsgError(new Exception(model.metaData.message));
                    using (var service = new SIM_Entities())
                    {
                        var kelas = service.SIMmKelas.FirstOrDefault(x => x.MappingBPJS == model.response.peserta.hakKelas.kode);
                        if (kelas != null)
                        {
                            item.KdKelas = kelas.NamaKelas;
                        }

                        var customer = service.VW_CustomerKerjasamaAnggota.FirstOrDefault(x => x.KelasID == item.KdKelas);
                        if (customer != null)
                        {
                            item.CompanyID = customer.Kode_Customer;
                            item.NamaPerusahaan = customer.Nama_Customer;
                            item.CustomerKerjasamaID = customer.CustomerKerjasamaID;
                        }

                    }
                    item.Status = model.response.peserta.statusPeserta.keterangan;
                    item.TglLahir_View = model.response.peserta.tglLahir.ToString("yyyy-MM-dd");
                    item.NamaPasien = model.response.peserta.nama;
                    item.NoAnggotaE = model.response.peserta.noKartu;
                    item.NamaAnggotaE = model.response.peserta.nama;
                    item.NoIdentitas = model.response.peserta.nik;
                    item.GenderAnggotaE = model.response.peserta.sex == "L" ? "M" : model.response.peserta.sex == "P" ? "F" : "O";
                    item.JenisKelamin = model.response.peserta.sex == "L" ? "M" : model.response.peserta.sex == "P" ? "F" : "O";
                }

                var result = new
                {
                    IsSuccess = true,
                    Data = item
                };
                return JsonConvert.SerializeObject(result);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== C H E C K  M A N U A L  N R M
        [HttpPost]
        public string check_manual_nrm(string nrm)
        {
            try
            {
                var item = new PasienViewModel();
                item.NRM = nrm;

                if (ModelState.IsValid)
                {
                    using (var s = new SIM_Entities())
                    {
                        if (item.NRM != null)
                        {
                            var pasien = s.mPasien.FirstOrDefault(x => x.NRM == item.NRM);
                            if (pasien != null)
                            {
                                return JsonHelper.JsonMsgInfo("NRM sudah digunakan pasien : " + pasien.NamaPasien, false);
                            }
                            else
                            {
                                return JsonHelper.JsonMsgInfo("Nomor NRM dapat digunakan", true);

                            }
                        }
                        else
                        {
                            return JsonHelper.JsonMsgInfo("Nomor NRM tidak boleh kosong", false);
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

    }
}