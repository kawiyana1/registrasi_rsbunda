﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Registrasi.Entities;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Registrasi.Controllers
{
    [Authorize(Roles = "Registrasi")]
    public class KunjunganPasienController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        #region ====== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Vw_Pasien> proses = s.Vw_Pasien;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Vw_Pasien.NRM)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Vw_Pasien.NamaPasien)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Vw_Pasien.NoIdentitas)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(Vw_Pasien.Alamat)}.Contains(@0)", filter[5]);
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(Vw_Pasien.Phone)}.Contains(@0)", filter[6]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PasienViewModel>(x));
                    foreach (var x in m)
                    {
                        x.TglLahir_View = x.TglLahir.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E T A I L
        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string id)
        {
            var item = new PasienViewModel();

            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.VW_Pasien_Regional.FirstOrDefault(x => x.NRM == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<PasienViewModel>(m);
                    var tipepasien = s.SIMmJenisKerjasama.FirstOrDefault(x => x.JenisKerjasamaID == m.JenisKerjasamaID);
                    if (tipepasien != null)
                    {
                        item.JenisKerjasama = tipepasien.JenisKerjasama;
                    }
                    var registrasi = s.SIMtrRegistrasi.OrderByDescending(x => x.TglReg).FirstOrDefault(x => x.NRM == id);
                    if(registrasi != null)
                    {
                        var dokter = s.mSupplier.FirstOrDefault(x => x.Kode_Supplier == registrasi.VendorID_Referensi);
                        if(dokter != null)
                        {
                            item.RujukanDari = dokter.Nama_Supplier;
                        }
                    }
                    item.DetailKunjungan_List = new ListDetail<KunjunganViewModel>();
                    var d_Kunjungan = s.Vw_StatusKunjunganPasien.Where(x => x.NRM == id).ToList();
                    foreach (var x in d_Kunjungan)
                    {
                        var y = IConverter.Cast<KunjunganViewModel>(x);
                        y.TglTerakhirKunjungan_View = y.TglTerakhirKunjungan.Value.ToString("dd/MM/yyyy");
                        item.DetailKunjungan_List.Add(false, y);
                    }

                    item.DetailRegistrasi_List = new ListDetail<DetailRegistrasiViewModel>();
                    var d_Registrasi = s.SIMtrRegistrasi.Where(x => x.NRM == id && x.Batal == false).ToList();
                    foreach (var x in d_Registrasi)
                    {
                        var y = IConverter.Cast<DetailRegistrasiViewModel>(x);
                        y.Jam = x.JamReg.ToString("hh:mm");
                        y.Tanggal = x.TglReg.ToString("dd/MM/yyyy");
                        item.DetailRegistrasi_List.Add(false, y);

                    }
                }


            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion
    }
}