﻿using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Registrasi.Entities;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Registrasi.Controllers
{
    [Authorize(Roles = "Registrasi")]
    public class AnggotaController : Controller
    {
        #region ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== C R E A T E
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string nrm, string namapasien, string perusahaan)
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.NRM = nrm;
            ViewBag.perusahaan = perusahaan;
            ViewBag.NamaPasien = namapasien;
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("Create")]
        public string Create_Post()
        {
            try
            {
                var item = new AnggotaInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var model = s.SIMdAnggotaKerjasama.FirstOrDefault(x => x.NoAnggota == item.NoKartu);
                        if (model != null)
                        {
                            return new JavaScriptSerializer().Serialize(new ResultStatus()
                            {
                                IsSuccess = false,
                                Message = "No Kartu Sudah Tersimpan"
                            });
                        }
                        else
                        {
                            var insert = s.Registrasi_AnggotaKerjasama_Create(
                            item.CustomerKerjasamaID,
                            item.NoKartu,
                            item.NamaPasien,
                            true,
                            item.Klp,
                            null,
                            null,
                            item.NRM_Anggota,
                            null,
                            null
                            );

                            var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                            {
                                Activity = $"Anggota Baru Create {item.NoKartu}"
                            };
                            UserActivity.InsertUserActivity(userActivity);

                            result = new ResultSS(1, insert);
                        }

                    }

                    return new JavaScriptSerializer().Serialize(new ResultStatus()
                    {
                        Data = new string[6] { item.NoKartu, item.KodePerusahaanAsal, item.NamaPerusahaanAsal, item.Klp, item.KelasID, item.NamaPasien },
                        IsSuccess = result.IsSuccess
                    });
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== E D I T
        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string nomor)
        {
            var model = new AnggotaInsertViewModel();
            using (var s = new SIM_Entities())
            {
                var r = s.SIMdAnggotaKerjasama.FirstOrDefault(x => x.NoAnggota == nomor);
                if(r != null)
                {
                    model.NRM_Anggota = r.NRM;
                    model.NamaPasien = r.Nama;
                    model.CustomerKerjasamaID = r.CustomerKerjasamaID;
                    var customer = s.VW_CustomerKerjasama.FirstOrDefault(x => x.CustomerKerjasamaID == r.CustomerKerjasamaID);
                    if (customer != null)
                    {
                        model.KodePerusahaanAsal = customer.Kode_Customer;
                        model.NamaPerusahaanAsal = customer.Nama_Customer;
                        model.KodePerusahaan = customer.Kode_Customer;
                        model.NamaPerusahaan = customer.Nama_Customer;
                    }
                    model.Klp = r.Klp;
                    model.NoKartu = r.NoAnggota;
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            ViewBag.NRM = model.NRM_Anggota;
            ViewBag.perusahaan = model.KodePerusahaan;
            ViewBag.NamaPasien = model.NamaPasien;
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Edit")]
        public string Edit_Post()
        {
            try
            {
                var item = new AnggotaInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var model = s.SIMdAnggotaKerjasama.FirstOrDefault(x => x.NoAnggota == item.NoKartu);
                        if (model != null)
                        {
                            model.Nama = item.NamaPasien;
                            model.CustomerKerjasamaID = item.CustomerKerjasamaID;
                            model.Klp = item.Klp;
                            var insert = s.SaveChanges();

                            var customer = s.VW_CustomerKerjasama.FirstOrDefault(x => x.CustomerKerjasamaID == item.CustomerKerjasamaID);
                            if (customer != null)
                            {
                                item.KelasID = customer.KelasID;
                            }

                            var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                            {
                                Activity = $"Anggota Edit {item.NoKartu}"
                            };
                            UserActivity.InsertUserActivity(userActivity);

                            result = new ResultSS(1, insert);
                        }
                        else
                        {
                            return new JavaScriptSerializer().Serialize(new ResultStatus()
                            {
                                IsSuccess = false,
                                Message = "Data angggota tidak ditemukan."
                            });
                        }

                    }

                    return new JavaScriptSerializer().Serialize(new ResultStatus()
                    {
                        Data = new string[6] { item.NoKartu, item.KodePerusahaanAsal, item.NamaPerusahaanAsal, item.Klp, item.KelasID, item.NamaPasien },
                        IsSuccess = result.IsSuccess
                    });
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

    }
}