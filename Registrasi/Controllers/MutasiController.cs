﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Registrasi.Entities;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Registrasi.Controllers
{
    [Authorize(Roles = "Registrasi")]
    public class MutasiController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Registrasi_MutasiRawatInap_GetData> proses = s.Registrasi_MutasiRawatInap_GetData;

                    if (filter[24] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[22]))
                        {
                            proses = proses.Where("TglReg >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[23]))
                        {
                            proses = proses.Where("TglReg <= @0", DateTime.Parse(filter[23]));
                        }

                    }
                    if (filter[25] == "1") { 
                        proses = proses.Where($"RawatInap=@0 AND SectionName!=@1", true, "");
                    }
                    else if (filter[25] == "0") { 
                        proses = proses.Where($"RawatInap=@0 OR SectionName=@1", false, "");
                    }

                    if(filter[0] != "") proses = proses.Where($"{nameof(Registrasi_MutasiRawatInap_GetData.NoReg)}.Contains(@0)", filter[0]);
                    if(filter[3] != "") proses = proses.Where($"{nameof(Registrasi_MutasiRawatInap_GetData.SectionName)}.Contains(@0)", filter[3]);
                    if(filter[4] != "") proses = proses.Where($"{nameof(Registrasi_MutasiRawatInap_GetData.NRM)}.Contains(@0)", filter[4]);
                    if(filter[5] != "") proses = proses.Where($"{nameof(Registrasi_MutasiRawatInap_GetData.NamaPasien)}.Contains(@0)", filter[5]);
                    if(filter[6] != "") proses = proses.Where($"{nameof(Registrasi_MutasiRawatInap_GetData.JenisKerjasama)}.Contains(@0)", filter[6]);
                    if(filter[7] != "") proses = proses.Where($"{nameof(Registrasi_MutasiRawatInap_GetData.StatusBayar)}.Contains(@0)", filter[7]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<MutasiViewModel>(x));
                    foreach (var x in m)
                    {
                        x.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                        x.JamReg_View = x.JamReg.Value.ToString("HH:mm");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== C R E A T E
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg)
        {
            var model = new MutasiViewModel();
            using (var s = new SIM_Entities())
            {
                var m = s.VW_Registrasi_WEBREGISTRASI.FirstOrDefault(x => x.NoReg == noreg);
                if (m != null)
                {
                    model = IConverter.Cast<MutasiViewModel>(m);
                    model.SectionAsalID = (m.SectionID == null || m.SectionID == "") ? "SEC001" : m.SectionID;
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            var item = new MutasiViewModel();
            try
            {
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var model = s.VW_Registrasi_WEBREGISTRASI.FirstOrDefault(x => x.NoReg == item.NoReg);

                        var SectionAsalID = (model.SectionID == null || model.SectionID == "") ? "SEC000" : model.SectionID;

                        var insert = s.Pelayanan_SIMtrDataRegPasien_Insert(
                            item.NoReg,
                            model.TglReg,
                            model.JamReg,
                            model.JenisKerjasamaID,
                            SectionAsalID,
                            item.SectionID,
                            item.KelasID,
                            item.KelasPelayanan,
                            item.Titip,
                            item.Dokter_ID,
                            1,
                            false,
                            item.NoKamar,
                            item.NoBed,
                            true,
                            DateTime.Now,
                            false,
                            "",
                            model.UmurThn,
                            model.UmurBln,
                            model.UmurHr,
                            item.RawatGabung,
                            false,
                            null,
                            true,
                            "", 
                            item.NoSep
                            );

                        var Reg = s.SIMtrRegistrasi.FirstOrDefault(e => e.NoReg == item.NoReg);
                        Reg.KdKelasPertanggungan = item.KelasPelayanan;
                        Reg.KdKelasAsal = item.KelasID;
                        Reg.KdKelas = item.KelasDitempati;
                        Reg.TitipKelas = item.Titip;
                        s.SaveChanges();

                        result = new ResultSS(1, insert);
                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Mutasi Create {item.NoReg}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion
    }
}