﻿using iHos.MVC.Property;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Registrasi.Controllers
{
    [Authorize(Roles = "Registrasi")]
    public class SettingController : Controller
    {
        public ActionResult Index()
        {
            var item = new SettingViewModel();
            try
            {
                item = new SettingViewModel()
                {
                    IPKartu = (Request.Cookies["IPKartu"] == null ? "" : Request.Cookies["IPKartu"].Value),
                    PortKartu = (Request.Cookies["PortKartu"] == null ? "" : Request.Cookies["PortKartu"].Value),
                    IPLabel = (Request.Cookies["IPLabel"] == null ? "" : Request.Cookies["IPLabel"].Value),
                    PortLabel = (Request.Cookies["PortLabel"] == null ? "" : Request.Cookies["PortLabel"].Value),
                    IPTracer = (Request.Cookies["IPTracer"] == null ? "" : Request.Cookies["IPTracer"].Value),
                    PortTracer = (Request.Cookies["PortTracer"] == null ? "" : Request.Cookies["PortTracer"].Value),
                    IPTracerRM = (Request.Cookies["IPTracerRM"] == null ? "" : Request.Cookies["IPTracerRM"].Value),
                    PortTracerRM = (Request.Cookies["PortTracerRM"] == null ? "" : Request.Cookies["PortTracerRM"].Value),
                    IPGelangD = (Request.Cookies["IPGelangD"] == null ? "" : Request.Cookies["IPGelangD"].Value),
                    PortGelangD= (Request.Cookies["PortGelangD"] == null ? "" : Request.Cookies["PortGelangD"].Value),
                    IPGelangB = (Request.Cookies["IPGelangB"] == null ? "" : Request.Cookies["IPGelangB"].Value),
                    PortGelangB = (Request.Cookies["PortGelangB"] == null ? "" : Request.Cookies["PortGelangB"].Value),
                    AutoPrintKartu = (Request.Cookies["AutoPrintKartu"] == null ? false : bool.Parse(Request.Cookies["AutoPrintKartu"].Value)),
                    AutoPrintLabel = (Request.Cookies["AutoPrintLabel"] == null ? false : bool.Parse(Request.Cookies["AutoPrintLabel"].Value)),
                    AutoPrintTracer= (Request.Cookies["AutoPrintTracer"] == null ? false : bool.Parse(Request.Cookies["AutoPrintTracer"].Value)),
                    JumlahPrintLabel = (Request.Cookies["JumlahPrintLabel"] == null ? 0 : int.Parse(Request.Cookies["JumlahPrintLabel"].Value)),
                    JumlahPrintGelang = (Request.Cookies["JumlahPrintGelang"] == null ? 0 : int.Parse(Request.Cookies["JumlahPrintGelang"].Value))
                };
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            return View(item);
        }

        [HttpPost]
        [ActionName("Index")]
        public string Index_POST()
        {
            var result = new ResultSS();
            var model = new SettingViewModel();
            TryUpdateModel(model);
          
            Response.Cookies["IPKartu"].Value = model.IPKartu ?? "";
            Response.Cookies["IPKartu"].Expires = DateTime.Now.AddYears(100);
            Response.Cookies["PortKartu"].Value = model.PortKartu == null ? "" : model.PortKartu.ToString();
            Response.Cookies["PortKartu"].Expires = DateTime.Now.AddYears(100);

            Response.Cookies["IPTracer"].Value = model.IPTracer ?? "";
            Response.Cookies["IPTracer"].Expires = DateTime.Now.AddYears(100);
            Response.Cookies["PortTracer"].Value = model.PortTracer == null ? "" : model.PortTracer.ToString();
            Response.Cookies["PortTracer"].Expires = DateTime.Now.AddYears(100);

            Response.Cookies["IPTracerRM"].Value = model.IPTracerRM ?? "";
            Response.Cookies["IPTracerRM"].Expires = DateTime.Now.AddYears(100);
            Response.Cookies["PortTracerRM"].Value = model.PortTracerRM == null ? "" : model.PortTracerRM.ToString();
            Response.Cookies["PortTracerRM"].Expires = DateTime.Now.AddYears(100);

            Response.Cookies["IPLabel"].Value = model.IPLabel ?? "";
            Response.Cookies["IPLabel"].Expires = DateTime.Now.AddYears(100);
            Response.Cookies["PortLabel"].Value = model.PortLabel == null ? "" : model.PortLabel.ToString();
            Response.Cookies["PortLabel"].Expires = DateTime.Now.AddYears(100);

            Response.Cookies["IPGelangD"].Value = model.IPGelangD ?? "";
            Response.Cookies["IPGelangD"].Expires = DateTime.Now.AddYears(100);
            Response.Cookies["PortGelangD"].Value = model.PortGelangD == null ? "" : model.PortGelangD.ToString();
            Response.Cookies["PortGelangD"].Expires = DateTime.Now.AddYears(100);

            Response.Cookies["IPGelangB"].Value = model.IPGelangB ?? "";
            Response.Cookies["IPGelangB"].Expires = DateTime.Now.AddYears(100);
            Response.Cookies["PortGelangB"].Value = model.PortGelangB == null ? "" : model.PortGelangB.ToString();
            Response.Cookies["PortGelangB"].Expires = DateTime.Now.AddYears(100);

            Response.Cookies["AutoPrintKartu"].Value = model.AutoPrintKartu.ToString();
            Response.Cookies["AutoPrintKartu"].Expires = DateTime.Now.AddYears(10);

            Response.Cookies["AutoPrintLabel"].Value = model.AutoPrintLabel.ToString();
            Response.Cookies["AutoPrintLabel"].Expires = DateTime.Now.AddYears(10);

            Response.Cookies["AutoPrintTracer"].Value = model.AutoPrintTracer.ToString();
            Response.Cookies["AutoPrintTracer"].Expires = DateTime.Now.AddYears(10);

            Response.Cookies["JumlahPrintLabel"].Value = model.JumlahPrintLabel.ToString();
            Response.Cookies["JumlahPrintLabel"].Expires = DateTime.Now.AddYears(10);

            Response.Cookies["JumlahPrintGelang"].Value = model.JumlahPrintGelang.ToString();
            Response.Cookies["JumlahPrintGelang"].Expires = DateTime.Now.AddYears(10);
            result.IsSuccess = true;
            result.Message = "Berhasil Simpan";
            result.Data = 1;
            result.Count = 1;
            return JsonHelper.JsonMsgCreate(result);
        }
    }
}