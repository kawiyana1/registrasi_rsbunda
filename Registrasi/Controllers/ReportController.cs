﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using iHos.MVC.Property;
using Registrasi.Entities;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Registrasi.Controllers
{
    [Authorize(Roles = "Registrasi")]
    public class ReportController : Controller
    {
        #region ===== C E T A K  T R A C E R
        public ActionResult CetakTracer(string noreg)
        {
            Session["noreg"] = noreg;
            ViewBag.NoReg = noreg;
            return View();
        }
        #endregion

        #region ===== C E T A K  L A B E L
        public ActionResult CetakLabel(string noreg, int jml = 1)
        {
            Session["noreg"] = noreg;
            Session["jml"] = jml;
            ViewBag.NoReg = noreg;
            ViewBag.Jml = jml;
            return View();
        }
        #endregion

        #region ===== C E T A K  G E N E R A L   C O  N S E N T
        public ActionResult CetakGeneralConsent(string noreg)
        {
            Session["noreg"] = noreg;
            ViewBag.NoReg = noreg;
            return View();
        }
        #endregion

        #region ===== K A R T U
        public ActionResult CetakKartu(string nrm, string noreg)
        {
            Session["noreg"] = noreg;
            Session["nrm"] = nrm;
            ViewBag.NoReg = noreg;
            ViewBag.NRM = nrm;
            return View();
        }
        #endregion

        #region ===== K A R T U S K I N  C A R E
        public ActionResult CetakKartuSkinCare(string nrm, string noreg)
        {
            Session["noreg"] = noreg;
            Session["nrm"] = nrm;
            ViewBag.NoReg = noreg;
            ViewBag.NRM = nrm;
            return View();
        }
        #endregion

        #region ===== A U T O - P R I N T


        [HttpPost]
        public string PrintLabel(string id, int jumlah, HttpRequestBase h = null)
        {
            string message = $"Print_Label {id} {jumlah}";
            IPAddress ipAddress;
            int port;
            try
            {
                ipAddress = IPAddress.Parse(h == null ? Request.Cookies["IPLabel"].Value : h.Cookies["IPLabel"].Value);
                port = int.Parse(h == null ? Request.Cookies["PortLabel"].Value : h.Cookies["PortLabel"].Value);
            }
            catch
            {
                Response.Cookies["IPLabel"].Value = "127.0.0.1";
                Response.Cookies["IPLabel"].Expires = DateTime.Now.AddYears(100);
                Response.Cookies["PortLabel"].Value = "3024";
                Response.Cookies["PortLabel"].Expires = DateTime.Now.AddYears(100);
                ipAddress = IPAddress.Parse("127.0.0.1");
                port = 3024;
            }
            var result = ProsesPrint(message, ipAddress, port, h);
            return JsonHelper.JsonMsg(result, result == "success");
        }

        [HttpPost]
        public string PrintTracer(string id, HttpRequestBase h = null)
        {
            string message = $"Print_Tracer {id}";
            IPAddress ipAddress;
            int port;
            try
            {
                ipAddress = IPAddress.Parse(h == null ? Request.Cookies["IPTracer"].Value : h.Cookies["IPTracer"].Value);
                port = int.Parse(h == null ? Request.Cookies["PortTracer"].Value : h.Cookies["PortTracer"].Value);
            }
            catch
            {
                Response.Cookies["IPTracer"].Value = "127.0.0.1";
                Response.Cookies["IPTracer"].Expires = DateTime.Now.AddYears(100);
                Response.Cookies["PortTracer"].Value = "3024";
                Response.Cookies["PortTracer"].Expires = DateTime.Now.AddYears(100);
                ipAddress = IPAddress.Parse("127.0.0.1");
                port = 3024;
            }
            var result = ProsesPrint(message, ipAddress, port, h);
            return JsonHelper.JsonMsg(result, result == "success");
        }

        [HttpPost]
        public string PrintKartu(string nrm, HttpRequestBase h = null)
        {
            string message = $"PrintKartu {nrm}";
            IPAddress ipAddress;
            int port;
            try
            {
                ipAddress = IPAddress.Parse(h == null ? Request.Cookies["IPKartu"].Value : h.Cookies["IPKartu"].Value);
                port = int.Parse(h == null ? Request.Cookies["PortKartu"].Value : h.Cookies["PortKartu"].Value);
            }
            catch
            {
                Response.Cookies["IPKartu"].Value = "127.0.0.1";
                Response.Cookies["IPKartu"].Expires = DateTime.Now.AddYears(100);
                Response.Cookies["PortKartu"].Value = "3024";
                Response.Cookies["PortKartu"].Expires = DateTime.Now.AddYears(100);
                ipAddress = IPAddress.Parse("127.0.0.1");
                port = 3024;
            }
            var result = ProsesPrint(message, ipAddress, port, h);
            return JsonHelper.JsonMsg(result, result == "success");
        }

        [HttpPost]
        public string PrintKartuSkinCare(string nrm, HttpRequestBase h = null)
        {
            string message = $"PrintKartuSkinCare {nrm}";
            IPAddress ipAddress;
            int port;
            try
            {
                ipAddress = IPAddress.Parse(h == null ? Request.Cookies["IPKartu"].Value : h.Cookies["IPKartu"].Value);
                port = int.Parse(h == null ? Request.Cookies["PortKartu"].Value : h.Cookies["PortKartu"].Value);
            }
            catch
            {
                Response.Cookies["IPKartu"].Value = "127.0.0.1";
                Response.Cookies["IPKartu"].Expires = DateTime.Now.AddYears(100);
                Response.Cookies["PortKartu"].Value = "3024";
                Response.Cookies["PortKartu"].Expires = DateTime.Now.AddYears(100);
                ipAddress = IPAddress.Parse("127.0.0.1");
                port = 3024;
            }
            var result = ProsesPrint(message, ipAddress, port, h);
            return JsonHelper.JsonMsg(result, result == "success");
        }

        private string ProsesPrint(string message, IPAddress ipAddress, int port, HttpRequestBase h = null)
        {
            try
            {
                byte[] bytes = new byte[1024];
                try
                {
                    IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
                    IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);
                    Socket sender = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                    try
                    {
                        IAsyncResult iaresult = sender.BeginConnect(remoteEP, null, null);
                        bool success = iaresult.AsyncWaitHandle.WaitOne(5000, true);
                        if (sender.Connected)
                        {
                            sender.EndConnect(iaresult);
                        }
                        else
                        {
                            sender.Close();
                            throw new ApplicationException("Failed to connect server.");
                        }

                        byte[] msg = Encoding.ASCII.GetBytes(message);
                        int bytesSent = sender.Send(msg);
                        int bytesRec = sender.Receive(bytes);
                        string result = Encoding.ASCII.GetString(bytes, 0, bytesRec);

                        sender.Shutdown(SocketShutdown.Both);
                        sender.Close();
                        return "success";
                    }
                    catch (ArgumentNullException ane)
                    {
                        return $"ArgumentNullException : {ane.Message}";
                    }
                    catch (SocketException se)
                    {
                        return $"SocketException : {se.Message}";
                    }
                    catch (Exception e)
                    {
                        return $"Unexpected exception : {e.Message}";
                    }

                }
                catch (Exception e)
                {
                    return $"Unexpected exception : {e.Message}";
                }
            }
            catch (Exception ex)
            {
                return $"Error : {ex.Message}";
            }
        }

        #endregion

        #region ===== VIEW PDF LABEL & TRACER
        public ActionResult Labelpdf(string noreg, int jml, int jml2 = 1)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/FormReport"), $"LabelBarcode.rpt"));
                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<System.Data.DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("RSUB_LoopDataLabel", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].Parameters.Add(new SqlParameter("@Jml", jml));
                    cmd[i].Parameters.Add(new SqlParameter("@Jml2", jml2));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new System.Data.DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"RSUB_LoopDataLabel;1"].SetDataSource(ds[i].Tables[0]);


                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public ActionResult Tracerpdf(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/FormReport"), $"CetakTracer.rpt"));
                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<System.Data.DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("PrintTracer", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new System.Data.DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"PrintTracer;1"].SetDataSource(ds[i].Tables[0]);


                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public ActionResult GeneralConsentpdf(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/FormReport"), $"GeneralConsent.rpt"));
                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<System.Data.DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("GeneralConsent", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@NoReg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new System.Data.DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"GeneralConsent;1"].SetDataSource(ds[i].Tables[0]);


                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

        public ActionResult Kartupdf(string noreg)
        {
            try
            {
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/FormReport"), $"CetakKartu.rpt"));
                var service = new SqlCon_SIM();
                using (var conn = new SqlConnection(service.ConString))
                {
                    var cmd = new List<SqlCommand>();
                    var da = new List<SqlDataAdapter>();
                    var ds = new List<System.Data.DataSet>();

                    var i = 0;
                    cmd.Add(new SqlCommand("Print_KartuPasien", conn));
                    cmd[i].Parameters.Add(new SqlParameter("@Noreg", noreg));
                    cmd[i].CommandType = CommandType.StoredProcedure;
                    da.Add(new SqlDataAdapter(cmd[i]));
                    ds.Add(new System.Data.DataSet());
                    da[i].Fill(ds[i]);
                    rd.Database.Tables[$"Print_KartuPasien"].SetDataSource(ds[i].Tables[0]);


                };
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                rd.Close();
                return File(stream, "application/pdf");
            }
            catch (LogOnException ex) { throw new Exception(ex.Message); }
            catch (DataSourceException ex) { throw new Exception(ex.Message); }
            catch (EngineException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion
    }
}