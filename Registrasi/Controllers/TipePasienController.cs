﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using Registrasi.Entities;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Runtime.Remoting.Contexts;
using System.Web;
using System.Web.Mvc;

namespace Registrasi.Controllers
{
    [Authorize(Roles = "Registrasi")]
    public class TipePasienController : Controller
    {
        #region   ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string noreg)
        {
            var model = new TipePasienViewModel();
            using (var s = new SIM_Entities())
            {
                var y = s.VW_Registrasi_WEBREGISTRASI.FirstOrDefault(x => x.NoReg == noreg);
                model.NoReg = y.NoReg;
                model.NRM = y.NRM;
                model.NamaPasien = y.NamaPasien;
                model.JenisKerjasama = y.JenisKerjasama;
                model.PasienKTP = y.PasienKTP;
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new TipePasienViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        if(item.CheckOtorisasi == 0) return JsonHelper.JsonMsgInfo("Otorisasi hak akses belum dilakukan");

                        var update = s.Registrasi_UpdateTipePasien(
                            item.NoReg,
                            item.JenisKerjasamaID,
                            item.KodePerusahaan,
                            item.NoAnggota,
                            item.PasienKTP,
                            item.PasienAsuransi,
                            item.CustomerKerjasamaID,
                            item.CaseNO,
                            item.KelasID ?? "XX",
                            item.NoSep,
                            item.KelasID ?? "XX",
                            item.JenisKerjasamaID
                         );
                        result = new ResultSS(1,update);

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"TipePasien Update {item.NoReg}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== L I S T  P E R U S A H A A N
        [HttpPost]
        public string ListPerusahaan(int jenisKerjaSama, string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var kerjasama = "UMUM";
                    var kerja_sama = s.SIMmJenisKerjasama.FirstOrDefault(x => x.JenisKerjasamaID == jenisKerjaSama);
                    kerjasama = (kerja_sama == null) ? kerjasama : kerja_sama.JenisKerjasama;

                    IQueryable <VW_CustomerKerjasama> proses = s.VW_CustomerKerjasama.Where(x => x.JenisKerjasama == kerjasama);
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(VW_CustomerKerjasama.Kode_Customer)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(VW_CustomerKerjasama.Nama_Customer)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<VW_CustomerKerjasama>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== C R E D E N T I A L S

        [HttpPost]
        public string checkOtorisasiUser(string Username, string Password)
        {
            try
            {
                object r;
                var manager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var signinManager = HttpContext.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                var user = manager.FindByName(Username);

                var checkPassword = signinManager.UserManager.CheckPassword(user, Password);
                if (checkPassword)
                {
                    using (var sim = new SIM_Entities())
                    {
                        var checkOtorisasi = sim.mUserOtorisasiKhusus.Where(x => x.UserIDWeb == user.Id && x.KriteriaOtorisasi == "PERUBAHAN TIPE PASIEN").FirstOrDefault();
                        if (checkOtorisasi != null)
                        {
                            r = new
                            {
                                isSuccess = true,
                                Data = checkOtorisasi,
                                Mssg = "Otorisasi diberikan.",
                            };
                        }
                        else
                        {
                            r = new
                            {
                                isSuccess = false,
                                Data = "",
                                Mssg = "Otorisasi tidak diberikan.",
                            };
                        }
                    }
                }
                else
                {
                    r = new
                    {
                        isSuccess = false,
                        Data = "",
                        Mssg = "Username atau password tidak ditemukan.",
                    };
                }

                return JsonConvert.SerializeObject(r);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

    }
}