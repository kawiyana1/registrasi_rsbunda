﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Newtonsoft.Json;
using Registrasi.Entities;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Registrasi.Controllers
{
    public class RegionalController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string ListProvinsi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mRegional> proses = s.mRegional.Where(x => x.Level_Ke == 1);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(mRegional.Kode_Regional)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(mRegional.Nama_Regional)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<mRegional>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListKabupaten(string parent, string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mRegional> proses = s.mRegional.Where(x => x.Kode_Parent == parent);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(mRegional.Kode_Regional)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(mRegional.Nama_Regional)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<mRegional>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListKecamatan(string parent, string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<VwKecamatan> proses = s.VwKecamatan.Where(x => x.ProvinsiId == parent);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(VwKecamatan.KecamatanId)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(VwKecamatan.KecamatanNama)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<VwKecamatan>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListDesa(string parent, string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<VwRegional> proses = s.VwRegional.Where(x => x.ProvinsiId == parent);    
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(VwRegional.DesaId)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(VwRegional.DesaNama)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<VwRegional>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        [HttpPost]
        public string GetData(string tipe, string parent)
        {
            try
            {
                var m = new VwRegional();
                using (var s = new SIM_Entities())
                {
                    IQueryable<VwRegional> proses = s.VwRegional;
                    if (tipe == "provinsi")
                    {
                        proses = proses.Where(x => x.ProvinsiId == parent);
                    }
                    else if (tipe == "kabupaten")
                    {
                        proses = proses.Where(x => x.KabupatenId == parent);
                    }
                    else if (tipe == "kecamatan")
                    {
                        proses = proses.Where(x => x.KecamatanId == parent);
                    }
                    else if (tipe == "desa")
                    {
                        proses = proses.Where(x => x.DesaId == parent);
                    }
                    else
                    {
                        proses = proses.Where(x => x.ProvinsiId == "XXXXXXXXXXXXXXXXXXXXX");
                    }
                    m = proses.FirstOrDefault();
                }
                return JsonConvert.SerializeObject(new {
                    IsSuccess = true,
                    Data = m
                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }


    }
}