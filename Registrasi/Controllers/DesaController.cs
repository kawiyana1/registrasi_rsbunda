﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Registrasi.Entities;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Registrasi.Controllers
{
    [Authorize(Roles = "Registrasi")]
    public class DesaController : Controller
    {
        #region ===== I N D E X
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== C R E A T E

        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new DesaViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var has = s.mDesa.Where(x => x.DesaNama == item.DesaNama).Count() > 0;
                        if (has) throw new Exception("Nama sudah digunakan");

                        var m = IConverter.Cast<mDesa>(item);
                        s.mDesa.Add(m);
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Desa Create {m.DesaID}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            DesaViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Vw_Desa.FirstOrDefault(x => x.DesaID == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<DesaViewModel>(m);
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post(string id)
        {
            try
            {
                var item = new DesaViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var model = s.mDesa.FirstOrDefault(x => x.DesaID == item.DesaID);
                        if (model == null) throw new Exception("Data Tidak ditemukan");

                        if (model.DesaNama.ToUpper() != item.DesaNama.ToUpper())
                        {
                            var has = s.mDesa.Where(x => x.DesaNama == item.DesaNama).Count() > 0;
                            if (has) throw new Exception("Nama sudah digunakan");
                        }

                        TryUpdateModel(model);
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Desa Edit {model.DesaID}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E L E T E

        [HttpPost]
        [Authorize(Roles = "SuperUser")]
        public string Delete(string id)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var m = s.mDesa.FirstOrDefault(x => x.DesaID == id);
                    if (m == null) throw new Exception("Data Tidak ditemukan");
                    s.mDesa.Remove(m);
                    result = new ResultSS(s.SaveChanges());

                    var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    {
                        Activity = $"Desa delete {id}"
                    };
                    UserActivity.InsertUserActivity(userActivity);
                }
                return JsonHelper.JsonMsgDelete(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== A U T O I D

        public string AutoId()
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var m = s.mDesa_AutoID().FirstOrDefault();
                    if (m == null) return JsonHelper.JsonMsgError("Data tidak ditemukan");
                    result = new ResultSS(m) { IsSuccess = true };
                }
                return JsonConvert.SerializeObject(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Vw_Desa> proses = s.Vw_Desa;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Vw_Desa.DesaID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Vw_Desa.DesaNama)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Vw_Desa.KecamatanNama)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Vw_Desa.Nama_Kabupaten)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(Vw_Desa.KecamatanID)}.Contains(@0)", filter[4]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<DesaViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== G E T D A T A

        [HttpPost]
        public string GetData(string id)
        {
            try
            {
                mDesa m;
                using (var s = new SIM_Entities())
                {
                    m = s.mDesa.FirstOrDefault(x => x.DesaID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = IConverter.Cast<DesaViewModel>(m)
                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        [HttpPost]
        public string ListDokter(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mRegional> proses = s.mRegional.Where(x => x.Level_Ke == 1);
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(mRegional.Kode_Regional)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(mRegional.Nama_Regional)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<mRegional>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}