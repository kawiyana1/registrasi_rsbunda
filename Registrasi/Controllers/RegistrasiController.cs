﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Registrasi.Entities;
using Registrasi.Entities.BPJS;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using static Registrasi.Models.BpjsModel;

namespace Registrasi.Controllers
{
    [Authorize(Roles = "Registrasi")]
    public class RegistrasiController : Controller
    {
        #region ===== I N D E X
        public ActionResult Index()
        {
            string url = ConfigurationManager.AppSettings["UrlBridging"];
            ViewBag.Url = url;
            return View();
        }
        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<VW_Registrasi_WEBREGISTRASI> proses = s.VW_Registrasi_WEBREGISTRASI.Where(x => x.RawatInap == false);

                    if (filter[24] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[22]))
                        {
                            proses = proses.Where("TglReg >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[23]))
                        {
                            proses = proses.Where("TglReg <= @0", DateTime.Parse(filter[23]));
                        }

                    }
                    if (filter[25] == "1")
                        proses = proses.Where(x => x.StatusPeriksa == "Belum");
                    else if (filter[25] == "2")
                        proses = proses.Where(x => x.StatusPeriksa == "Sudah" || x.StatusPeriksa == "Sudah Periksa");
                    else if (filter[25] == "3")
                        proses = proses.Where(x => x.StatusPeriksa == "CO");
                    else if (filter[26] == "True")
                        proses = proses.Where(x => x.DibuatkanSEP == false);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(VW_Registrasi_WEBREGISTRASI.NoReg)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(VW_Registrasi_WEBREGISTRASI.SectionName)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(VW_Registrasi_WEBREGISTRASI.NRM)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(VW_Registrasi_WEBREGISTRASI.NamaPasien)}.Contains(@0)", filter[4]);
                    if (!string.IsNullOrEmpty(filter[5])) proses = proses.Where($"{nameof(VW_Registrasi_WEBREGISTRASI.JenisKerjasama)}.Contains(@0)", filter[5]);
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(VW_Registrasi_WEBREGISTRASI.Alamat)}.Contains(@0)", filter[6]);
                    if (!string.IsNullOrEmpty(filter[15])) proses = proses.Where($"{nameof(VW_Registrasi_WEBREGISTRASI.Nama_Supplier)}.Contains(@0)", filter[15]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<RegistrasiViewModel>(x));
                    foreach (var x in m)
                    {
                        var pasienkhusus = s.Vw_PasienBermasalah.OrderByDescending(z => z.TglInput).FirstOrDefault(z => z.NRM == x.NRM);
                        if (pasienkhusus != null)
                        {
                            x.Emoticon = pasienkhusus.Emoticon;
                        }
                        x.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                        x.JamReg_View = x.JamReg.ToString("HH:mm");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== C R E A T E
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get()
        {
            string url = ConfigurationManager.AppSettings["UrlBridging"];
            ViewBag.Url = url;
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView();
            else
                return View();
        }

        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public ActionResult Create_Post()
        {
            var item = new RegistrasiInsertViewModel();
            var noReg = "";

            try
            {
                TryUpdateModel(item);
                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        if (item.PasienPanjer == true)
                        {
                            item.Kode_Customer = "CUST-0001";
                            item.NoKartu = "PANJER";
                            item.KelasID = "11";
                        }

                        if (item.JenisPasienKerjasamaID == 2 || item.JenisPasienKerjasamaID == 9)
                        {
                            if (item.NoKartu == null)
                            {
                                ModelState.AddModelError("", "Nomor Kartu tidak boleh kosong");
                                return View(item);
                            }
                            if (item.Kode_Customer == null)
                            {
                                ModelState.AddModelError("", "Customer Kerja Sama belum dipilih");
                                return View(item);
                            }
                        }



                        var statusDirawat = s.mPasien.FirstOrDefault(x => x.NRM == item.NRM);
                        if (statusDirawat != null)
                        {
                            statusDirawat.NoIdentitas = item.NoIdentitas;
                            var reg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NRM == item.NRM && x.Batal != true && x.StatusBayar != "Sudah Bayar");
                            
                            if (statusDirawat.SedangDirawat == true || reg != null)
                            {
                                ModelState.AddModelError("", "Pasien Sedang Dirawat");
                                return View(item);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "Data pasien tidak ditemukan");
                            return View(item);
                        }


                        var Reg = s.Pelayanan_Registrasi_Insert(
                        item.NRM,
                        item.Dokter_ID == null ? "XX" : item.Dokter_ID,
                        item.SectionID,
                        item.Tanggal,
                        item.WaktuPraktek_ID == null ? 73 : item.WaktuPraktek_ID,
                        1,
                        item.NoReservasi == null ? "" : item.NoReservasi,
                        item.JenisPasienKerjasamaID,
                        item.Kode_Customer,
                        item.NoKartu,
                        item.KelasID,
                        item.PasienRS,
                        item.PenanggungNRM,
                        item.PenanggungNama,
                        item.PenanggungAlamat,
                        item.PenanggungHubungan,
                        item.PenanggungPhone,
                        "",
                        item.VendorID,
                        "",
                        item.DokterPengirim_ID,
                        item.PasienAsuransi,
                        item.AsuransiID,
                        item.AssCompany_ID,
                        item.PasienPaket,
                        item.PaketJasa_ID,
                        item.NoKartuAsuransi,
                        item.CaraDatang,
                        item.ICD_ID,
                        false,
                        item.PasienKTP,
                        item.NoSep,
                        item.PasienCovid,
                        item.PasienPanjer
                        ).FirstOrDefault();


                        noReg = Reg.NoReg;
                        var Antrian = Reg.NoAntrian;

                        var insDiagnosaAwal = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noReg);
                        var tr = new SIMtrRegistrasi();

                        if (noReg != null)
                        {
                            insDiagnosaAwal.NoReg = noReg;
                            insDiagnosaAwal.DiagnosaAwal = item.Diagnosa;
                        }
                        else
                        {
                            ModelState.AddModelError("", "NoReg is Kosong");
                        }

                        if (noReg == null)
                        {
                            ModelState.AddModelError("", "NoReg is Niul");
                            return View(item);
                        }

                        if (!string.IsNullOrEmpty(item.Memo))
                        {
                            s.SIMtrMemo.Add(new SIMtrMemo()
                            {
                                NOReg = noReg,
                                Jam = DateTime.Now,
                                Tanggal = item.Tanggal.Value,
                                Memo = item.Memo,
                                NoUrut = Antrian.ToString(),
                                SectionID = item.SectionID,
                                User_ID = 490
                            });
                            s.SaveChanges();
                        }

                        var rg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noReg);
                        var userWeb = User.Identity.GetUserId();
                        if (rg != null)
                        {
                            if(item.ClickPasienBaru == null)
                            {
                                s.BPJS_Task_Insert(
                                    noReg,
                                    "Pasien Lama",
                                    DateTime.Now,
                                    DateTime.Now,
                                    DateTime.Now
                                );
                            }
                            else
                            {
                                s.BPJS_Task_Insert(
                                    noReg,
                                    "Pasien Baru",
                                    item.ClickPasienBaru.Value.AddMinutes(-10),
                                    item.ClickPasienBaru,
                                    DateTime.Now
                                );
                            }

                            rg.Rujukan = item.Rujukan;
                            rg.Pribadi = item.PasienPribadi;
                            rg.PasienBayi = item.PasienBayi;
                            rg.DiagnosaKet = item.DiagnosaKet;
                            rg.DokterPengirim = item.DokterPengirim;
                            rg.Faskes = item.Faskes;
                            rg.UserIDWeb = userWeb;
                            rg.NoRujukanBPJS = item.NoRujukanBPJS;
                            rg.NIK = item.NIK;
                            rg.Phone_Reg = item.Phone;
                            s.SaveChanges();
                        }
                        var pasien = s.mPasien.FirstOrDefault(x => x.NRM == rg.NRM);
                        if (pasien != null)
                        {
                            pasien.NoIdentitas = item.NIK;
                            pasien.Phone = item.Phone;
                            s.SaveChanges();
                        }
                        if (item.getRujukanFrom == null) item.getRujukanFrom = "0";
                        if (item.PoliRujukan == null) item.PoliRujukan = "XX";
                        if (item.PoliRujukanNama == null) item.PoliRujukanNama = "XX";
                        s.BridgingBPJS_Insert_BpjsV2_RegisterTaskID(noReg, int.Parse(item.getRujukanFrom), item.PoliRujukan, item.PoliRujukanNama);

                        var getlinkpushantrean = s.BPJS_URL_PushTask(noReg).FirstOrDefault();
                        var bpjsbridging = new BpjsSEPService();
                        bpjsbridging.preTambahAntrian(getlinkpushantrean.Base, getlinkpushantrean.Isi);

                        s.SaveChanges();

                        result = new ResultSS(1, rg);
                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Registrasi Create {noReg}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    if (result.IsSuccess)
                    {
                        if (!string.IsNullOrEmpty(item.NoSep))
                        {
                            using (var ss = new BPJSEntities())
                            {
                                var m = ss.BPJStrSEPVClaim.FirstOrDefault(x => x.NoSEP == item.NoSep);
                                if (m != null)
                                {
                                    m.NoReg = noReg;
                                    m.Status = "SUDAH";
                                    ss.SaveChanges();
                                }
                            }
                        }

                        if (Request.Cookies["AutoPrintLabel"] != null)
                        {
                            if (bool.Parse(Request.Cookies["AutoPrintLabel"].Value))
                            {
                                var x = new ReportController();
                                x.PrintLabel(noReg, int.Parse(Request.Cookies["JumlahPrintLabel"].Value), Request);
                            }
                        }


                        return RedirectToAction("Detail", new { id = noReg });
                    }
                    else
                    {
                        ModelState.AddModelError("", result.Message);
                        return View(item);
                    }
                }
                else
                {
                    return View(item);
                }
            }
            catch (ValidationException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(item);
            }
            catch (SqlException ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(item);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(item);
            }
        }
        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            RegistrasiInsertViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    string url = ConfigurationManager.AppSettings["UrlBridging"];
                    ViewBag.Url = url;

                    var tujuan = s.Registrasi_GetListTujuan.FirstOrDefault(x => x.NoReg == id);
                    var m = s.VW_Registrasi_WEBREGISTRASI.FirstOrDefault(x => x.NoReg == id);
                    var registrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<RegistrasiInsertViewModel>(m);
                    item.PenanggungPhone = m.PenanggungTelp;
                    item.JenisPasienKerjasama = m.JenisKerjasama;
                    item.PasienPribadi = m.Pribadi;
                    item.SectionID = tujuan.SectionID;
                    item.NamaSection = tujuan.NamaSection;
                    item.Dokter = tujuan.NamaDokter;
                    item.Waktu = tujuan.NamaWaktu;
                    item.Tanggal = tujuan.Tanggal;
                    item.JenisKelamin = m.JenisKelamin == "M" ? "Male" : m.JenisKelamin == "F" ? "Female" : "Other";
                    item.Kelas = m.NamaKelas;
                    item.UmurView = m.UmurThn + " <b>Th</b> " + m.UmurBln + " <b>Bln</b> " + m.UmurHr + " <b>Hr</b>";
                    item.PenanggungNRM = m.PenanggungNRM;
                    item.PasienRS = m.PenanggungIsPasien;
                    item.Rujukan = registrasi.Rujukan;
                    item.CaraDatang = registrasi.CaraDatang;
                    item.DiagnosaKet = registrasi.DiagnosaKet;
                    item.NoSep = registrasi.NoSEP;
                    if (registrasi != null)
                    {
                        item.VendorID = registrasi.VendorID_Referensi;
                        var dokter = s.mSupplier.FirstOrDefault(x=> x.Kode_Supplier == registrasi.VendorID_Referensi);
                        if(dokter!= null)
                        {
                            item.VendorPengirim = dokter.Nama_Supplier;
                        }

                        item.Faskes = registrasi.Faskes;
                        var faskes = s.mSupplier.FirstOrDefault(x => x.Kode_Supplier == registrasi.Faskes);
                        if (faskes != null)
                        {
                            item.FaskesNama = faskes.Nama_Supplier;
                        }
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post(string id)
        {
            try
            {
                var item = new RegistrasiInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var model = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.NoReg);
                        if (model == null) throw new Exception("Data Tidak ditemukan");
                        var userWeb = User.Identity.GetUserId();
                        model.Rujukan = item.Rujukan;
                        model.Pribadi = item.PasienPribadi;
                        model.VendorID_Referensi = item.VendorID;
                        model.CaraDatang = item.CaraDatang;
                        model.UserIDWeb = userWeb;
                        model.PasienBayi = item.PasienBayi;
                        model.NoSEP = item.NoSep;
                        model.DiagnosaKet = item.DiagnosaKet;
                        model.DokterPengirim = item.DokterPengirim;
                        result = new ResultSS(s.SaveChanges());

                        if (!string.IsNullOrEmpty(item.NoSep))
                        {
                            using (var ss = new BPJSEntities())
                            {
                                var m = ss.BPJStrSEPVClaim.FirstOrDefault(x => x.NoSEP == item.NoSep);
                                if (m != null)
                                {
                                    m.NoReg = item.NoReg;
                                    m.Status = "SUDAH";
                                    ss.SaveChanges();
                                }
                            }
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Registrasi Edit {model.NoReg}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== B A T A L

        [HttpPost]
        public string Batal(string id, string alasan)
        {
            try
            {
                ResultSS result;

                using (var s = new SIM_Entities())
                {
                    var user = User.Identity.GetUserId();
                    var ceksep = s.SIMtrRegistrasi.Where(x => x.NoReg == id).FirstOrDefault();
                    if (ceksep != null) { 
                        if(ceksep.NoSEP != null) return JsonHelper.JsonMsgError("NoReg " + id + " sudah mendapat NoSEP");
                    }
                    var batal = s.BatalRegistrasiDenganAsalan(id, true, alasan).FirstOrDefault();
                    if (batal == "SUKSES")
                    {
                        result = new ResultSS(1, batal);
                        if (result.IsSuccess == true)
                        {
                            var reg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == id);
                            if (reg != null)
                            {
                                reg.UserIDWeb = user;
                                s.SaveChanges();
                            }
                        }
                    }
                    else
                    {
                        return JsonHelper.JsonMsgError(batal);
                    }
                    
                }
                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                {
                    Activity = $"Registrasi Batal {id}"
                };
                UserActivity.InsertUserActivity(userActivity);

                return JsonHelper.JsonMsgCustom(result, "Dibatalkan");
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E T A I L
        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string id)
        {
            var result = new RegistrasiInsertViewModel();
            using (var s = new SIM_Entities())
            {
                var model = s.VW_Registrasi_WEBREGISTRASI.FirstOrDefault(x => x.NoReg == id);
                var tujuan = s.Registrasi_GetListTujuan.FirstOrDefault(x => x.NoReg == id);
                result = IConverter.Cast<RegistrasiInsertViewModel>(model);
                result.JenisKelamin = model.JenisKelamin == "M" ? "Male" : model.JenisKelamin == "F" ? "Female" : "Other";
                result.Kelas = model.NamaKelas;
                result.PenanggungPhone = model.PenanggungTelp;
                result.JenisPasienKerjasama = model.JenisKerjasama;
                result.SectionID = tujuan.SectionID;
                result.Dokter_ID = tujuan.DokterID;
                result.Dokter = tujuan.NamaDokter;
                result.NamaSection = tujuan.NamaSection;
                result.Waktu = tujuan.NamaWaktu;
                result.WaktuPraktek_ID = tujuan.WaktuID;
                result.Tanggal = tujuan.Tanggal;
                result.Tanggal_view = tujuan.Tanggal.ToString("dd/MM/yyyy");
                result.UmurView = model.UmurThn + " <b>Th</b> " + model.UmurBln + " <b>Bln</b> " + model.UmurHr + " <b>Hr</b>";
                //result.Umur = model.UmurThn;
                result.PenanggungNRM = model.PenanggungNRM;
                result.PasienRS = model.PenanggungIsPasien;
                result.Antrian = model.NoUrut;
            }
            var item = new SIMmKelas();
            if (result.KelasID == null)
            {
                item.NamaKelas = null;
            }
            else
            {
                using (var service = new SIM_Entities())
                {

                    item = service.SIMmKelas.FirstOrDefault(x => x.KelasID == result.KelasID);
                }
                result.Kelas = item.NamaKelas;
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(result);
            else
                return View(result);
        }
        #endregion

        #region==== C H E C K  B O N
        [HttpGet]
        public string checkPasienBON(string nrm)
        {
            try
            {
                Object r;
                using (var s = new SIM_Entities())
                {
                    var bon = s.VW_InformasiPasienBON.Where(x => x.NRM == nrm).FirstOrDefault();
                    
                    if (bon != null)
                    {
                        r = new
                        {
                            isSuccess = true,
                            Data = "",
                            Mssg = "",
                        };
                    }
                    else
                    {
                        r = new
                        {
                            isSuccess = false,
                            Data = "",
                            Mssg = "",
                        };
                    }
                }
                return JsonConvert.SerializeObject(r);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== R I W A Y A T  K U N J U N G A N
        [HttpGet]
        public ActionResult RiwayatKunjungan(string id)
        {
            var item = new PasienViewModel();

            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.VW_Pasien_Regional.FirstOrDefault(x => x.NRM == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<PasienViewModel>(m);
                    var tipepasien = s.SIMmJenisKerjasama.FirstOrDefault(x => x.JenisKerjasamaID == m.JenisKerjasamaID);
                    if (tipepasien != null)
                    {
                        item.JenisKerjasama = tipepasien.JenisKerjasama;
                    }
                    var registrasi = s.SIMtrRegistrasi.OrderByDescending(x => x.TglReg).FirstOrDefault(x => x.NRM == id);
                    if (registrasi != null)
                    {
                        var dokter = s.mSupplier.FirstOrDefault(x => x.Kode_Supplier == registrasi.VendorID_Referensi);
                        if (dokter != null)
                        {
                            item.RujukanDari = dokter.Nama_Supplier;
                        }
                    }
                    item.DetailKunjungan_List = new ListDetail<KunjunganViewModel>();
                    var d_Kunjungan = s.Vw_StatusKunjunganPasien.Where(x => x.NRM == id).ToList();
                    foreach (var x in d_Kunjungan)
                    {
                        var y = IConverter.Cast<KunjunganViewModel>(x);
                        y.TglTerakhirKunjungan_View = y.TglTerakhirKunjungan.Value.ToString("dd/MM/yyyy");
                        item.DetailKunjungan_List.Add(false, y);
                    }

                    item.DetailRegistrasi_List = new ListDetail<DetailRegistrasiViewModel>();
                    var d_Registrasi = s.SIMtrRegistrasi.Where(x => x.NRM == id && x.Batal == false).ToList();
                    foreach (var x in d_Registrasi)
                    {
                        var y = IConverter.Cast<DetailRegistrasiViewModel>(x);
                        y.Jam = x.JamReg.ToString("hh:mm");
                        y.Tanggal = x.TglReg.ToString("dd/MM/yyyy");
                        item.DetailRegistrasi_List.Add(false, y);

                    }
                }


            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        public string DetailRiwayatKunjungan(string noreg)
        {
            try
            {
                object d;
                int count; 
                using (var s = new SIM_Entities())
                {
                    d = s.RiwayatPasien_PerNoReg(noreg).ToList();
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = "",
                    Data = d,
                    Message = ""
                });
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }
        #endregion

        #region ===== D E T A I L
        [HttpGet]
        [ActionName("DetailPasien")]
        public ActionResult DetailPasien(string id)
        {
            var result = new RegistrasiInsertViewModel();
            using (var s = new SIM_Entities())
            {
                var model = s.VW_Registrasi_WEBREGISTRASI.FirstOrDefault(x => x.NoReg == id);
                var tujuan = s.Registrasi_GetListTujuan.FirstOrDefault(x => x.NoReg == id);
                result = IConverter.Cast<RegistrasiInsertViewModel>(model);
                result.JenisKelamin = model.JenisKelamin == "M" ? "Male" : model.JenisKelamin == "F" ? "Female" : "Other";
                result.Kelas = model.NamaKelas;
                result.PenanggungPhone = model.PenanggungTelp;
                result.JenisPasienKerjasama = model.JenisKerjasama;
                result.SectionID = tujuan.SectionID;
                result.Dokter_ID = tujuan.DokterID;
                result.Dokter = tujuan.NamaDokter;
                result.NamaSection = tujuan.NamaSection;
                result.Waktu = tujuan.NamaWaktu;
                result.WaktuPraktek_ID = tujuan.WaktuID;
                result.Tanggal = tujuan.Tanggal;
                result.Tanggal_view = tujuan.Tanggal.ToString("dd/MM/yyyy");
                result.UmurView = model.UmurThn + " Th " + model.UmurBln + " Bln " + model.UmurHr + " Hr" ;
                //result.Umur = model.UmurThn;
                result.PenanggungNRM = model.PenanggungNRM;
                result.PasienRS = model.PenanggungIsPasien;
                result.Antrian = model.NoUrut;
                result.TglLahir = model.TglLahir;
                result.JamReg = model.JamReg;
            }
            var item = new SIMmKelas();
            if (result.KelasID == null)
            {
                item.NamaKelas = null;
            }
            else
            {
                using (var service = new SIM_Entities())
                {

                    item = service.SIMmKelas.FirstOrDefault(x => x.KelasID == result.KelasID);
                }
                result.Kelas = item.NamaKelas;
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(result);
            else
                return View(result);
        }
        #endregion

        #region ===== C H E C K  S T A T U S  P E S E R T A
        [HttpPost]
        [ActionName("CheckPesertaBPJS")]
        public string CheckPesertaBPJS(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id)) return JsonHelper.JsonMsgError(new Exception("data harus lengkap"));

                var s = new BpjsSEPService();
                var item = new PasienViewModel();

                var model = new PesertaNoKartuBPJSModel();
                model = s.PesertaNoKartuBPJS(id);

                if (model.response == null)
                    return JsonHelper.JsonMsgError(new Exception("Data Tidak Ditemukan"));
                if (model.metaData.code != "200")
                    return JsonHelper.JsonMsgError(new Exception(model.metaData.message));
                using (var service = new SIM_Entities())
                {
                    var kelas = service.SIMmKelas.FirstOrDefault(x => x.MappingBPJS == model.response.peserta.hakKelas.kode);
                    if (kelas != null)
                    {
                        item.KdKelas = kelas.NamaKelas;
                    }

                    var customer = service.VW_CustomerKerjasamaAnggota.FirstOrDefault(x => x.KelasID == item.KdKelas);
                    if (customer != null)
                    {
                        item.CompanyID = customer.Kode_Customer;
                        item.NamaPerusahaan = customer.Nama_Customer;
                        item.CustomerKerjasamaID = customer.CustomerKerjasamaID;
                    }

                }
                item.Status = model.response.peserta.statusPeserta.keterangan;
                item.TglLahir_View = model.response.peserta.tglLahir.ToString("yyyy-MM-dd");
                item.NamaPasien = model.response.peserta.nama;
                item.NoAnggotaE = model.response.peserta.noKartu;
                item.NamaAnggotaE = model.response.peserta.nama;
                item.NoIdentitas = model.response.peserta.nik;
                item.GenderAnggotaE = model.response.peserta.sex == "L" ? "M" : model.response.peserta.sex == "P" ? "F" : "O";
                item.JenisKelamin = model.response.peserta.sex == "L" ? "M" : model.response.peserta.sex == "P" ? "F" : "O";

                var result = new
                {
                    IsSuccess = true,
                    Data = item
                };
                return JsonConvert.SerializeObject(result);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        [HttpPost]
        [ActionName("CheckPoliSpesialis")]
        public string CheckPoliSpesialis(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id)) return JsonHelper.JsonMsgError(new Exception("Section Tujuan harus diisi."));

                bool statts = false;
                var sec = "";
                using (var sim = new SIM_Entities())
                {
                    var section = sim.SIMmSection.FirstOrDefault(x => x.PoliKlinik == "SPESIALIS" && x.SectionID == id);
                    if (section != null)
                    {
                        statts = true;
                        sec = section.SectionName;
                    }
                }
                var result = new
                {
                    IsSuccess = statts,
                    Data = sec
                };
                return JsonConvert.SerializeObject(result);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
    }
}