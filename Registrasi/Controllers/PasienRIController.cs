﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Registrasi.Entities;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Registrasi.Controllers
{
    [Authorize(Roles = "Registrasi")]
    public class PasienRIController : Controller
    {
        #region ===== I N D E X

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region ===== D E T A I L
        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string id)
        {
            PasienRIViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Pelayanan_Informasi_PasienRI.FirstOrDefault(x => x.NoReg == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<PasienRIViewModel>(m);
                    item.TglReg_View = m.TglReg.ToString("dd/MM/yyyy");
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_Informasi_PasienRI> proses = s.Pelayanan_Informasi_PasienRI;
                    if (filter[10] != "True")
                    {
                        proses = proses.Where("TglReg >= @0", DateTime.Parse(filter[11]));
                        proses = proses.Where("TglReg <= @0", DateTime.Parse(filter[12]));
                    }
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_PasienRI.NoReg)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_PasienRI.NRM)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_PasienRI.NamaPasien)}.Contains(@0)", filter[2]);
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_PasienRI.Alamat)}.Contains(@0)", filter[3]);
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_PasienRI.SectionName)}.Contains(@0)", filter[4]);
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_PasienRI.Kamar)}.Contains(@0)", filter[5]);
                    proses = proses.Where($"{nameof(Pelayanan_Informasi_PasienRI.NamaDOkter)}.Contains(@0)", filter[6]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PasienRIViewModel>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}