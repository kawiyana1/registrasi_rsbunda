﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Registrasi.Entities;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Registrasi.Controllers
{
    [Authorize(Roles = "Registrasi")]
    public class ReservasiController : Controller
    {
        #region ===== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Vw_Reservasi> proses = s.Vw_Reservasi;
                    if (filter[24] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[22]))
                        {
                            proses = proses.Where("UntukTanggal >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[23]))
                        {
                            proses = proses.Where("UntukTanggal <= @0", DateTime.Parse(filter[23]));
                        }

                    }
                    proses = proses.Where($"{nameof(Vw_Reservasi.NoReservasi)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(Vw_Reservasi.NRM)}.Contains(@0)", filter[3]);
                    proses = proses.Where($"{nameof(Vw_Reservasi.Nama)}.Contains(@0)", filter[4]);
                    if (filter[5] != null && filter[5] != "")
                    {
                        int nourut = IFilter.F_int(filter[5]).Value;
                        proses = proses.Where(x => x.NoUrut == nourut);
                    }
                    proses = proses.Where($"{nameof(Vw_Reservasi.SectionName)}.Contains(@0)", filter[6]);
                    proses = proses.Where($"{nameof(Vw_Reservasi.NamaDOkter)}.Contains(@0)", filter[8]);
                    if (filter[25] == "1")
                        proses = proses.Where(x => x.Registrasi == false && x.Batal == false);
                    else if (filter[25] == "2")
                        proses = proses.Where(x => x.Registrasi == true && x.Batal == false);
                    else if (filter[25] == "3")
                        proses = proses.Where(x => x.StatusPeriksa == "Sudah Periksa" && x.StatusBayar != "Sudah Bayar" && x.Batal == false);
                    else if (filter[25] == "4")
                        proses = proses.Where(x => x.StatusBayar == "Sudah Bayar" && x.Batal == false);
                    else if (filter[25] == "5")
                        proses = proses.Where(x => x.Batal == true);
                    proses = proses.OrderByDescending(x => x.Jam);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<ReservasiViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        x.UntukTanggal_View = x.UntukTanggal.ToString("dd/MM/yyyy");
                        x.Jam_View = x.Jam.ToString("HH:mm");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== C R E A T E
        [HttpGet]
        [ActionName("Create")]
        public ActionResult Create_Get(string id)
        {

            var model = new ReservasiViewModel();
            using (var s = new SIM_Entities())
            {
                var m = s.Vw_JadwalSuratKontrolPasien.FirstOrDefault(x => x.NoBukti == id);
                if (m != null)
                {
                    model.NoBukti = m.NoBukti;
                    model.Nama = m.Nama;
                    model.NamaDOkter = m.NamaDOkter;
                    model.SectionName = m.SectionName;
                    model.UntukDokterID = m.DokterID;
                    model.UntukSectionID = m.SectionID;
                    model.Alamat = m.Alamat;
                    model.WaktuID = m.WaktuID;
                    model.Keterangan = m.Keterangan;
                    model.UntukTanggal = m.Tanggal.Value;
                    model.Phone = m.Phone;
                    model.NRM = m.NRM;
                    model.JadwalManual = m.JadwalManual.Value;
                }
                else
                {
                    model.UntukTanggal = DateTime.Today;
                }
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }


        [HttpPost]
        [ActionName("Create")]
        [ValidateAntiForgeryToken]
        public string Create_Post()
        {
            try
            {
                var item = new ReservasiViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    var result = new ResultSS();
                    var resultJadwal = new ResultSS();
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region Hari
                                switch (item.UntukTanggal.DayOfWeek)
                                {
                                    case DayOfWeek.Monday:
                                        item.UntukHari = "Senin";
                                        break;
                                    case DayOfWeek.Tuesday:
                                        item.UntukHari = "Selasa";
                                        break;
                                    case DayOfWeek.Wednesday:
                                        item.UntukHari = "Rabu";
                                        break;
                                    case DayOfWeek.Thursday:
                                        item.UntukHari = "Kamis";
                                        break;
                                    case DayOfWeek.Friday:
                                        item.UntukHari = "Jumat";
                                        break;
                                    case DayOfWeek.Saturday:
                                        item.UntukHari = "Sabtu";
                                        break;
                                    case DayOfWeek.Sunday:
                                        item.UntukHari = "Minggu";
                                        break;
                                }
                                #endregion

                                if (item.JadwalManual == true)
                                {
                                    if(item.WaktuID == null) return JsonHelper.JsonMsgInfo("Waktu belum dipilih");
                                    if(item.UntukDokterID == null) return JsonHelper.JsonMsgInfo("Dokter belum dipilih");
                                    if(item.UntukSectionID == null) return JsonHelper.JsonMsgInfo("Section belum dipilih");

                                    #region Shift
                                    var shift = "";
                                    var waktu = s.SIMmWaktuPraktek.FirstOrDefault(x => x.WaktuID == item.WaktuID);
                                    if (waktu.NamaWaktu.Contains("PAGI") == true || waktu.NamaWaktu.Contains("SIANG") == true)
                                    {
                                        shift = "Pagi";
                                    }
                                    else
                                    {
                                        shift = "Sore";
                                    }
                                    #endregion

                                    var waktupraktek = s.SIMmWaktuPraktek.FirstOrDefault(z => z.WaktuID == item.WaktuID);

                                    //checked Header
                                    var jadwalheader = s.SIMtrDokterJaga.Where(x => x.DokterID == item.UntukDokterID && x.SectionID == item.UntukSectionID);
                                    if (jadwalheader.Count() > 0)
                                    {
                                        var jadwaldetail = s.SIMtrDokterJagaDetail.Where(x => x.DokterID == item.UntukDokterID && x.SectionID == item.UntukSectionID && x.Tanggal == item.UntukTanggal && x.WaktuID == item.WaktuID);
                                        if (jadwaldetail.Count() > 0)
                                        {
                                            return JsonHelper.JsonMsgError("Jadwal Sudah Ada");
                                        }
                                        else
                                        {
                                            #region Detail
                                            var d = new SIMtrDokterJagaDetail()
                                            {
                                                DokterID = item.UntukDokterID,
                                                SectionID = item.UntukSectionID,
                                                Tanggal = item.UntukTanggal,
                                                PublishReservasiFromJam = new DateTime(item.UntukTanggal.Year, item.UntukTanggal.Month, item.UntukTanggal.Day, waktupraktek.FromJam.Hour, waktupraktek.FromJam.Minute, 0),
                                                PublishReservasiToJam = new DateTime(item.UntukTanggal.Year, item.UntukTanggal.Month, item.UntukTanggal.Day, waktupraktek.ToJam.Hour, waktupraktek.ToJam.Minute, 0),
                                                PublishRegistrasiFromJam = new DateTime(item.UntukTanggal.Year, item.UntukTanggal.Month, item.UntukTanggal.Day, waktupraktek.FromJam.Hour, waktupraktek.FromJam.Minute, 0),
                                                PublishRegistrasiToJam = new DateTime(item.UntukTanggal.Year, item.UntukTanggal.Month, item.UntukTanggal.Day, waktupraktek.ToJam.Hour, waktupraktek.ToJam.Minute, 0),
                                                FromJam = new DateTime(item.UntukTanggal.Year, item.UntukTanggal.Month, item.UntukTanggal.Day, waktupraktek.FromJam.Hour, waktupraktek.FromJam.Minute, 0),
                                                ToJam = new DateTime(item.UntukTanggal.Year, item.UntukTanggal.Month, item.UntukTanggal.Day, waktupraktek.ToJam.Hour, waktupraktek.ToJam.Minute, 0),
                                                Cancel = false,
                                                JmlAntrian = 0,
                                                NoAntrianTerakhir = 0,
                                                Realisasi = true,
                                                DokterPenggantiID = null,
                                                KeteranganStatusPraktek = null,
                                                NoAntrianSaatIni = 0,
                                                StatusPraktek = "Available",
                                                Verified = true,
                                                Hari = item.UntukHari,
                                                WaktuID = item.WaktuID.Value,
                                                NoRuang = "1",
                                                Pagi = shift == "Pagi" ? true : false,
                                                JadwalManual = true
                                                
                                            };

                                            s.SIMtrDokterJagaDetail.Add(d);
                                            resultJadwal = new ResultSS(s.SaveChanges());
                                            #endregion

                                        }
                                    }
                                    else
                                    {
                                        #region Header
                                        s.SIMtrDokterJaga.Add(new SIMtrDokterJaga()
                                        {
                                            DokterID = item.UntukDokterID,
                                            SectionID = item.UntukSectionID,
                                            Senen = item.UntukHari == "Senin" ? true : false,
                                            Selasa = item.UntukHari == "Selasa" ? true : false,
                                            Rabu = item.UntukHari == "Rabu" ? true : false,
                                            Kamis = item.UntukHari == "Kamis" ? true : false,
                                            Jumat = item.UntukHari == "Jumat" ? true : false,
                                            Sabtu = item.UntukHari == "Sabtu" ? true : false,
                                            Minggu = item.UntukHari == "Minggu" ? true : false,
                                            Senin_WaktuPagiID = item.UntukHari == "Senin" && shift == "Pagi" ? item.WaktuID : 1,
                                            Senin_WaktuSoreID = item.UntukHari == "Senin" && shift == "Sore" ? item.WaktuID : 1,
                                            Selasa_WaktuPagiID = item.UntukHari == "Senin" && shift == "Pagi" ? item.WaktuID : 1,
                                            Selasa_WaktuSoreID = item.UntukHari == "Selasa" && shift == "Pagi" ? item.WaktuID : 1,
                                            Rabu_WaktuPagiID = item.UntukHari == "Rabu" && shift == "Pagi" ? item.WaktuID : 1,
                                            Rabu_WaktuSoreID = item.UntukHari == "Rabu" && shift == "Sore" ? item.WaktuID : 1,
                                            Kamis_WaktuPagiID = item.UntukHari == "Kamis" && shift == "Pagi" ? item.WaktuID : 1,
                                            Kamis_WaktuSoreID = item.UntukHari == "Kamis" && shift == "Sore" ? item.WaktuID : 1,
                                            Jumat_WaktuPagiID = item.UntukHari == "Jumat" && shift == "Pagi" ? item.WaktuID : 1,
                                            Jumat_WaktuSoreID = item.UntukHari == "Jumat" && shift == "Sore" ? item.WaktuID : 1,
                                            Sabtu_WaktuPagiID = item.UntukHari == "Sabtu" && shift == "Pagi" ? item.WaktuID : 1,
                                            Sabtu_WaktuSoreID = item.UntukHari == "Sabtu" && shift == "Sore" ? item.WaktuID : 1,
                                            Minggu_WaktuPagiID = item.UntukHari == "Minggu" && shift == "Pagi" ? item.WaktuID : 1,
                                            Minggu_WaktuSoreID = item.UntukHari == "Mingggu" && shift == "Sore" ? item.WaktuID : 1,
                                            FromJam = null,
                                            ToJam = null,
                                            Pagi = shift == "Pagi" ? true : false,
                                            Sore = shift == "Sore" ? true : false,
                                            Tanggal = item.UntukTanggal,
                                            JmlAntrian = null
                                        });
                                        #endregion

                                        #region Detail

                                        var d = new SIMtrDokterJagaDetail()
                                        {
                                            DokterID = item.UntukDokterID,
                                            SectionID = item.UntukSectionID,
                                            Tanggal = item.UntukTanggal,
                                            PublishReservasiFromJam = new DateTime(item.UntukTanggal.Year, item.UntukTanggal.Month, item.UntukTanggal.Day, waktupraktek.FromJam.Hour, waktupraktek.FromJam.Minute, 0),
                                            PublishReservasiToJam = new DateTime(item.UntukTanggal.Year, item.UntukTanggal.Month, item.UntukTanggal.Day, waktupraktek.ToJam.Hour, waktupraktek.ToJam.Minute, 0),
                                            PublishRegistrasiFromJam = new DateTime(item.UntukTanggal.Year, item.UntukTanggal.Month, item.UntukTanggal.Day, waktupraktek.FromJam.Hour, waktupraktek.FromJam.Minute, 0),
                                            PublishRegistrasiToJam = new DateTime(item.UntukTanggal.Year, item.UntukTanggal.Month, item.UntukTanggal.Day, waktupraktek.ToJam.Hour, waktupraktek.ToJam.Minute, 0),
                                            FromJam = new DateTime(item.UntukTanggal.Year, item.UntukTanggal.Month, item.UntukTanggal.Day, waktupraktek.FromJam.Hour, waktupraktek.FromJam.Minute, 0),
                                            ToJam = new DateTime(item.UntukTanggal.Year, item.UntukTanggal.Month, item.UntukTanggal.Day, waktupraktek.ToJam.Hour, waktupraktek.ToJam.Minute, 0),
                                            Cancel = false,
                                            JmlAntrian = 0,
                                            NoAntrianTerakhir = 0,
                                            Realisasi = true,
                                            DokterPenggantiID = null,
                                            KeteranganStatusPraktek = null,
                                            NoAntrianSaatIni = 0,
                                            StatusPraktek = "Available",
                                            Verified = true,
                                            Hari = item.UntukHari,
                                            WaktuID = item.WaktuID.Value,
                                            NoRuang = "1",
                                            Pagi = shift == "Pagi" ? true : false,
                                            JadwalManual = true
                                        };

                                        s.SIMtrDokterJagaDetail.Add(d);
                                        resultJadwal = new ResultSS(s.SaveChanges());
                                        #endregion
                                    }

                                    if (resultJadwal.IsSuccess)
                                    {
                                        #region Insert Reservasi
                                        var insert = s.SimTrReservasi_Insert(
                                            DateTime.Today,
                                            DateTime.Now,
                                            item.PasienBaru,
                                            item.NRM ?? "",
                                            item.Nama,
                                            item.Alamat,
                                            item.Phone,
                                            item.UntukSectionID,
                                            item.UntukDokterID,
                                            item.UntukHari,
                                            item.UntukTanggal,
                                            item.UntukTanggal,
                                            0,
                                            490,
                                            false,
                                            null,
                                            item.WaktuID,
                                            item.JenisKerjasamaID,
                                            null,
                                            "",
                                            null,
                                            null,
                                            null,
                                            false,
                                            false,
                                            null,
                                            null,
                                            "",
                                            false,
                                            false,
                                            "",
                                            null,
                                            "",
                                            "",
                                            ""
                                           ).ToString().FirstOrDefault();

                                        result = new ResultSS(1, insert);
                                        #endregion
                                    }
                                    else
                                    {
                                        return JsonHelper.JsonMsgCustom(resultJadwal, resultJadwal.Message);
                                    }
                                }
                                else
                                {
                                    if (item.WaktuID == null) return JsonHelper.JsonMsgInfo("Jadwal belum dipilih");

                                    #region Insert Reservasi
                                    var insert = s.SimTrReservasi_Insert(
                                            DateTime.Today,
                                            DateTime.Now,
                                            item.PasienBaru,
                                            item.NRM ?? "",
                                            item.Nama,
                                            item.Alamat,
                                            item.Phone,
                                            item.UntukSectionID,
                                            item.UntukDokterID,
                                            item.UntukHari,
                                            item.UntukTanggal,
                                            item.UntukTanggal,
                                            0,
                                            490,
                                            false,
                                            null,
                                            item.WaktuID,
                                            item.JenisKerjasamaID,
                                            null,
                                            "",
                                            null,
                                            null,
                                            null,
                                            false,
                                            false,
                                            null,
                                            null,
                                            "",
                                            false,
                                            false,
                                            "",
                                            null,
                                            "",
                                            "",
                                            ""
                                           ).ToString().FirstOrDefault();

                                    result = new ResultSS(1, insert);
                                    #endregion

                                }

                                #region Update Status Reservasi
                                if (!string.IsNullOrEmpty(item.NoBukti))
                                {
                                    using (var s2 = new SIM_Entities())
                                    {
                                        var jadwalkontrol = s2.SIMtrFormKontrolPasien.FirstOrDefault(x => x.NoBukti == item.NoBukti);
                                        if (jadwalkontrol != null)
                                        {
                                            jadwalkontrol.SudahReservasi = true;
                                            s2.SaveChanges();
                                        }
                                    }
                                }
                                #endregion

                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Reservasi Create {item.NRM ?? item.Nama}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                var msgs = new List<string>();
                                foreach (var eve in ex.EntityValidationErrors)
                                {
                                    var msg = $"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors:";
                                    foreach (var ve in eve.ValidationErrors) { msg += $"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\""; }
                                    msgs.Add(msg);
                                }
                                throw new Exception(string.Join(", ", msgs));
                            }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string id)
        {
            ReservasiViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Vw_Reservasi.FirstOrDefault(x => x.NoReservasi == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<ReservasiViewModel>(m);
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post(string id)
        {
            try
            {
                var item = new DesaViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var model = s.mDesa.FirstOrDefault(x => x.DesaID == item.DesaID);
                        if (model == null) throw new Exception("Data Tidak ditemukan");

                        if (model.DesaNama.ToUpper() != item.DesaNama.ToUpper())
                        {
                            var has = s.mDesa.Where(x => x.DesaNama == item.DesaNama).Count() > 0;
                            if (has) throw new Exception("Nama sudah digunakan");
                        }

                        TryUpdateModel(model);
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Desa Edit {model.DesaID}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== D E T A I L
        [HttpGet]
        public ActionResult Detail(string id)
        {
            ReservasiViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Vw_Reservasi.FirstOrDefault(x => x.NoReservasi == id);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<ReservasiViewModel>(m);
                    item.UntukTanggal_View = item.UntukTanggal.ToString("dd/MM/yyyy");
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
        #endregion

        #region ===== B A T A L

        [HttpPost]
        public string Batal(string id, string alasan)
        {
            try
            {
                ResultSS result;

                using (var s = new SIM_Entities())
                {
                    var batal = s.BatalReservasiDenganAsalan(id, true, alasan);
                    result = new ResultSS(1, batal);
                }
                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                {
                    Activity = $"Reservasi Batal {id}"
                };
                UserActivity.InsertUserActivity(userActivity);

                return JsonHelper.JsonMsgCustom(result, "Dibatalkan");
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        [HttpPost]
        [ActionName("SingkronisasiNRM")]
        [ValidateAntiForgeryToken]
        public string SingkronisasiNRM(ReservasiViewModel item)
        {
            try
            {
                //var item = new ReservasiViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    var result = new ResultSS();
                    using (var s = new SIM_Entities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var res = s.SIMtrReservasi.FirstOrDefault(x => x.NoReservasi == item.NoReservasi);
                                if(res == null) return JsonHelper.JsonMsgError("Data Reservasi tidak ditemukan");

                                res.NRM = item.NRM;
                                res.Nama = item.Nama;
                                res.Alamat = item.Alamat;
                                res.Phone = item.Phone;
                                res.PasienBaru = false;

                                var insert = s.SaveChanges();
                                result = new ResultSS(1, insert);

                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Reservasi Singkronisasi {item.NRM ?? item.Nama}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                var msgs = new List<string>();
                                foreach (var eve in ex.EntityValidationErrors)
                                {
                                    var msg = $"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors:";
                                    foreach (var ve in eve.ValidationErrors) { msg += $"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\""; }
                                    msgs.Add(msg);
                                }
                                throw new Exception(string.Join(", ", msgs));
                            }
                        }
                    }
                    return JsonHelper.JsonMsgCreate(result);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }


    }
}