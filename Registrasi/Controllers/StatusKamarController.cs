﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Registrasi.Entities;
using Registrasi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Registrasi.Controllers
{
    [Authorize(Roles = "Registrasi")]
    public class StatusKamarController : Controller
    {
        #region ===== I N D E X

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string nokamar, string nobed)
        {
            StatusKamarViewModel item;
            try
            {
                using (var s = new SIM_Entities())
                {
                    var m = s.Pelayanan_Informasi_StatusKamar.FirstOrDefault(x => x.NoBed == nobed && x.NoKamar == nokamar);
                    if (m == null) return HttpNotFound();
                    if (m.IDStatus == "AV" && m.IDStatus == "VD")
                    {
                        ViewBag.LockEdit = true;
                    }
                    else
                    {
                        ViewBag.LockEdit = false;
                    }
                    //ViewBag.LockEdit = m.IDStatus != "CL" && m.IDStatus != "RV" && m.IDStatus != "AV" && m.IDStatus == "VD";
                    item = IConverter.Cast<StatusKamarViewModel>(m);
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post()
        {
            try
            {
                var item = new StatusKamarViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIM_Entities())
                    {
                        var model = s.mKamarDetail.FirstOrDefault(x => x.NoBed == item.NoBed && x.NoKamar == item.NoKamar);
                        if (model == null) throw new Exception("Data Tidak ditemukan");
                        if (model.Status == "AV" && model.Status == "VD")
                        {
                            throw new Exception("Status kamar tidak boleh di ubah");
                        }


                        model.Status = item.IDStatus;
                        result = new ResultSS(s.SaveChanges());

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Memo Status Kamar {model.NoKamar} {model.NoBed} {model.Status}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (DbEntityValidationException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Pelayanan_Informasi_StatusKamar> proses = s.Pelayanan_Informasi_StatusKamar;
                    if (!string.IsNullOrEmpty(filter[0]))
                    {
                        proses = proses.Where($"{nameof(Pelayanan_Informasi_StatusKamar.SectionID)}=@0", filter[0]);
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<StatusKamarViewModel>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== S T A T U S  K A M A R 
        [HttpPost]
        public string ListStatusKamar(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMmSection> proses = s.SIMmSection.Where(x => x.RIOnly == true && x.StatusAktif == true);
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(SIMmSection.SectionID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(SIMmSection.SectionName)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<SectionViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion


        //#region ===== I N D E X

        //[HttpGet]
        //public ActionResult Index()
        //{
        //    return View();
        //}

        //#endregion

        //#region ===== E D I T

        //[HttpGet]
        //[ActionName("Edit")]
        //public ActionResult Edit_Get(string nokamar, string nobed)
        //{
        //    StatusKamarViewModel item;
        //    try
        //    {
        //        using (var s = new SIM_Entities())
        //        {
        //            var m = s.Pelayanan_Informasi_StatusKamar.FirstOrDefault(x => x.NoBed == nobed && x.NoKamar == nokamar);
        //            if (m == null) return HttpNotFound();
        //            ViewBag.LockEdit = m.IDStatus != "CL" && m.IDStatus != "RV" && m.IDStatus != "AV" && m.IDStatus == "VD";
        //            item = IConverter.Cast<StatusKamarViewModel>(m);
        //        }
        //    }
        //    catch (SqlException ex) { throw new Exception(ex.Message); }
        //    catch (Exception ex) { throw new Exception(ex.Message); }

        //    ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
        //    if (Request.IsAjaxRequest())
        //        return PartialView(item);
        //    else
        //        return View(item);
        //}

        //[HttpPost]
        //[ActionName("Edit")]
        //[ValidateAntiForgeryToken]
        //public string Edit_Post()
        //{
        //    try
        //    {
        //        var item = new StatusKamarViewModel();
        //        TryUpdateModel(item);

        //        if (ModelState.IsValid)
        //        {
        //            ResultSS result;
        //            using (var s = new SIM_Entities())
        //            {
        //                var model = s.mKamarDetail.FirstOrDefault(x => x.NoBed == item.NoBed && x.NoKamar == item.NoKamar);
        //                if (model == null) throw new Exception("Data Tidak ditemukan");
        //                if (model.Status != "CL" && model.Status != "RV" && model.Status != "AV" && model.Status == "VD")
        //                {
        //                    throw new Exception("Status kamar tidak boleh di ubah");
        //                }

        //                model.Status = item.IDStatus;
        //                result = new ResultSS(s.SaveChanges());

        //                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
        //                {
        //                    Activity = $"Memo Status Kamar {model.NoKamar} {model.NoBed} {model.Status}"
        //                };
        //                UserActivity.InsertUserActivity(userActivity);
        //            }
        //            return JsonHelper.JsonMsgEdit(result, -1);
        //        }
        //        else
        //            return JsonHelper.JsonMsgError(ViewData);
        //    }
        //    catch (DbEntityValidationException ex) { return JsonHelper.JsonMsgError(ex); }
        //    catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
        //    catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        //}

        //#endregion

        //#region ===== T A B L E

        //[HttpPost]
        //public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        //{
        //    try
        //    {
        //        ResultSS result;
        //        using (var s = new SIM_Entities())
        //        {
        //            IQueryable<Pelayanan_Informasi_StatusKamar> proses = s.Pelayanan_Informasi_StatusKamar;
        //            if (!string.IsNullOrEmpty(filter[0]))
        //            {
        //                proses = proses.Where($"{nameof(Pelayanan_Informasi_StatusKamar.SectionID)}=@0", filter[0]);
        //            }
        //            var totalcount = proses.Count();
        //            var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
        //                .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
        //            result = new ResultSS(models.Length, models, totalcount, pageIndex);
        //            var m = models.ToList().ConvertAll(x => IConverter.Cast<StatusKamarViewModel>(x));
        //            result.Data = m;
        //        }
        //        return JsonConvert.SerializeObject(new TableList(result));
        //    }
        //    catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
        //    catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        //}

        //#endregion
    }
}