﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using LZStringCSharp;
using Newtonsoft.Json;
using Registrasi.Entities;
using Registrasi.Entities.BPJS;
using Registrasi.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using static Registrasi.Models.BpjsModel;

namespace Registrasi.Controllers
{
    [Authorize(Roles = "Registrasi")]
    public class LookUpController : Controller
    {
        #region ==== K A R T U
        [HttpPost]
        public string ListKartu(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var jenisKerjasamaID = (filter[4] == null) ? "xx" : filter[4];
                    var jenisKerjasama = s.SIMmJenisKerjasama.Where(xx => xx.JenisKerjasamaID.ToString() == jenisKerjasamaID).FirstOrDefault();
                    IQueryable<VW_CustomerKerjasamaAnggota> proses = s.VW_CustomerKerjasamaAnggota;
                    proses = proses.Where($"{nameof(VW_CustomerKerjasamaAnggota.NoAnggota)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(VW_CustomerKerjasamaAnggota.Nama)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(VW_CustomerKerjasamaAnggota.Klp)}.Contains(@0)", filter[2]);
                    if(jenisKerjasama != null)
                    {
                        proses = proses.Where($"{nameof(VW_CustomerKerjasamaAnggota.JenisKerjasama)}.Contains(@0)", jenisKerjasama.JenisKerjasama ?? "");
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<KartuViewModel>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== P A S I E N  A L L
        [HttpPost]
        public string ListPasienAll(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Vw_Pasien> proses = s.Vw_Pasien;
                    proses = proses.Where($"{nameof(Vw_Pasien.NRM)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(Vw_Pasien.NamaPasien)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(Vw_Pasien.Alamat)}.Contains(@0)", filter[2]);
                    proses = proses.Where($"{nameof(Vw_Pasien.Phone)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4]))
                    {
                        short umur = IFilter.F_short(filter[4]) ?? 0;
                        proses = proses.Where(x => x.UmurSaatInput == umur);
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<PasienViewModel>(x));

                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== P A S I E N  R E S E R V A S I
        [HttpPost]
        public string ListPasienReservasi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Vw_Pasien> proses = s.Vw_Pasien;
                    proses = proses.Where($"{nameof(Vw_Pasien.NRM)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(Vw_Pasien.NamaPasien)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(Vw_Pasien.Alamat)}.Contains(@0)", filter[2]);
                    proses = proses.Where($"{nameof(Vw_Pasien.Phone)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4]))
                    {
                        short umur = IFilter.F_short(filter[4]) ?? 0;
                        proses = proses.Where(x => x.UmurSaatInput == umur);
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PasienViewModel>(x));
                    foreach (var x in m)
                    {
                        x.TglLahir_View = x.TglLahir.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== P A S I E N  R E G I S T R A S I
        [HttpPost]
        public string ListPasienRegistrasi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Vw_Pasien> proses = s.Vw_Pasien;
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Vw_Pasien.NRM)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Vw_Pasien.NamaPasien)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Vw_Pasien.Alamat)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Vw_Pasien.Phone)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4]))
                    {
                        short umur = IFilter.F_short(filter[4]) ?? 0;
                        proses = proses.Where(x => x.UmurSaatInput == umur);
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<PasienViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== T U J U A N
        [HttpPost]
        public string ListTujuanReservasi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<JadwalPraktekDokter_GetData> proses = s.JadwalPraktekDokter_GetData.Where(x => x.StatusAktif == true
                    && (x.JadwalCancel == false || (x.JadwalCancel == true && x.DokterID != null))
                    && x.TipePelayanan == "RJ"
                    && x.DokterAktif == true
                    && x.JadwalVerified == true
                    );
                    if (!string.IsNullOrEmpty(filter[22]))
                    {
                        proses = proses.Where("Tanggal = @0", DateTime.Parse(filter[22]).Date);
                    }
                    proses = proses.Where($"{nameof(JadwalPraktekDokter_GetData.NamaSection)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(JadwalPraktekDokter_GetData.NamaDOkter)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(JadwalPraktekDokter_GetData.Keterangan)}.Contains(@0)", filter[2]);
                    proses = proses.Where($"{nameof(JadwalPraktekDokter_GetData.Hari)}.Contains(@0)", filter[4]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<TujuanReservasiViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        x.UntukTanggal = x.Tanggal.ToString("yyyy-MM-dd");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== R E S E R V A S I
        [HttpPost]
        public string ListReservasi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Vw_Reservasi> proses = s.Vw_Reservasi.Where(x => x.Registrasi == false && x.Batal == false);
                    if (filter[24] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[22]))
                        {
                            proses = proses.Where("UntukTanggal = @0", DateTime.Parse(filter[22]).Date);
                        }
                    }
                    proses = proses.Where($"{nameof(Vw_Reservasi.NoReservasi)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(Vw_Reservasi.NRM)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(Vw_Reservasi.Nama)}.Contains(@0)", filter[2]);
                    proses = proses.Where($"{nameof(Vw_Reservasi.SectionName)}.Contains(@0)", filter[3]);

                    if (filter[4] != null && filter[4] != "")
                    {
                        int nourut = IFilter.F_int(filter[4]).Value;
                        proses = proses.Where(x => x.NoUrut == nourut);
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<ReservasiViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_View = x.Tanggal.ToString("dd/MM/yyyy");
                        x.UntukTanggal_View = x.UntukTanggal.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== P E R U S A H A A N
        [HttpPost]
        public string ListPerusahaan(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<VW_CustomerNew> proses = s.VW_CustomerNew;
                    proses = proses.Where($"{nameof(VW_CustomerNew.Nama_Customer)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2]))
                    {
                        int jenisKerjasama = IFilter.F_int(filter[2]).Value;
                        proses = proses.Where(x => x.JenisKerjasamaID == jenisKerjasama);
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<CustomerViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== P E R U S A H A A N
        [HttpPost]
        public string ListCustomerKerjasama(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<VW_CustomerKerjasama> proses = s.VW_CustomerKerjasama;
                    proses = proses.Where($"{nameof(VW_CustomerKerjasama.Kode_Customer)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(VW_CustomerKerjasama.Nama_Customer)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(VW_CustomerKerjasama.NamaKelas)}.Contains(@0)", filter[2]);
                    proses = proses.Where($"{nameof(VW_CustomerKerjasama.JenisKerjasama)}.Contains(@0)", filter[3]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<CustomerViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== P E N A N G G U N G
        [HttpPost]
        public string ListPenanggung(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<Vw_Pasien> proses = s.Vw_Pasien;
                    proses = proses.Where($"{nameof(Vw_Pasien.NRM)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(Vw_Pasien.NamaPasien)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(Vw_Pasien.Alamat)}.Contains(@0)", filter[2]);
                    proses = proses.Where($"{nameof(Vw_Pasien.Phone)}.Contains(@0)", filter[3]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<PasienViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== S E C T I O N  A L L
        [HttpPost]
        public string ListSectionAll(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMmSection> proses = s.SIMmSection.Where(x => x.StatusAktif == true && (x.TipePelayanan == "RJ" || x.TipePelayanan == "RI" || x.TipePelayanan == "PENUNJANG" || x.TipePelayanan == "PENUNJANG2") && x.RIOnly == false);
                    proses = proses.Where($"{nameof(SIMmSection.SectionID)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(SIMmSection.SectionName)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<SectionViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListSectionReservasi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMmSection> proses = s.SIMmSection.Where(x => x.StatusAktif == true && (x.TipePelayanan == "RJ") && (x.PoliKlinik == "UMUM" || x.PoliKlinik == "SPESIALIS"));
                    proses = proses.Where($"{nameof(SIMmSection.SectionID)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(SIMmSection.SectionName)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<SectionViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== W A K T U  P R A K T E K
        [HttpPost]
        public string ListWaktu(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMmWaktuPraktek> proses = s.SIMmWaktuPraktek.Where(x=> x.WaktuID != 1);
                    proses = proses.Where($"{nameof(SIMmWaktuPraktek.Keterangan)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(SIMmWaktuPraktek.NamaWaktu)}.Contains(@0)", filter[2]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<WaktuPraktekViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== S E C T I O N  R I
        [HttpPost]
        public string ListSectionRI(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMmSection> proses = s.SIMmSection.Where(x => x.RIOnly == true && x.StatusAktif == true);
                    proses = proses.Where($"{nameof(SIMmSection.SectionName)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<SIMmSection>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== T U J U A N
        [HttpPost]
        public string ListTujuanRegistrasi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<JadwalPraktekDokter_GetData> proses = s.JadwalPraktekDokter_GetData.Where(x => x.StatusAktif == true
                    && (x.JadwalCancel == false || (x.JadwalCancel == true && x.DokterID != null))
                    && x.TipePelayanan == "RJ"
                    && x.DokterAktif == true
                    && x.JadwalVerified == true
                    );

                    proses = proses.Where(x => x.Tanggal == DateTime.Today.Date);
                    proses = proses.Where($"{nameof(JadwalPraktekDokter_GetData.SectionID)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(JadwalPraktekDokter_GetData.NamaSection)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(JadwalPraktekDokter_GetData.NamaDOkter)}.Contains(@0)", filter[2]);
                    proses = proses.Where($"{nameof(JadwalPraktekDokter_GetData.Keterangan)}.Contains(@0)", filter[3]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<TujuanViewModel>(x));
                    foreach (var x in m)
                    {
                        x.Tanggal_view = x.Tanggal.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== S U P P L I E R
        [HttpPost]
        public string ListSupplier(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mSupplier> proses = s.mSupplier;
                    proses = proses.Where($"{nameof(mSupplier.Nama_Supplier)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<SupplierViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== D O K T E R
        [HttpPost]
        public string ListDokter(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mDokter> proses = s.mDokter.Where(x => x.Active == true);
                    proses = proses.Where($"{nameof(mDokter.DokterID)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(mDokter.NamaDOkter)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(mDokter.Alamat)}.Contains(@0)", filter[2]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<DokterViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string ListDokterPraktek(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mDokter> proses = s.mDokter.Where(x => x.Active == true && x.DokterPraktek == true);
                    proses = proses.Where($"{nameof(mDokter.DokterID)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(mDokter.NamaDOkter)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(mDokter.Alamat)}.Contains(@0)", filter[2]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<DokterViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== P A K E T
        public string ListPaket(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMmListJasa> proses = s.SIMmListJasa;
                    proses = proses.Where($"{nameof(SIMmListJasa.JasaName)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<PaketViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== A S U R A N S I
        [HttpPost]
        public string ListAsuransi(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mCustomer> proses = s.mCustomer;
                    proses = proses.Where($"{nameof(mCustomer.Nama_Customer)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<AsuransiViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== V E N D O R
        [HttpPost]
        public string ListVendor(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mSupplier> proses = s.mSupplier;
                    proses = proses.Where($"{nameof(mSupplier.Nama_Supplier)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<SupplierViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== K A M A R
        [HttpPost]
        public string ListKamar(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    var SectionBayi = ConfigurationManager.AppSettings["SectionBayi"];
                    IQueryable<Vw_Kamar> proses = s.Vw_Kamar;
                    string salID = filter[2];
                    if (salID != SectionBayi)
                    {
                        if (filter[5] == "1")
                        {
                            proses = s.Vw_Kamar.Where(x => (x.Status == "FD" || x.Status == "AV") && x.SalID == salID);
                        }
                        else
                        {
                            proses = s.Vw_Kamar.Where(x => (x.Status == "AV") && x.SalID == salID);
                        }
                        proses = proses.Where($"{nameof(Vw_Kamar.KelasID)}.Contains(@0)", filter[4]);
                    }
                    else
                    {
                        proses = s.Vw_Kamar.Where(x => x.SalID == salID);
                    }
                    proses = proses.Where($"{nameof(Vw_Kamar.NamaKamar)}.Contains(@0)", filter[10]);
                    proses = proses.Where($"{nameof(Vw_Kamar.NoKamar)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(Vw_Kamar.NoBed)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[3]))
                    {
                        proses = proses.Where(x => x.NoLantai == (byte?)IFilter.F_int(filter[3]));
                    }
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<KamarViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== J A S A
        public string ListJasa(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<SIMmListJasa> proses = s.SIMmListJasa;
                    proses = proses.Where($"{nameof(SIMmListJasa.JasaName)}.Contains(@0)", filter[0]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<JasaViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region GET DATA PASIEN
        [HttpGet]
        public string GetDataPasienRegistrasi(string id)
        {
            VW_Pasien_Regional m;
            var result = new RegistrasiInsertViewModel();
            using (var s = new SIM_Entities())
            {
                m = s.VW_Pasien_Regional.FirstOrDefault(x => x.NRM == id);
                if (m != null)
                {
                    var reg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NRM == m.NRM && x.Batal != true && x.StatusBayar != "Sudah Bayar");
                    if(m.SedangDirawat != true && reg == null)
                    {
                        result.IsSuccess = true;
                        result.NRM = m.NRM;
                        result.NamaPasien = m.NamaPasien;
                        result.JenisKelamin = m.JenisKelamin;
                        result.Umur = m.UmurSaatInput;
                        result.Alamat = m.Alamat;
                        result.Phone = m.Phone;
                        result.Kelas = m.NamaKelas;
                        result.JenisPasienKerjasamaID = m.JenisKerjasamaID;
                        result.NoKartu = m.NoKartu;
                        result.Nama_Customer = m.Nama_Customer;
                        result.PenanggungNRM = m.PenanggungNRM;
                        result.PenanggungNama = m.PenanggungNama;
                        result.PenanggungAlamat = m.PenanggungAlamat;
                        result.PenanggungPhone = m.PenanggungPhone;
                        result.PenanggungHubungan = m.PenanggungHubungan;
                        result.PasienRS = m.PenanggungIsPasien ?? false;
                        result.PasienKTP = m.PasienKTP;
                        result.Kode_Customer = m.CompanyID;
                        result.KelasID = m.KdKelas;
                        result.NoIdentitas = m.NoIdentitas;
                        if (result.KelasID == null || result.KelasID == "")
                        {
                            var kelas = s.VW_CustomerKerjasama.FirstOrDefault(e => e.CustomerKerjasamaID == m.CustomerKerjasamaID);
                            if (kelas != null)
                            {
                                result.KelasID = kelas.KelasID;
                            }
                        }
                    }
                    else
                    {
                        result.IsSuccess = false;
                        result.Message = "Pasien sudah di registrasi";
                    }
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = "Data pasien Tidak ditemukan";
                }

            }

            
            return JsonConvert.SerializeObject(result);
        }

        [HttpGet]
        public string GetDataPasienReservasi(string id)
        {
            VW_Pasien_Regional m;
            using (var s = new SIM_Entities())
            {
                m = s.VW_Pasien_Regional.FirstOrDefault(x => x.NRM == id);
                if (m == null) throw new Exception("Data Pasien tidak ditemukan");
            }
            var result = new ReservasiViewModel
            {
                Nama = m.NamaPasien,
                Alamat = m.Alamat,
                Phone = m.Phone,
                JenisKerjasamaID = m.JenisKerjasamaID
            };
            return JsonConvert.SerializeObject(result);
        }

        [HttpGet]
        public string GetDataPasienAll(string id)
        {
            Vw_Pasien m;
            using (var s = new SIM_Entities())
            {
                m = s.Vw_Pasien.FirstOrDefault(x => x.NRM == id);
                if (m == null) throw new Exception("Data Pasien tidak ditemukan");
            }
            var result = new PasienBermasalahViewModel
            {
                NamaPasien = m.NamaPasien
            };
            return JsonConvert.SerializeObject(result);
        }
        #endregion

        #region GET DATA PENANGGUNG
        [HttpGet]
        public string GetDataPenanggung(string id)
        {
            Vw_Pasien m;
            var result = new RegistrasiInsertViewModel();
            using (var s = new SIM_Entities())
            {
                m = s.Vw_Pasien.FirstOrDefault(x => x.NRM == id);
                if(m != null)
                {
                    result.PenanggungNRM = m.NRM;
                    result.PenanggungNama = m.NamaPasien;
                    result.PenanggungAlamat = m.Alamat;
                    result.PenanggungPhone = m.Phone;
                    result.IsSuccess = true;
                } else
                {
                    result.IsSuccess = false;
                }
                
            }
            
            return JsonConvert.SerializeObject(result);
        }
        #endregion

        #region GET DATA PENANGGUNG SEBELUM
        [HttpGet]
        public string GetDataPenanggungSebelum(string id)
        {
            var result = new RegistrasiInsertViewModel();
            Registrasi_PenanggungPasien_Get_Result m;
            using (var s = new SIM_Entities())
            {
                m = s.Registrasi_PenanggungPasien_Get(id).FirstOrDefault();
            }

            if (m != null)
            {
                result.PenanggungNRM = m.PenanggungNRM;
                result.PenanggungNama = m.PenanggungNama;
                result.PenanggungAlamat = m.PenanggungAlamat;
                result.PenanggungPhone = m.PenanggungTelp;
                result.PenanggungHubungan = m.PenanggungHubungan;
                result.PasienRS = m.PenanggungIsPasien;
                result.IsSuccess = true;
            }
            else
            {
                result.IsSuccess = false;
            }

            return JsonConvert.SerializeObject(result);
        }
        #endregion

        #region GET DATA SEP
        [HttpGet]
        public string GetDataSEP(string nokartu)
        {
            string noSEP = "";
            using (var s = new BPJSEntities())
            {
                var m = s.BPJStrSEPVClaim.OrderByDescending(x => x.TglSEP).FirstOrDefault(x => x.NoKartu == nokartu && x.Status == "BELUM");
                if (m != null)
                {
                    noSEP = m.NoSEP;
                }
            }

            return JsonConvert.SerializeObject(new
            {
                IsSuccess = true,
                Data = noSEP
            });
        }

        [HttpGet]
        public string GetDataReservasi(string id)
        {
            var result = new RegistrasiInsertViewModel();

            using (var s = new SIM_Entities())
            {
                var m = s.Vw_Reservasi.FirstOrDefault(x => x.NoReservasi == id && x.Registrasi == false && x.Batal == false);
                if(m != null)
                {
                    result.NoReservasi = m.NoReservasi;
                    result.NRM = m.NRM;
                    result.NamaPasien = m.Nama;
                    result.Alamat = m.Alamat;
                    result.Phone = m.Phone;
                    result.SectionID = m.UntukSectionID;
                    result.NamaSection = m.SectionName;
                    result.Dokter_ID = m.UntukDokterID;
                    result.Dokter = m.NamaDOkter;
                    result.Tanggal = m.UntukTanggal;
                    result.WaktuPraktek_ID = m.WaktuID;
                    result.Waktu = m.Keterangan;
                    result.JenisPasienKerjasamaID = m.JenisKerjasamaID ?? 0;
                    result.IsSuccess = true;
                } else
                {
                    result.IsSuccess = false;
                    result.Message = "Reservasi Tidak ditemukan atau sudah Registrasi";
                }
            }

            return JsonConvert.SerializeObject(result);
        }
        #endregion

        [HttpGet]
        public string GetDataKartu(string id)
        {
            var result = new PasienViewModel();
            using (var s = new SIM_Entities())
            {
                var m = s.VW_CustomerKerjasamaAnggota.FirstOrDefault(x => x.NoAnggota == id);
                result = new PasienViewModel
                {
                    NoAnggotaE = m.NoAnggota,
                    NamaAnggotaE = m.Nama,
                    Klp = m.Klp,
                    GenderAnggotaE = m.Gender,
                    KdKelas = m.KelasID
                };
            }

            return JsonConvert.SerializeObject(result);
        }

        //#region === R U J U K A N
        //[HttpPost]
        //public string ListRujukan(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        //{
        //    try
        //    {

        //        var s = new BpjsSEPService();
        //        RujukanListModel x;
        //        if (filter[1].Length != 13)
        //            return JsonHelper.JsonMsgError(new Exception("No Kartu harus 13 digit"));

        //        if (filter[0] == "2")
        //        {
        //            x = s.RujukanBerdasarkanNoKartuListRS(filter[1]);
        //        }
        //        else
        //        {
        //            x = s.RujukanBerdasarkanNoKartuListPCare(filter[1]);
        //        }

        //        if (x.metaData.code != "200")
        //            return JsonHelper.JsonMsgError(new Exception(x.metaData.message));

        //        var resultData = new List<RujukanViewModel>();
        //        foreach (var dt in x.response.rujukan)
        //        {
        //            string sectionID;
        //            using (var s1 = new BPJSEntities())
        //            {
        //                if (dt.poliRujukan.kode == "IGD")
        //                {
        //                    var m = s1.Mapping_SIMmSection.FirstOrDefault(y => y.MapingBPJSRujukan == "UGD");
        //                    sectionID = m.SectionID;
        //                }
        //                else
        //                {
        //                    var m = s1.Mapping_SIMmSection.FirstOrDefault(y => y.MapingBPJSRujukan == dt.poliRujukan.kode);
        //                    if (m == null)
        //                    {
        //                        sectionID = dt.poliRujukan.kode;
        //                    }
        //                    else
        //                    {
        //                        sectionID = m.SectionID;
        //                    }
        //                }

        //            }

        //            var diagnosa = s.ReferensiDiagnosa(dt.diagnosa.kode);
        //            var namadiagnosa = diagnosa.response.diagnosa.FirstOrDefault().nama;

        //            resultData.Add(new RujukanViewModel
        //            {
        //                NoRujukan = dt.noKunjungan,
        //                Nama = dt.peserta.nama,
        //                NoKartu = dt.peserta.noKartu,
        //                PPKPerujuk = dt.provPerujuk.kode,
        //                PPKPerujuk_Nama = dt.provPerujuk.nama,
        //                SubSpesialis = dt.poliRujukan.nama,
        //                TglRujukan_View = dt.tglKunjungan.ToString("dd/MM/yyyy"),
        //                TglRujukan = dt.tglKunjungan,
        //                Diagnosa = dt.diagnosa.kode,
        //                Diagnosa_Nama = namadiagnosa,
        //                SubSpesialisID = sectionID,
        //                NoTlp = dt.peserta.mr.noTelepon

        //            });
        //        }
        //        var resultSS = new ResultSS(resultData.Count, resultData.ToArray(), x.response.rujukan.Count, 0);
        //        resultSS.Data = resultData;
        //        var tblList = new TableList(resultSS);
        //        return JsonConvert.SerializeObject(tblList);

        //    }
        //    catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
        //    catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        //}
        //#endregion

        #region ==== D I A G N O S A
        [HttpPost]
        public string ListDiagnosa(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIM_Entities())
                {
                    IQueryable<mICD> proses = s.mICD;
                    //proses = proses.Where($"{nameof(mICD.KodeICD)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(mICD.Descriptions)}.Contains(@0)", filter[1]);
                    
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    result.Data = models.ToList().ConvertAll(x => IConverter.Cast<DiagnosaViewModel>(x));
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== API MODEL RUJUKAN DAN LIST LOOKUP
        // Lookup Rujukan di LookupController
        [HttpPost]
        public string ListRujukan(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {

                var s = new BpjsSEPService();
                RujukanListModel x;
                if (filter[1].Length != 13)
                    return JsonHelper.JsonMsgError(new Exception("No Kartu harus 13 digit"));

                var getrujukanfrom = 0;
                if (filter[0] == "2")
                {
                    x = s.RujukanBerdasarkanNoKartuListRS(filter[1]);
                    getrujukanfrom = 2;
                }
                else
                {
                    x = s.RujukanBerdasarkanNoKartuListPCare(filter[1]);
                    getrujukanfrom = 1;
                }

                if (x.metaData.code != "200")
                    return JsonHelper.JsonMsgError(new Exception(x.metaData.message));

                var resultData = new List<RujukanViewModel>();
                foreach (var dt in x.response.rujukan)
                {
                    string sectionID;
                    using (var s1 = new BPJSEntities())
                    {
                        if (dt.poliRujukan.kode == "IGD")
                        {
                            var m = s1.Mapping_SIMmSection.FirstOrDefault(y => y.MapingBPJSRujukan == "UGD");
                            sectionID = m.SectionID;
                        }
                        else
                        {
                            var m = s1.Mapping_SIMmSection.FirstOrDefault(y => y.MapingBPJSRujukan == dt.poliRujukan.kode);
                            if (m == null)
                            {
                                sectionID = dt.poliRujukan.kode;
                            }
                            else
                            {
                                sectionID = m.SectionID;
                            }
                        }

                    }

                    var diagnosa = s.ReferensiDiagnosa(dt.diagnosa.kode);
                    var namadiagnosa = diagnosa.response.diagnosa.FirstOrDefault().nama;

                    resultData.Add(new RujukanViewModel
                    {
                        NoRujukan = dt.noKunjungan,
                        Nama = dt.peserta.nama,
                        NoKartu = dt.peserta.noKartu,
                        PPKPerujuk = dt.provPerujuk.kode,
                        PPKPerujuk_Nama = dt.provPerujuk.nama,
                        SubSpesialis = dt.poliRujukan.nama,
                        TglRujukan_View = dt.tglKunjungan.ToString("dd/MM/yyyy"),
                        TglRujukan = dt.tglKunjungan,
                        Diagnosa = dt.diagnosa.kode,
                        Diagnosa_Nama = namadiagnosa,
                        SubSpesialisID = sectionID,
                        NoTlp = dt.peserta.mr.noTelepon,
                        NoNIK = dt.peserta.nik,
                        GetRujukanFrom = getrujukanfrom,
                        PoliRujukan = dt.poliRujukan.kode,
                        PoliRujukanNama = dt.poliRujukan.nama,
                    });
                }
                var resultSS = new ResultSS(resultData.Count, resultData.ToArray(), x.response.rujukan.Count, 0);
                resultSS.Data = resultData;
                var tblList = new TableList(resultSS);
                return JsonConvert.SerializeObject(tblList);

            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion
    }
}