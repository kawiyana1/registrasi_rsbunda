﻿using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class KecamatanViewModel
    {
        public string KecamatanID { get; set; }
        public string KecamatanNama { get; set; }
        public string Kode_Kabupaten { get; set; }
        public string Nama_Kabupaten { get; set; }
        public string Kode_Propinsi { get; set; }
        public string Nama_Propinsi { get; set; }
        public short PropinsiID { get; set; }
    }
}