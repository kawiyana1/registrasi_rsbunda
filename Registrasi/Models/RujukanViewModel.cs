﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class RujukanViewModel
    {
        public string NoRujukan { get; set; }

        [DataType(DataType.Date)]
        public DateTime TglRujukan { get; set; }
        public string TglRujukan_View { get; set; }

        public string NoKartu { get; set; }

        public string Nama { get; set; }

        public string PPKPerujuk { get; set; }

        public string PPKPerujuk_Nama { get; set; }

        public string SubSpesialisID { get; set; }

        public string SubSpesialis { get; set; }

        public string Diagnosa { get; set; }

        public string Diagnosa_Nama { get; set; }

        public string NoTlp { get; set; }
        public string NoNIK { get; set; }
        public int GetRujukanFrom { get; set; }
        public string PoliRujukan { get; set; }
        public string PoliRujukanNama { get; set; }
    }
}