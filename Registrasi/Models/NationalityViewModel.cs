﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class NationalityViewModel
    {
        [Display(Name = "ID Nationality")]
        public string NationalityID { get; set; }

        [Display(Name = "Nationality")]
        public string Nationality { get; set; }

        [Display(Name = "Default")]
        public bool NationalityDefault { get; set; }
    }
}