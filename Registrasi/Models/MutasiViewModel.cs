﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class MutasiViewModel
    {
        public string NoReg { get; set; }
        public DateTime TglReg { get; set; }
        public string TglReg_View { get; set; }
        public DateTime? JamReg { get; set; }
        public string JamReg_View { get; set; }
        public string StatusPeriksa { get; set; }
        public string StatusBayar { get; set; }
        public bool PasienBaru { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string NoIdentitas { get; set; }
        public string JenisKelamin { get; set; }
        public int? Umur { get; set; }
        public DateTime? TglLahir { get; set; }
        public string TglLahir_View { get; set; }
        public string Alamat { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int? JenisPasienKerjasamaID { get; set; }
        public string JenisKerjasama { get; set; }
        public string Kode_Customer { get; set; }
        public string Nama_Customer { get; set; }
        public string NoSep { get; set; }
        public string Kelas { get; set; }
        public string NoKartu { get; set; }
        public string SectionAsalID { get; set; }
        [Required]
        public string SectionID { get; set; }
        [Required]
        public string Dokter_ID { get; set; }
        public DateTime Tanggal { get; set; }
        public string Tanggal_view { get; set; }
        public string SectionName { get; set; }
        public string Dokter { get; set; }
        public bool PasienRS { get; set; }
        public string PenanggungNRM { get; set; }
        public string PenanggungNama { get; set; }
        public string PenanggungAlamat { get; set; }
        public string PenanggungPhone { get; set; }
        public string PenanggungHubungan { get; set; }
        [Required]
        public string KelasID { get; set; }
        [Required]
        public string KelasPelayanan { get; set; }
        public bool OK { get; set; }
        public bool VK { get; set; }
        public bool Titip { get; set; }
        [Required]
        public string NoKamar { get; set; }
        [Required]
        public string NoBed { get; set; }
        public bool RawatGabung { get; set; }
        public bool RawatInap { get; set; }
        public string KelasDitempati { get; set; }
    }
}