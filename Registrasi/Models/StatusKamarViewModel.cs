﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class StatusKamarViewModel
    {
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string NamaKelas { get; set; }
        public string NoKamar { get; set; }
        public string NoBed { get; set; }
        public byte JmlBed { get; set; }
        public byte NoLantai { get; set; }
        public string IDStatus { get; set; }
        public string Status { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }
        public string Alamat { get; set; }
        public string JenisKerjasama { get; set; }
        public string DokterRawat { get; set; }
        public Nullable<int> Umur { get; set; }
        public string Keterangan { get; set; }
        public string Expr1 { get; set; }
    }
}