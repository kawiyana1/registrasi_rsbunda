﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class DiagnosaViewModel
    {
        public string KodeICD { get; set; }
        public string SectionID { get; set; }
        public string Descriptions { get; set; }
        public string DiagnosaHC { get; set; }
        public string IDGroupICD { get; set; }
    }
}