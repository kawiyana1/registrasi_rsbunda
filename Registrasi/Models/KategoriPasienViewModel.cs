﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class KategoriPasienViewModel
    {
        public string KategoriMasalah { get; set; }
        public string Emoticon { get; set; }
    }
}