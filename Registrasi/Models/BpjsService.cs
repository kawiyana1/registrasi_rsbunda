﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Registrasi.Models
{
    public class BpjsService
    {
        #region Main
        protected string BaseUrl = ConfigurationManager.AppSettings["BaseUrl"];
        protected string BaseUrlAplicares = ConfigurationManager.AppSettings["BaseUrlAplicares"];
        protected string ServiceName = ConfigurationManager.AppSettings["ServiceName"];
        protected string CustomerId = ConfigurationManager.AppSettings["CustomerId"];
        protected string CustomerSecret = ConfigurationManager.AppSettings["CustomerSecret"];
        protected string KodePPK = ConfigurationManager.AppSettings["CustomerPpkCode"];
        protected string UserKey = ConfigurationManager.AppSettings["UserKey"];
        protected string UserKeyWsBPJS = ConfigurationManager.AppSettings["UserKeyWsBPJS"];
        protected string ServiceNameWsBPJS = ConfigurationManager.AppSettings["ServiceNameWsBPJS"];
        protected Int32 _Timestamp;
        protected string _keyDecrypt;

        public BpjsService()
        {
            _Timestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            _keyDecrypt = $"{CustomerId}{CustomerSecret}{_Timestamp}";
        }

        public string GetSignature
        {
            get
            {
                var data = $"{CustomerId}&{_Timestamp}";
                var secretKey = CustomerSecret;
                var hashObject = new HMACSHA256(Encoding.UTF8.GetBytes(secretKey));
                var signature = hashObject.ComputeHash(Encoding.UTF8.GetBytes(data));
                var encodedSignature = Convert.ToBase64String(signature);
                return encodedSignature;
            }
        }

        public string GetKodePPK
        {
            get { return KodePPK; }
        }

        public Int32 Timestamp
        {
            get { return _Timestamp; }
        }
        public string keyDecrypt
        {
            get { return _keyDecrypt; }
        }

        public string Decrypt(string key, string data)
        {
            string decData = null;
            byte[][] keys = GetHashKeys(key);

            try
            {
                decData = DecryptStringFromBytes_Aes(data, keys[0], keys[1]);
            }
            catch (CryptographicException cEx)
            {
            }
            catch (ArgumentNullException aEx)
            {
            }

            return decData;
        }

        private string DecryptStringFromBytes_Aes(string cipherTextString, byte[] Key, byte[] IV)
        {
            byte[] cipherText = Convert.FromBase64String(cipherTextString);

            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            string plaintext = null;

            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt =
                            new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }

        private byte[][] GetHashKeys(string key)
        {
            byte[][] result = new byte[2][];
            Encoding enc = Encoding.UTF8;

            SHA256 sha2 = new SHA256CryptoServiceProvider();

            byte[] rawKey = enc.GetBytes(key);
            byte[] rawIV = enc.GetBytes(key);

            byte[] hashKey = sha2.ComputeHash(rawKey);
            byte[] hashIV = sha2.ComputeHash(rawIV);

            Array.Resize(ref hashIV, 16);

            result[0] = hashKey;
            result[1] = hashIV;

            return result;
        }
        #endregion
    }

    #region META
    public class MetaData
    {
        public string code { get; set; }
        public string message { get; set; }
        public string totalitems { get; set; }
    }
    #endregion


}