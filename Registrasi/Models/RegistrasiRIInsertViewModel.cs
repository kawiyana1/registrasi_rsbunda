﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class RegistrasiRIInsertViewModel
    {
        public string NoReg { get; set; }
        public DateTime TglReg { get; set; }
        public string TglReg_View { get; set; }
        public DateTime? JamReg { get; set; }
        public string JamReg_View { get; set; }
        public string NoReservasi { get; set; }
        public bool PasienBaru { get; set; }
        [Required]
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string NoIdentitas { get; set; }
        public string JenisKelamin { get; set; }
        public int? Umur { get; set; }
        public DateTime? TglLahir { get; set; }
        public string TglLahir_View { get; set; }
        public bool TglLahirDiketahui { get; set; }
        public string Alamat { get; set; }
        public string Nama_Kabupaten { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int? JenisPasienKerjasamaID { get; set; }
        public string JenisPasienKerjasama { get; set; }
        public bool PasienKTP { get; set; }
        public string CompanyID { get; set; }
        public bool AnggotaBaruReg { get; set; }
        public string NamaPerusahaan { get; set; }
        public string Kelas { get; set; }
        public string KelasID { get; set; }
        public string NoKartu { get; set; }
        public string SectionID { get; set; }
        [Required]
        public string Dokter_ID { get; set; }
        public int? WaktuPraktek_ID { get; set; }
        public DateTime Tanggal { get; set; }
        public string Tanggal_view { get; set; }
        public string NamaSection { get; set; }
        public int Antrian { get; set; }
        public string Dokter { get; set; }
        public string Waktu { get; set; }
        public bool BlockVitamin { get; set; }
        public bool BlockSuplemen { get; set; }
        public bool BlockObatHerbal { get; set; }
        public bool BlockObatKosmetik { get; set; }
        public bool BlockObatJiwa { get; set; }
        public bool Rujukan { get; set; }
        public bool PasienPribadi { get; set; }
        public bool CP { get; set; }
        public string DokterPengirim_ID { get; set; }
        public string DokterPengirim { get; set; }
        public string VendorID { get; set; }
        public string VendorPengirim { get; set; }
        public string Memo { get; set; }
        public string CaraDatang { get; set; }
        public bool PasienRS { get; set; }
        public string PenanggungNRM { get; set; }
        [Required]
        public string PenanggungNama { get; set; }
        public string PenanggungAlamat { get; set; }
        [Required]
        public string PenanggungPhone { get; set; }
        [Required]
        public string PenanggungHubungan { get; set; }
        public string ICD_ID { get; set; }
        public string Deskripsi { get; set; }
        public string Keterangan { get; set; }
        public bool PasienAsuransi { get; set; }
        public bool HC_SHE { get; set; }
        public bool IKS { get; set; }
        public string AsuransiID { get; set; }
        public string PayorCompany { get; set; }
        public bool FisrtPayor { get; set; }
        public string AssCompany_ID { get; set; }
        public string AssistingCompany { get; set; }
        public string NoKartuAsuransi { get; set; }
        public string NoSJP { get; set; }
        public bool PasienPaket { get; set; }
        public string PaketJasa_ID { get; set; }
        public string Paket { get; set; }
        public bool HD { get; set; }
        public bool Diabetes { get; set; }
        public string TipePembayaran { get; set; }
        [Required]
        public string NoKamar { get; set; }
        public string NoBed { get; set; }
        public bool? RawatGabung { get; set; }
        public string JenisKunjungan { get; set; }
        public int? JenisPembayaran { get; set; }
        public string KelasPelayanan { get; set; }
        public string KelasAsal { get; set; }
        public bool TitipKelas { get; set; }
        public string NoSEP { get; set; }
        public bool PasienBayi { get; set; }
        public bool PasienCovid { get; set; }

        public bool PasienPanjer { get; set; }

        public string Reg_KelasPertanggungan { get; set; }
        public string Reg_KelasPelayanan { get; set; }
        public string Reg_KelasDitempati { get; set; }

    }
}