﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class JadwalKontrolViewModel
    {
        public string NoBukti { get; set; }
        public DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string NRM { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public string SectionName { get; set; }
        public string Keterangan { get; set; }
        public string NamaDOkter { get; set; }
        public bool KedatanganPasien { get; set; }
        public bool SudahReservasi { get; set; }
        public string DokterID { get; set; }
        public string SectionID { get; set; }
        public int WaktuID { get; set; }
        public string phone { get; set; }
    }
}