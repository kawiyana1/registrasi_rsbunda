﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class DokterPertamaViewModel
    {
        [Display(Name = "Kode")]
        public string Kode_Supplier { get; set; }

        [Display(Name = "Nama")]
        public string Nama_Supplier { get; set; }
    }
}