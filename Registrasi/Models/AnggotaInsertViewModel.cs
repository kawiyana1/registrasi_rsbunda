﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class AnggotaInsertViewModel
    {
        public string KodePerusahaan { get; set; }

        public string NamaPerusahaan { get; set; }

        public string NRM_Anggota { get; set; }

        public string NamaPasien { get; set; }

        public string KodePerusahaanAsal { get; set; }

        [Display(Name = "Perusahaan Asal")]
        public string NamaPerusahaanAsal { get; set; }

        public string Klp { get; set; }

        public string NoKartu { get; set; }

        public string CustomerID { get; set; }
        public string KelasID { get; set; }

        public decimal CustomerKerjasamaID { get; set; }
    }
}