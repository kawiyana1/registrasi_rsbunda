﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class BpjsModel
    {
        #region REFERENSI
        // =============== Ruang Rawat
        public class RuangRawat
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }

        public class ReferensiRuangRawatResponse
        {
            public List<List> list { get; set; }
        }

        public class ReferensiRuangRawatModel
        {
            public MetaData metaData { get; set; }
            public ReferensiRuangRawatResponse response { get; set; }
        }

        // =============== Cara Keluar
        public class CaraKeluar
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }

        public class ReferensiCaraKeluarResponse
        {
            public List<List> list { get; set; }
        }

        public class ReferensiCaraKeluarModel
        {
            public MetaData metaData { get; set; }
            public ReferensiCaraKeluarResponse response { get; set; }
        }

        // =============== Pasca Pulang
        public class PascaPulang
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }

        public class ReferensiPascaPulangResponse
        {
            public List<List> list { get; set; }
        }

        public class ReferensiPascaPulangModel
        {
            public MetaData metaData { get; set; }
            public ReferensiPascaPulangResponse response { get; set; }
        }

        // =============== Propinsi
        public class Propinsi
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }
        public class ReferensiPropinsiResponse
        {
            public List<List> list { get; set; }
        }

        public class ReferensiPropinsiModel
        {
            public MetaData metaData { get; set; }
            public ReferensiPropinsiResponse response { get; set; }
        }

        // =============== Kabupaten
        public class Kabupaten
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }
        public class ReferensiKabupatenResponse
        {
            public List<List> list { get; set; }
        }

        public class ReferensiKabupatenModel
        {
            public MetaData metaData { get; set; }
            public ReferensiKabupatenResponse response { get; set; }
        }

        // =============== Kecamatan
        public class Kecamatan
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }
        public class ReferensiKecamatanResponse
        {
            public List<List> list { get; set; }
        }

        public class ReferensiKecamatanModel
        {
            public MetaData metaData { get; set; }
            public ReferensiKecamatanResponse response { get; set; }
        }

        // =============== DPJP
        public class DPJP
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }
        public class ReferensiDPJPResponse
        {
            public List<List> list { get; set; }
        }

        public class ReferensiDPJPModel
        {
            public MetaData metaData { get; set; }
            public ReferensiDPJPResponse response { get; set; }
        }

        public class ReferensiDiagnosaPRBModel
        {
            public MetaData metaData { get; set; }
            public ReferensiDiagnosaPRBResponse response { get; set; }
        }

        public class ReferensiDiagnosaPRBResponse
        {
            public List<ReferensiDiagnosaPRB> list { get; set; }
        }

        public class ReferensiDiagnosaPRB
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }

        public class ReferensiObatPRBModel
        {
            public MetaData metaData { get; set; }
            public ReferensiObatPRBResponse response { get; set; }
        }
        public class ReferensiObatPRBResponse
        {
            public List<ObatPRB> list { get; set; }
        }

        public class ObatPRB
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }
        // =============== Kelas Rawat
        public class Spesialistik
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }

        public class ReferensiSpesialistikResponse
        {
            public List<List> list { get; set; }
        }

        public class ReferensiSpesialistikModel
        {
            public MetaData metaData { get; set; }
            public ReferensiSpesialistikResponse response { get; set; }
        }

        // =============== Kelas Rawat
        public class Dokter
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }

        public class ReferensiDokterResponse
        {
            public List<Dokter> list { get; set; }
        }

        public class ReferensiDokterModel
        {
            public MetaData metaData { get; set; }
            public ReferensiDokterResponse response { get; set; }
        }

        // =============== Kelas Rawat
        public class List
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }

        public class ReferensiKelasRawatResponse
        {
            public List<List> list { get; set; }
        }

        public class ReferensiKelasRawatModel
        {
            public MetaData metaData { get; set; }
            public ReferensiKelasRawatResponse response { get; set; }
        }

        // =============== Tindakan
        public class Procedure
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }

        public class ReferensiTindakanResponse
        {
            public List<Procedure> procedure { get; set; }
        }

        public class ReferensiTindakanModel
        {
            public MetaData metaData { get; set; }
            public ReferensiTindakanResponse response { get; set; }
        }

        // =============== Fasilitas Kesehatan
        public class Faskes
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }

        public class ReferensiFasilitasKesehatanResponse
        {
            public List<Faskes> faskes { get; set; }
        }

        public class ReferensiFasilitasKesehatanModel
        {
            public MetaData metaData { get; set; }
            public ReferensiFasilitasKesehatanResponse response { get; set; }
        }

        // =============== Poli
        public class Poli
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }

        public class ReferensiPoliResponse
        {
            public List<Poli> poli { get; set; }
        }

        public class ReferensiPoliModel
        {
            public MetaData metaData { get; set; }
            public ReferensiPoliResponse response { get; set; }
        }
        // =============== Diagnosa
        public class ReferensiDiagnosaResponse
        {
            public List<Diagnosa> diagnosa { get; set; }
        }

        public class ReferensiDiagnosaModel
        {
            public MetaData metaData { get; set; }
            public ReferensiDiagnosaResponse response { get; set; }
        }

        public class ReferensiProcedureModel
        {
            public MetaData metaData { get; set; }
            public ReferensiProcedureResponse response { get; set; }
        }

        public class ReferensiProcedureResponse
        {
            public List<Procedure> procedure { get; set; }
        }
        #endregion

        #region SEP

        public class SEPCariResponse
        {
            public string noSep { get; set; }
            public string tglSep { get; set; }
            public string jnsPelayanan { get; set; }
            public string kelasRawat { get; set; }
            public string diagnosa { get; set; }
            public string noRujukan { get; set; }
            public string poli { get; set; }
            public string poliEksekutif { get; set; }
            public string catatan { get; set; }
            public string penjamin { get; set; }
            public string kdStatusKecelakaan { get; set; }
            public string nmstatusKecelakaan { get; set; }
            public LokasiKejadian lokasiKejadian { get; set; }
            public DPJPCariSEP dpjp { get; set; }
            public PesertaSEP peserta { get; set; }
            public KelasRawat klsRawat { get; set; }
            public Kontrol kontrol { get; set; }
            public string cob { get; set; }
            public string katarak { get; set; }

        }

        public class Kontrol
        {
            public string kdDokter { get; set; }
            public string nmDokter { get; set; }
            public string noSurat { get; set; }
        }

        public class DPJPCariSEP
        {
            public string kdDPJP { get; set; }
            public string nmDPJP { get; set; }
        }

        public class LokasiKejadian
        {
            public string kdKab { get; set; }
            public string kdKec { get; set; }
            public string kdProp { get; set; }
            public string ketKejadian { get; set; }
            public string lokasi { get; set; }
            public string tglKejadian { get; set; }
        }

        public class SEPCariModel
        {
            public MetaData metaData { get; set; }
            public SEPCariResponse response { get; set; }
        }

        public class SEPUpdateTanggalPulangModel
        {
            public string noSep { get; set; }
            public string statusPulang { get; set; }
            public string noSuratMeninggal { get; set; }
            public string tglMeninggal { get; set; }
            public string tglPulang { get; set; }
            public string noLPManual { get; set; }
            public string user { get; set; }
        }

        public class SEPDeleteModel
        {
            public string noSep { get; set; }
            public string user { get; set; }
        }

        public class Jaminan
        {
            public string lakaLantas { get; set; }
            public PenjaminSEP penjamin { get; set; }
        }

        public class SKDPSEP
        {
            public string noSurat { get; set; }
            public string kodeDPJP { get; set; }
        }

        public class PenjaminSEP
        {
            public string tglKejadian { get; set; }
            public string keterangan { get; set; }
            public SuplesiSEP suplesi { get; set; }
        }

        public class SuplesiSEP
        {
            public string suplesi { get; set; }
            public string noSepSuplesi { get; set; }
            public LokasiLakaSEP lokasiLaka { get; set; }
        }

        public class LokasiLakaSEP
        {
            public string kdPropinsi { get; set; }
            public string kdKabupaten { get; set; }
            public string kdKecamatan { get; set; }
        }

        public class SEPUpdateModel
        {
            public string noSep { get; set; }
            public KelasRawat klsRawat { get; set; }
            public string noMR { get; set; }
            public string catatan { get; set; }
            public string diagAwal { get; set; }
            public PoliUpdateSEP poli { get; set; }
            public CobSEP cob { get; set; }
            public KatarakSEP katarak { get; set; }
            public Jaminan jaminan { get; set; }
            public string dpjpLayan { get; set; }
            public string noTelp { get; set; }
            public string user { get; set; }
        }

        public class PesertaSEP
        {
            public string asuransi { get; set; }
            public string hakKelas { get; set; }
            public string jnsPeserta { get; set; }
            public string kelamin { get; set; }
            public string nama { get; set; }
            public string noKartu { get; set; }
            public string noMr { get; set; }
            public string tglLahir { get; set; }
        }

        public class SEPResponseData
        {
            public string catatan { get; set; }
            public string diagnosa { get; set; }
            public string jnsPelayanan { get; set; }
            public string kelasRawat { get; set; }
            public string noSep { get; set; }
            public string penjamin { get; set; }
            public PesertaSEP peserta { get; set; }
            public string poli { get; set; }
            public string poliEksekutif { get; set; }
            public string tglSep { get; set; }
        }

        public class SEPResponse
        {
            public SEPResponseData sep { get; set; }
        }

        public class SEPResponseModel
        {
            public MetaData metaData { get; set; }
            public SEPResponse response { get; set; }
        }

        public class SEPUpdateDeleteResponseModel
        {
            public MetaData metaData { get; set; }
            public string response { get; set; }
        }

        public class RujukanSEP
        {
            public string asalRujukan { get; set; }
            public string tglRujukan { get; set; }
            public string noRujukan { get; set; }
            public string ppkRujukan { get; set; }
        }

        public class PoliSEP
        {
            public string tujuan { get; set; }
            public string eksekutif { get; set; }
        }

        public class PoliUpdateSEP
        {
            public string tujuan { get; set; }
            public string eksekutif { get; set; }
        }

        public class CobSEP
        {
            public string cob { get; set; }
        }

        public class KatarakSEP
        {
            public string katarak { get; set; }
        }

        public class SEPDataModel
        {
            public string noKartu { get; set; }
            public string tglSep { get; set; }
            public string ppkPelayanan { get; set; }
            public string jnsPelayanan { get; set; }
            public KelasRawat klsRawat { get; set; }
            public string noMR { get; set; }
            public RujukanSEP rujukan { get; set; }
            public string catatan { get; set; }
            public string diagAwal { get; set; }
            public PoliSEP poli { get; set; }
            public CobSEP cob { get; set; }
            public KatarakSEP katarak { get; set; }
            public Jaminan jaminan { get; set; }
            public string tujuanKunj { get; set; }
            public string flagProcedure { get; set; }
            public string kdPenunjang { get; set; }
            public string assesmentPel { get; set; }
            public SKDPSEP skdp { get; set; }
            public string dpjpLayan { get; set; }
            public string noTelp { get; set; }
            public string user { get; set; }
        }

        public class KelasRawat
        {
            public string klsRawatHak { get; set; }
            public string klsRawatNaik { get; set; }
            public string pembiayaan { get; set; }
            public string penanggungJawab { get; set; }
        }
        #endregion

        #region SEP INTERNAL
        public class SEPInternalResponseModel
        {
            public MetaData metaData { get; set; }
            public SEPInternalResponse response { get; set; }
        }

        public class SEPInternalResponse
        {
            public string count { get; set; }
            public List<SEPInternal> list { get; set; }
        }


        public class SEPInternal
        {
            public string tujuanrujuk { get; set; }
            public string nmtujuanrujuk { get; set; }
            public string nmpoliasal { get; set; }
            public string tglrujukinternal { get; set; }
            public string nosep { get; set; }
            public string nosepref { get; set; }
            public string ppkpelsep { get; set; }
            public string nokapst { get; set; }
            public string tglsep { get; set; }
            public string nosurat { get; set; }
            public string flaginternal { get; set; }
            public string kdpoliasal { get; set; }
            public string kdpolituj { get; set; }
            public string kdpenunjang { get; set; }
            public string nmpenunjang { get; set; }
            public string diagppk { get; set; }
            public string kddokter { get; set; }
            public string nmdokter { get; set; }
            public string flagprosedur { get; set; }
            public string opsikonsul { get; set; }
            public string flagsep { get; set; }
            public string fuser { get; set; }
            public string fdate { get; set; }
            public string nmdiag { get; set; }
        }

        public class SEPInternalDeleteModel
        {
            public string noSep { get; set; }
            public string noSurat { get; set; }
            public string tglRujukanInternal { get; set; }
            public string kdPoliTuj { get; set; }
            public string user { get; set; }
        }

        public class SEPInternalDeleteResponseModel
        {
            public MetaData metaData { get; set; }
            public string response { get; set; }
        }
        #endregion

        #region APPROVAL
        public class StringResponseModel
        {
            public MetaData metaData { get; set; }
            public string response { get; set; }
        }

        public class ApprovalDataModel
        {
            public string noKartu { get; set; }
            public string tglSep { get; set; }
            public string jnsPelayanan { get; set; }
            public string jnsPengajuan { get; set; }
            public string keterangan { get; set; }
            public string user { get; set; }
        }
        #endregion

        #region PESERTA
        // =============== No. Kartu BPJ
        public class Cob
        {
            public string nmAsuransi { get; set; }
            public string noAsuransi { get; set; }
            public string tglTAT { get; set; }
            public string tglTMT { get; set; }
        }

        public class HakKelas
        {
            public string keterangan { get; set; }
            public string kode { get; set; }
        }

        public class Informasi
        {
            public string dinsos { get; set; }
            public string noSKTM { get; set; }
            public string prolanisPRB { get; set; }
        }

        public class JenisPeserta
        {
            public string keterangan { get; set; }
            public string kode { get; set; }
        }

        public class Mr
        {
            public string noMR { get; set; }
            public string noTelepon { get; set; }
        }

        public class ProvUmum
        {
            public string kdProvider { get; set; }
            public string nmProvider { get; set; }
        }

        public class StatusPeserta
        {
            public string keterangan { get; set; }
            public string kode { get; set; }
        }

        public class Umur
        {
            public string umurSaatPelayanan { get; set; }
            public string umurSekarang { get; set; }
        }

        public class Peserta
        {
            public Cob cob { get; set; }
            public HakKelas hakKelas { get; set; }
            public Informasi informasi { get; set; }
            public JenisPeserta jenisPeserta { get; set; }
            public Mr mr { get; set; }
            public string nama { get; set; }
            public string nik { get; set; }
            public string noKartu { get; set; }
            public string pisa { get; set; }
            public ProvUmum provUmum { get; set; }
            public string sex { get; set; }
            public StatusPeserta statusPeserta { get; set; }
            public string tglCetakKartu { get; set; }
            public DateTime tglLahir { get; set; }
            public DateTime tglTAT { get; set; }
            public DateTime tglTMT { get; set; }
            public Umur umur { get; set; }
        }

        public class PesertaResponse
        {
            public Peserta peserta { get; set; }
        }

        public class PesertaNoKartuBPJSModel
        {
            public MetaData metaData { get; set; }
            public PesertaResponse response { get; set; }
        }

        // =============== NIK

        public class PesertaNIKModel
        {
            public MetaData metaData { get; set; }
            public PesertaResponse response { get; set; }
        }
        #endregion

        #region RUJUKAN
        // =============== Data Rujukan by Nomor Rujukan
        public class Diagnosa
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }

        public class Pelayanan
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }

        public class PoliRujukan
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }

        public class ProvPerujuk
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }

        public class Rujukan
        {
            public Diagnosa diagnosa { get; set; }
            public string keluhan { get; set; }
            public string noKunjungan { get; set; }
            public Pelayanan pelayanan { get; set; }
            public Peserta peserta { get; set; }
            public PoliRujukan poliRujukan { get; set; }
            public ProvPerujuk provPerujuk { get; set; }
            public DateTime tglKunjungan { get; set; }
        }

        public class RujukanInsertModel
        {
            public string noSep { get; set; }
            public string tglRujukan { get; set; }
            public string tglRencanaKunjungan { get; set; }
            public string ppkDirujuk { get; set; }
            public string jnsPelayanan { get; set; }
            public string catatan { get; set; }
            public string diagRujukan { get; set; }
            public string tipeRujukan { get; set; }
            public string poliRujukan { get; set; }
            public string user { get; set; }
        }

        public class RujukanUpdateModel
        {
            public string noRujukan { get; set; }
            public string tglRujukan { get; set; }
            public string tglRencanaKunjungan { get; set; }
            public string ppkDirujuk { get; set; }
            public string jnsPelayanan { get; set; }
            public string catatan { get; set; }
            public string diagRujukan { get; set; }
            public string tipeRujukan { get; set; }
            public string poliRujukan { get; set; }
            public string user { get; set; }
        }

        public class RujukanDeleteModel
        {
            public string noRujukan { get; set; }
            public string user { get; set; }
        }


        public class RujukanResponse
        {
            public Rujukan rujukan { get; set; }
        }

        public class RujukanListResponse
        {
            public List<Rujukan> rujukan { get; set; }
        }

        public class RujukanModel
        {
            public MetaData metaData { get; set; }
            public RujukanResponse response { get; set; }
        }

        public class AsalRujukan
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }

        public class PesertaRujukan
        {
            public string asuransi { get; set; }
            public object hakKelas { get; set; }
            public string jnsPeserta { get; set; }
            public string kelamin { get; set; }
            public string nama { get; set; }
            public string noKartu { get; set; }
            public string noMr { get; set; }
            public string tglLahir { get; set; }
        }

        public class PoliTujuan
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }

        public class TujuanRujukan
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }

        public class RujukanData
        {
            public AsalRujukan AsalRujukan { get; set; }
            public Diagnosa diagnosa { get; set; }
            public string noRujukan { get; set; }
            public PesertaRujukan peserta { get; set; }
            public PoliTujuan poliTujuan { get; set; }
            public string tglBerlakuKunjungan { get; set; }
            public string tglRencanaKunjungan { get; set; }
            public string tglRujukan { get; set; }
            public TujuanRujukan tujuanRujukan { get; set; }
        }

        public class RujukanDataResponse
        {
            public RujukanData rujukan { get; set; }
        }

        public class RujukanResponseModel
        {
            public MetaData metaData { get; set; }
            public RujukanDataResponse response { get; set; }
        }

        public class RujukanResponseUpdateDeleteModel
        {
            public MetaData metaData { get; set; }
            public string response { get; set; }
        }

        public class RujukanListModel
        {
            public MetaData metaData { get; set; }
            public RujukanListResponse response { get; set; }
        }

        public class SpesialistikRujukanModel
        {
            public MetaData metaData { get; set; }
            public SpesialistikRujukanResponse response { get; set; }
        }

        public class SpesialistikRujukanResponse
        {
            public List<ListSpesialistikRujukan> list { get; set; }
        }

        public class ListSpesialistikRujukan
        {
            public string kodeSpesialis { get; set; }
            public string namaSpesialis { get; set; }
            public string kapasitas { get; set; }
            public string jumlahRujukan { get; set; }
            public string persentase { get; set; }
        }

        public class SarananModel
        {
            public MetaData metaData { get; set; }
            public SaranaResponse response { get; set; }
        }

        public class SaranaResponse
        {
            public List<ListSarana> list { get; set; }
        }

        public class ListSarana
        {
            public string kodeSarana { get; set; }
            public string namaSarana { get; set; }

        }

        #endregion

        #region MONITORING HISTORI
        public class Histori
        {
            public string diagnosa { get; set; }
            public string jnsPelayanan { get; set; }
            public string kelasRawat { get; set; }
            public string namaPeserta { get; set; }
            public string noKartu { get; set; }
            public string noSep { get; set; }
            public string noRujukan { get; set; }
            public string poli { get; set; }
            public string ppkPelayanan { get; set; }
            public string tglPlgSep { get; set; }
            public string tglSep { get; set; }
        }

        public class MonitoringHistoriResponse
        {
            public List<Histori> histori { get; set; }
        }

        public class MonitoringHistoriModel
        {
            public MetaData metaData { get; set; }
            public MonitoringHistoriResponse response { get; set; }
        }
        #endregion

        #region MONITORING DATA KUNJUNGAN
        public class DataKunjunganSEP
        {
            public string diagnosa { get; set; }
            public string jnsPelayanan { get; set; }
            public string kelasRawat { get; set; }
            public string nama { get; set; }
            public string noKartu { get; set; }
            public string noSep { get; set; }
            public string noRujukan { get; set; }
            public string poli { get; set; }
            public string tglPlgSep { get; set; }
            public string tglSep { get; set; }
        }

        public class MonitoringDataKunjunganResponse
        {
            public List<DataKunjunganSEP> sep { get; set; }
        }

        public class MonitoringDataKunjunganModel
        {
            public MetaData metaData { get; set; }
            public MonitoringDataKunjunganResponse response { get; set; }
        }
        #endregion

        #region MONITORING DATA KLAIM
        public class DataInacbg
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }

        public class DataBiaya
        {
            public string byPengajuan { get; set; }
            public string bySetujui { get; set; }
            public string byTarifGrouper { get; set; }
            public string byTarifRS { get; set; }
            public string byTopup { get; set; }
        }

        public class DataPeserta
        {
            public string nama { get; set; }
            public string noKartu { get; set; }
            public string noMR { get; set; }
        }

        public class DataKlaim
        {
            public DataInacbg Inacbg { get; set; }
            public DataBiaya biaya { get; set; }
            public string kelasRawat { get; set; }
            public string noFPK { get; set; }
            public string noSEP { get; set; }
            public DataPeserta peserta { get; set; }
            public string poli { get; set; }
            public string status { get; set; }
            public string tglPulang { get; set; }
            public string tglSep { get; set; }
        }

        public class MonitoringDataKlaimResponse
        {
            public List<DataKlaim> klaim { get; set; }
        }

        public class MonitoringDataKlaimModel
        {
            public MetaData metaData { get; set; }
            public MonitoringDataKlaimResponse response { get; set; }
        }
        #endregion

        #region MONITORING DATA JASA RAHARJA
        public class DataSEPJasaRaharja
        {
            public string noSEP { get; set; }
            public string tglSEP { get; set; }
            public string tglPlgSEP { get; set; }
            public string noMr { get; set; }
            public string jnsPelayanan { get; set; }
            public string poli { get; set; }
            public string diagnosa { get; set; }
            public DataPeserta peserta { get; set; }
        }

        public class DataJasaRaharja
        {
            public string tglKejadian { get; set; }
            public string noRegister { get; set; }
            public string ketStatusDijamin { get; set; }
            public string ketStatusDikirim { get; set; }
            public string biayaDijamin { get; set; }
            public string plafon { get; set; }
            public string jmlDibayar { get; set; }
            public string resultsJasaRaharja { get; set; }
        }

        public class DataJaminan
        {
            public DataSEPJasaRaharja sep { get; set; }
            public DataJasaRaharja jasaRaharja { get; set; }
        }

        public class MonitoringDataJasaRaharjaResponse
        {
            public List<DataJaminan> jaminan { get; set; }
        }

        public class MonitoringDataJasaRaharjaModel
        {
            public MetaData metaData { get; set; }
            public MonitoringDataJasaRaharjaResponse response { get; set; }
        }
        #endregion

        #region RENCANA KONTROL
        public class RencanaKontrolResponseModel
        {
            public MetaData metaData { get; set; }
            public RencanaKontrolResponse response { get; set; }
        }

        public class RencanaKontrolResponse
        {
            public string noSuratKontrol { get; set; }
            public string tglRencanaKontrol { get; set; }
            public string namaDokter { get; set; }
            public string noKartu { get; set; }
            public string nama { get; set; }
            public string kelamin { get; set; }
            public string tglLahir { get; set; }
            public string namaDiagnosa { get; set; }
        }

        public class RencanaKontrolDataModel
        {
            public string noSEP { get; set; }
            public string kodeDokter { get; set; }
            public string poliKontrol { get; set; }
            public string tglRencanaKontrol { get; set; }
            public string user { get; set; }
        }

        public class RencanaKontrolUpdateDataModel
        {
            public string noSuratKontrol { get; set; }
            public string kodeDokter { get; set; }
            public string poliKontrol { get; set; }
            public string tglRencanaKontrol { get; set; }
            public string user { get; set; }
        }

        public class RencanaKontrolDeleteResponseModel
        {
            public MetaData metaData { get; set; }
            public string response { get; set; }
        }

        public class RencanaKontrolDeleteModel
        {
            public DeleteSuratKontrol t_suratkontrol { get; set; }
        }

        public class DeleteSuratKontrol
        {
            public string noSuratKontrol { get; set; }
            public string user { get; set; }
        }

        public class DataNomorSuratKontrolResponseModel
        {
            public MetaData metaData { get; set; }
            public DataNomorSuratKontrolResponse response { get; set; }
        }
        public class CariNomorSuratKontrolResponseModel
        {
            public MetaData metaData { get; set; }
            public CariNomorSuratKontrolResponse response { get; set; }

        }

        public class CariNomorSuratKontrolResponse
        {
            public string noSuratKontrol { get; set; }
            public string tglRencanaKontrol { get; set; }
            public string tglTerbit { get; set; }
            public string jnsKontrol { get; set; }
            public string poliTujuan { get; set; }
            public string namaPoliTujuan { get; set; }
            public string kodeDokter { get; set; }
            public string namaDokter { get; set; }
            public string flagKontrol { get; set; }
            public string kodeDokterPembuat { get; set; }
            public string namaDokterPembuat { get; set; }
            public string namaJnsKontrol { get; set; }
            public CariNomorSuratKontrolSEP sep { get; set; }

        }
        public class CariNomorSuratKontrolSEP
        {
            public string noSep { get; set; }
            public string tglSep { get; set; }
            public string jnsPelayanan { get; set; }
            public string poli { get; set; }
            public string diagnosa { get; set; }
            public PesertaRencanaKontrol peserta { get; set; }
            public ProvUmum provUmum { get; set; }
            public ProvPerujukRencanaKontrol provPerujuk { get; set; }

        }
        public class SEPRencanaKontrolResponseModel
        {
            public MetaData metaData { get; set; }
            public SEPRencanaKontrolResponse response { get; set; }

        }

        public class SEPRencanaKontrolResponse
        {
            public string noSep { get; set; }
            public string tglSep { get; set; }
            public string jnsPelayanan { get; set; }
            public string poli { get; set; }
            public string diagnosa { get; set; }
            public PesertaRencanaKontrol peserta { get; set; }
            public ProvUmum provUmum { get; set; }
            public ProvPerujukRencanaKontrol provPerujuk { get; set; }

        }

        public class PesertaRencanaKontrol
        {
            public string noKartu { get; set; }
            public string nama { get; set; }
            public string tglLahir { get; set; }
            public string kelamin { get; set; }
            public string hakKelas { get; set; }
        }

        public class ProvPerujukRencanaKontrol
        {
            public string kdProviderPerujuk { get; set; }
            public string nmProviderPerujuk { get; set; }
            public string asalRujukan { get; set; }
            public string noRujukan { get; set; }
            public string tglRujukan { get; set; }

        }

        public class DataNomorSuratKontrolResponse
        {
            public List<SuratKontrol> list { get; set; }
        }

        public class SuratKontrol
        {
            public string noSuratKontrol { get; set; }
            public string jnsPelayanan { get; set; }
            public string jnsKontrol { get; set; }
            public string namaJnsKontrol { get; set; }
            public string tglRencanaKontrol { get; set; }
            public string tglTerbitKontrol { get; set; }
            public string noSepAsalKontrol { get; set; }
            public string poliAsal { get; set; }
            public string namaPoliAsal { get; set; }
            public string poliTujuan { get; set; }
            public string namaPoliTujuan { get; set; }
            public string tglSEP { get; set; }
            public string kodeDokter { get; set; }
            public string namaDokter { get; set; }
            public string noKartu { get; set; }
            public string nama { get; set; }

        }


        public class DataPoliResponseModel
        {
            public MetaData metaData { get; set; }
            public DataPoliResponse response { get; set; }
        }

        public class DataPoliResponse
        {
            public List<DataPoli> list { get; set; }
        }

        public class DataPoli
        {
            public string kodePoli { get; set; }
            public string namaPoli { get; set; }
            public string kapasitas { get; set; }
            public string jmlRencanaKontroldanRujukan { get; set; }
            public string persentase { get; set; }
        }


        public class DataDokterResponseModel
        {
            public MetaData metaData { get; set; }
            public DataDokterResponse response { get; set; }
        }

        public class DataDokterResponse
        {
            public List<DataDokter> list { get; set; }
        }

        public class DataDokter
        {
            public string kodeDokter { get; set; }
            public string namaDokter { get; set; }
            public string jadwalPraktek { get; set; }
            public string kapasitas { get; set; }
        }
        #endregion

        #region SPRI
        public class SPRIResponseModel
        {
            public MetaData metaData { get; set; }
            public SPRIResponse response { get; set; }
        }

        public class SPRIResponse
        {
            public string noSPRI { get; set; }
            public string tglRencanaKontrol { get; set; }
            public string namaDokter { get; set; }
            public string noKartu { get; set; }
            public string nama { get; set; }
            public string kelamin { get; set; }
            public string tglLahir { get; set; }
            public string namaDiagnosa { get; set; }
        }

        public class SPRIDataModel
        {
            public string noKartu { get; set; }
            public string kodeDokter { get; set; }
            public string poliKontrol { get; set; }
            public string tglRencanaKontrol { get; set; }
            public string user { get; set; }
        }

        public class SPRIUpdateDataModel
        {
            public string noSPRI { get; set; }
            public string kodeDokter { get; set; }
            public string poliKontrol { get; set; }
            public string tglRencanaKontrol { get; set; }
            public string user { get; set; }
        }
        #endregion

        #region RUJUKAN BALIK
        public class RujukanBalikResponseModel
        {
            public MetaData metaData { get; set; }
            public RujukanBalikResponse response { get; set; }
        }
        public class RujukanBalikResponse
        {
            public RujukanBalikDPJP DPJP { get; set; }
            public string keterangan { get; set; }
            public string noSRB { get; set; }
            public RujukanBalikObat obat { get; set; }
            public RujukanBalikPeserta peserta { get; set; }
            public ProgramPRB programPRB { get; set; }
            public string saran { get; set; }
            public string tglSRB { get; set; }
        }
        public class RujukanBalikDPJP
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }
        public class RujukanBalikObat
        {
            public List<Obat> list { get; set; }
        }
        public class Obat
        {
            public string jmlObat { get; set; }
            public string nmObat { get; set; }
            public string signa { get; set; }
        }
        public class RujukanBalikPeserta
        {
            public string alamat { get; set; }
            public RujukanBalikAsalFaskes asalFaskes { get; set; }
            public string email { get; set; }
            public string kelamin { get; set; }
            public string nama { get; set; }
            public string noKartu { get; set; }
            public string noTelepon { get; set; }
            public string tglLahir { get; set; }
        }
        public class RujukanBalikAsalFaskes
        {
            public string kode { get; set; }
            public string nama { get; set; }
        }
        public class RujukanBalikDataModel
        {
            public string noSep { get; set; }
            public string noKartu { get; set; }
            public string alamat { get; set; }
            public string email { get; set; }
            public string programPRB { get; set; }
            public string kodeDPJP { get; set; }
            public string keterangan { get; set; }
            public string saran { get; set; }
            public string user { get; set; }
            public List<RujukanBalikInsertObat> obat { get; set; }
        }
        public class RujukanBalikDataUpdateModel
        {
            public string noSrb { get; set; }
            public string noSep { get; set; }
            public string noKartu { get; set; }
            public string alamat { get; set; }
            public string email { get; set; }
            public string programPRB { get; set; }
            public string kodeDPJP { get; set; }
            public string keterangan { get; set; }
            public string saran { get; set; }
            public string user { get; set; }
            public List<RujukanBalikInsertObat> obat { get; set; }
        }
        public class RujukanBalikInsertObat
        {
            public string kdObat { get; set; }
            public string signa1 { get; set; }
            public string signa2 { get; set; }
            public string jmlObat { get; set; }
        }
        public class DataPRBResponseModel
        {
            public MetaData metaData { get; set; }
            public DataPRBResponse response { get; set; }
        }
        public class DataPRBResponse
        {
            public DataPRB prb { get; set; }
        }
        public class DataPRB
        {
            public List<ListDataPRB> list { get; set; }
        }
        public class ListDataPRB
        {
            public DPJP DPJP { get; set; }
            public string noSEP { get; set; }
            public string noSRB { get; set; }
            public PRBPeserta peserta { get; set; }
            public ProgramPRB programPRB { get; set; }
            public string keterangan { get; set; }
            public string saran { get; set; }
            public string tglSRB { get; set; }
        }
        public class PRBPeserta
        {
            public string alamat { get; set; }
            public string email { get; set; }
            public string nama { get; set; }
            public string noKartu { get; set; }
            public string noTelepon { get; set; }

        }
        public class ProgramPRB
        {
            public string kode { get; set; }
            public string nama { get; set; }

        }

        public class RujukanBalikUpdateResponseModel
        {
            public MetaData metaData { get; set; }
            public string response { get; set; }
        }

        public class RujukanBalikDeleteResponseModel
        {
            public MetaData metaData { get; set; }
            public string response { get; set; }
        }

        public class RujukanBalikDeleteModel
        {
            public string noSrb { get; set; }
            public string user { get; set; }
        }

        public class NomorSRBResponseModel
        {
            public MetaData metaData { get; set; }
            public NomorSRBResponse response { get; set; }
        }

        public class NomorSRBResponse
        {
            public PRB prb { get; set; }
        }

        public class PRB
        {
            public RujukanBalikDPJP DPJP { get; set; }
            public string noSEP { get; set; }
            public string noSRB { get; set; }
            public ObatModel obat { get; set; }
            public RujukanBalikPeserta peserta { get; set; }
            public ProgramPRB programPRB { get; set; }
            public string keterangan { get; set; }
            public string saran { get; set; }
            public string tglSRB { get; set; }
        }

        public class ObatModel
        {
            public List<ListDataObat> obat { get; set; }
        }

        public class ListDataObat
        {
            public string jmlObat { get; set; }
            public string kdObat { get; set; }
            public string nmObat { get; set; }
            public string signa1 { get; set; }
            public string signa2 { get; set; }
        }
        #endregion

        #region RUJUKAN KHUSUS
        public class RujukanKhususResponseModel
        {
            public MetaData metaData { get; set; }
            public RujukanKhususResponse response { get; set; }
        }

        public class RujukanKhususResponse
        {
            public RujukanKhusus rujukan { get; set; }

        }

        public class RujukanKhusus
        {
            public string idrujukan { get; set; }
            public string norujukan { get; set; }
            public string nokapst { get; set; }
            public string nmpst { get; set; }
            public string diagppk { get; set; }
            public string tglrujukan_awal { get; set; }
            public string tglrujukan_berakhir { get; set; }
        }

        public class RujukanKhususDataModel
        {
            public string noRujukan { get; set; }
            public List<RujukanKhususDiagnosa> diagnosa { get; set; }
            public List<RujukanKhususProcedure> procedure { get; set; }
            public string user { get; set; }
        }

        public class RujukanKhususDiagnosa
        {
            public string kode { get; set; }
        }

        public class RujukanKhususProcedure
        {
            public string kode { get; set; }
        }

        public class RujukanKhususDeleteResponseModel
        {
            public MetaData metaData { get; set; }
            public string response { get; set; }
        }

        public class RujukanKhususDeleteModel
        {
            public string idRujukan { get; set; }
            public string noRujukan { get; set; }
            public string user { get; set; }
        }

        public class ListRujukanKhususResponseModel
        {
            public MetaData metaData { get; set; }
            public ListRujukanKhususResponse response { get; set; }
        }

        public class ListRujukanKhususResponse
        {
            public List<ListRujukanKhusus> rujukan { get; set; }
        }

        public class ListRujukanKhusus
        {
            public string idrujukan { get; set; }
            public string norujukan { get; set; }
            public string nokapst { get; set; }
            public string nmpst { get; set; }
            public string diagppk { get; set; }
            public string tglrujukan_awal { get; set; }
            public string tglrujukan_berakhir { get; set; }
        }
        #endregion

        #region FINGER PRINT
        public class FingerPrintResponseModel
        {
            public MetaData metaData { get; set; }
            public FingerPrintResponse response { get; set; }
        }

        public class FingerPrintResponse
        {
            public string kode { get; set; }
            public string status { get; set; }
        }

        public class ListFingerPrintResponseModel
        {
            public MetaData metaData { get; set; }
            public ListFingerPrintResponse response { get; set; }
        }

        public class ListFingerPrintResponse
        {
            public List<ListFingerPrint> list { get; set; }
        }

        public class ListFingerPrint
        {
            public string noKartu { get; set; }
            public string noSEP { get; set; }
        }
        #endregion

        #region SUPLESI
        public class SuplesiResponseModel
        {
            public MetaData metaData { get; set; }
            public SuplesiResponse response { get; set; }
        }

        public class SuplesiResponse
        {
            public List<ListSuplesi> jaminan { get; set; }
        }

        public class ListSuplesi
        {
            public string noRegister { get; set; }
            public string noSep { get; set; }
            public string noSepAwal { get; set; }
            public string noSuratJaminan { get; set; }
            public string tglKejadian { get; set; }
            public string tglSep { get; set; }
        }

        public class DataIndukKecelakaanResponseModel
        {
            public MetaData metaData { get; set; }
            public DataIndukKecelakaanResponse response { get; set; }
        }

        public class DataIndukKecelakaanResponse
        {
            public List<DataIndukKecelakaan> list { get; set; }
        }

        public class DataIndukKecelakaan
        {
            public string noSEP { get; set; }
            public string tglKejadian { get; set; }
            public string ppkPelSEP { get; set; }
            public string kdProp { get; set; }
            public string kdKab { get; set; }
            public string kdKec { get; set; }
            public string ketKejadian { get; set; }
            public string noSEPSuplesi { get; set; }
        }
        #endregion

        #region JADWAL
        public class JadwalDokterResponseModel
        {
            public MetaData metaData { get; set; }
            public List<JadwalDokterResponse> response { get; set; }
        }

        public class JadwalDokterResponse
        {
            public string kodesubspesialis { get; set; }
            public string hari { get; set; }
            public string kapasitaspasien { get; set; }
            public string libur { get; set; }
            public string namahari { get; set; }
            public string jadwal { get; set; }
            public string namasubspesialis { get; set; }
            public string namadokter { get; set; }
            public string kodepoli { get; set; }
            public string namapoli { get; set; }
            public string kodedokter { get; set; }
        }

        public class JadwalDokterDataUpdateModel
        {
            public string kodepoli { get; set; }
            public string kodesubspesialis { get; set; }
            public string kodedokter { get; set; }
            public List<Jadwal> jadwal { get; set; }
        }

        public class Jadwal
        {
            public string hari { get; set; }
            public string buka { get; set; }
            public string tutup { get; set; }
        }

        public class JadwalDokterUpdateResponseModel
        {
            public MetaData metaData { get; set; }
        }
        public class PoliResponseModel
        {
            public MetaData metaData { get; set; }
            public List<PoliResponse> response { get; set; }
        }

        public class PoliResponse
        {
            public string nmpoli { get; set; }
            public string nmsubspesialis { get; set; }
            public string kdsubspesialis { get; set; }
            public string kdpoli { get; set; }
        }


        #endregion

        #region DECRYPT
        public class BpjsApiEncryptedResponse
        {
            public BpjsApiEncryptedResponse()
            {
                metaData = new MetaData();
            }

            public MetaData metaData { get; set; }
            public string response { get; set; }

            public bool isSuccess { get => metaData?.code == "200"; }
        }
        #endregion
    }
}