﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class ManageSEPViewModel
    {
        public string NoReg { get; set; }
        public string NoSEP { get; set; }
        public string NoKartu { get; set; }
        public string SectionID { get; set; }
        public string NRM { get; set; }
    }
}