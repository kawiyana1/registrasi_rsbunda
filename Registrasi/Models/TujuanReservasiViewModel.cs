﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class TujuanReservasiViewModel
    {
        public string SectionID { get; set; }
        public string DokterID { get; set; }
        public int WaktuID { get; set; }
        public DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string UntukTanggal { get; set; }
        public string NamaSection { get; set; }
        public int Antrian { get; set; }
        public string NamaDOkter { get; set; }
        public string Keterangan { get; set; }
        public string Hari { get; set; }
    }
}