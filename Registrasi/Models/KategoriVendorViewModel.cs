﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class KategoriVendorViewModel
    {
        public string Kode_Kategori { get; set; }
        public string Kategori_Name { get; set; }
    }
}