﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class PasienRIViewModel
    {
        public string NoReg { get; set; }
        public System.DateTime TglReg { get; set; }
        public string TglReg_View { get; set; }
        public System.DateTime JamReg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }
        public string Kamar { get; set; }
        public string NoBed { get; set; }
        public string NamaKelas { get; set; }
        public string JenisKerjasama { get; set; }
        public string Nama_Customer { get; set; }
        public string NoAnggota { get; set; }
        public string NamaDOkter { get; set; }
        public string SectionName { get; set; }
        public string Alamat { get; set; }
    }
}