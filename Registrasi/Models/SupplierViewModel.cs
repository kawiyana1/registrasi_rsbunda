﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registrasi.Models
{
    public class SupplierViewModel
    {
        [Display(Name = "Kode Dokter")]
        public string Kode_Supplier { get; set; }

        [Display(Name = "Nama Dokter")]
        public string Nama_Supplier { get; set; }
    }
}
