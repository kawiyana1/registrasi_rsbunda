﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class PekerjaanViewModel
    {
        public string NamaPekerjaan { get; set; }
        public string NamaPekerjaanEnglish { get; set; }
    }
}