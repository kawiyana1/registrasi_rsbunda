﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registrasi.Models
{
    public class VendorViewModel
    {
        [Display(Name ="Kode Vendor")]
        public string Kode_Supplier { get; set; }
    
        [Display(Name = "Nama Vendor")]
        public string Nama_Supplier { get; set; }
        public short Supplier_ID { get; set; }
        public string Alamat_1 { get; set; }
        public string No_Telepon_1 { get; set; }
        public string KodeKategoriVendor { get; set; }
        public string KategoriName { get; set; }
        public bool Active { get; set; }
    }
}
