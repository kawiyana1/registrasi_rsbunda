﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class TipePasienViewModel
    {
        public string NoReg { get; set; }

        public string NRM { get; set; }

        public string NamaPasien { get; set; }
        public string NamaAnggota { get; set; }

        [Required]
        public int? JenisKerjasamaID { get; set; }

        public string JenisKerjasama { get; set; }

        public string KodePerusahaan { get; set; }

        public bool AnggotaBaru { get; set; }

        public string NoAnggota { get; set; }

        public bool PasienKTP { get; set; }

        public bool PasienAsuransi { get; set; }

        public decimal CustomerKerjasamaID { get; set; }

        public string CaseNO { get; set; }

        public string KdKelasPertanggungan { get; set; }

        public string NoSep { get; set; }

        public string Klp { get; set; }

        public string KelasID { get; set; }

        public int JenisPasienID { get; set; }

        public string NamaPerusahaan { get; set; }
        public int CheckOtorisasi { get; set; }
        
    }
}