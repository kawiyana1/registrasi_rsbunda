﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class ProvinsiViewModel
    {
        public Nullable<short> Negara_ID { get; set; }
        public short Propinsi_ID { get; set; }
        public string Kode_Propinsi { get; set; }
        public string Nama_Propinsi { get; set; }
        public bool Propinsi_Default { get; set; }
    }
}