﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registrasi.Models
{
    public class SectionViewModel
    {
        [Display(Name = "Section ID")]
        public string SectionID { get; set; }

        [Display(Name = "Section Name")]
        public string SectionName { get; set; }

        [Display(Name = "Tipe Pelayanan")]
        public string TipePelayanan { get; set; }

        public string PenanggungJawab { get; set; }
        public string KelompokJasa { get; set; }
        public Nullable<int> Gudang_ID { get; set; }
        public short Lokasi_ID { get; set; }
        public bool StatusAktif { get; set; }
        public bool GudangUtama { get; set; }
        public string KodeNoBukti { get; set; }
        public string PswdManager { get; set; }
        public Nullable<bool> RIOnly { get; set; }
        public string PoliKlinik { get; set; }
        public Nullable<bool> NonCharge { get; set; }
        public Nullable<int> NoUrut { get; set; }
        public bool ICU { get; set; }
        public string NoIP { get; set; }
        public Nullable<bool> RBayi { get; set; }
        public Nullable<byte> GroupSection { get; set; }
        public Nullable<int> PiutangAkun_ID { get; set; }
        public string UnitBisnisID { get; set; }
        public Nullable<int> Customer_ID { get; set; }
        public string IPFarmasi { get; set; }
        public string Pelayanan { get; set; }
        public Nullable<int> PiutangSHAkun_ID { get; set; }
        public Nullable<int> CustomerSH_ID { get; set; }
        public Nullable<bool> BlokCoPay { get; set; }
        public Nullable<double> NilaiKinerjaSection { get; set; }
        public Nullable<bool> SectionAbsenDefault { get; set; }
        public string KelompokPelayanan { get; set; }
        public Nullable<int> MutasiKeluarAkun_ID { get; set; }
        public Nullable<int> MutasiMasukAkun_ID { get; set; }
        public string KelompokKegiatan { get; set; }
        public Nullable<bool> NonKelas { get; set; }
        public Nullable<int> JumlahTT { get; set; }
        public string KelompokSection { get; set; }
        public Nullable<int> DiskonAkun_ID { get; set; }
        public string MappingInhealth { get; set; }
        public string Spesialisasi { get; set; }
        public string MappingBPJS { get; set; }
        public string SectionNameBC { get; set; }
        public string MappingSIRANAP { get; set; }
        public Nullable<bool> PrintBilling { get; set; }
        public Nullable<bool> SectionNonMedis { get; set; }
        public string SectionIdHaiMed { get; set; }
        public Nullable<int> nomorpoli { get; set; }
        public string KelompokLayanan { get; set; }
        public string KodeReg { get; set; }
        public string SectionFarmasiID { get; set; }
        public Nullable<decimal> PlafonHarianJKBM { get; set; }
        public string KelompokSectionJKBM { get; set; }
    }
}
