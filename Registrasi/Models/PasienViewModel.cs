﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registrasi.Models
{
    public class PasienViewModel
    {
        public ListDetail<KunjunganViewModel> DetailKunjungan_List { get; set; }
        public ListDetail<DetailRegistrasiViewModel> DetailRegistrasi_List { get; set; }
        public string typeNRM { get; set; }
        public string NRM { get; set; }
        [Required]
        public string NamaPasien { get; set; }
        public string NoIdentitas { get; set; }
        [Required]
        public string JenisKelamin { get; set; }
        [DataType(DataType.Date)]
        public DateTime TglLahir { get; set; }
        public string TglLahir_View { get; set; }
        public bool TglLahirDiketahui { get; set; }
        public short UmurSaatInput { get; set; }
        public string Pekerjaan { get; set; }
        [Required]
        public string Alamat { get; set; }
        public Nullable<short> PropinsiID { get; set; }
        public string NamaPropinsi { get; set; }
        public string KabupatenID { get; set; }
        public string NamaKabupaten { get; set; }
        public string KecamatanID { get; set; }
        public string NamaKecamatan { get; set; }
        public string DesaID { get; set; }
        public string NamaDesa { get; set; }
        public string BanjarID { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string JenisPasien { get; set; }
        [Required]
        public int JenisKerjasamaID { get; set; }
        public bool AnggotaBaru { get; set; }
        public Nullable<decimal> CustomerKerjasamaID { get; set; }
        public string CompanyID { get; set; }
        public string KodePerusahaan { get; set; }
        public string NoKartu { get; set; }
        public string Klp { get; set; }
        public string JabatanDiPerusahaan { get; set; }
        public bool PasienLoyal { get; set; }
        public int TotalKunjunganRawatInap { get; set; }
        public int TotalKunjunganRawatJalan { get; set; }
        public int KunjunganRJ_TahunIni { get; set; }
        public int KunjunganRI_TahunIni { get; set; }
        public string EtnisID { get; set; }
        [Required]
        public string NationalityID { get; set; }
        public bool PasienVVIP { get; set; }
        public bool PasienKTP { get; set; }
        public Nullable<System.DateTime> TglInput { get; set; }
        public Nullable<short> UserID { get; set; }
        public string CaraDatangPertama { get; set; }
        public string DokterID_ReferensiPertama { get; set; }
        public bool SedangDirawat { get; set; }
        public Nullable<int> JmlKunjunganHD { get; set; }
        public int JmlKunjunganDB { get; set; }
        public bool KomunitasDB { get; set; }
        public Nullable<System.DateTime> TglMulaiKomunitasDB { get; set; }
        public Nullable<int> JmlRIThnIni { get; set; }
        public Nullable<int> JmlRIOpnameIni { get; set; }
        public Nullable<System.DateTime> LastDateRI { get; set; }
        public string KodePos { get; set; }
        public Nullable<System.DateTime> TglRegKasusKecelakaanBaru { get; set; }
        public string NoRegKecelakaanBaru { get; set; }
        public bool Aktive_Keanggotaan { get; set; }
        [Required]
        public string Agama { get; set; }
        public string NoAnggotaE { get; set; }
        public string NamaAnggotaE { get; set; }
        public string GenderAnggotaE { get; set; }
        public Nullable<System.DateTime> TglTidakAktif { get; set; }
        public string TipePasienAsal { get; set; }
        public string NoKartuAsal { get; set; }
        public string NamaPerusahaanAsal { get; set; }
        public Nullable<bool> PenanggungIsPasien { get; set; }
        public string PenanggungNRM { get; set; }
        public string PenanggungNama { get; set; }
        public string PenanggungAlamat { get; set; }
        public string PenanggungPhone { get; set; }
        public string PenanggungKTP { get; set; }
        public string PenanggungHubungan { get; set; }
        public Nullable<bool> Aktif { get; set; }
        public Nullable<bool> PasienBlackList { get; set; }
        public string NamaIbuKandung { get; set; }
        public bool NonPBI { get; set; }
        public string KdKelas { get; set; }
        public string Pendidikan { get; set; }
        public string MobileUserId { get; set; }
        public string HaiMedRelasiId { get; set; }
        public string HaiMedUserId { get; set; }
        public Nullable<bool> SinkronisasiHaiMed { get; set; }
        public string NamaPerusahaan { get; set; }
        public string NamaKelas { get; set; }
        public string TempatLahir { get; set; }
        public string Status { get; set; }
        public string Emoticon { get; set; }
        public string JenisKerjasama { get; set; }
        public string RujukanDari { get; set; }
        public string StatusKawin { get; set; }
        public string Golda { get; set; }
    }
}
