﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class KartuViewModel
    {
        public decimal CustomerKerjasamaID { get; set; }
        public string NoAnggota { get; set; }
        public string Nama { get; set; }
        public bool Active { get; set; }
        public string Klp { get; set; }
        public string Gender { get; set; }
        public string PerusahaanID { get; set; }
        public string PerusahaanNama { get; set; }
        public string Kode_Customer { get; set; }
        public string Nama_Customer { get; set; }
        public string KelasID { get; set; }
    }
}