﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class WaktuPraktekViewModel
    {
        public int WaktuID { get; set; }
        public string NamaWaktu { get; set; }
        public System.DateTime FromJam { get; set; }
        public System.DateTime ToJam { get; set; }
        public string Keterangan { get; set; }
    }
}