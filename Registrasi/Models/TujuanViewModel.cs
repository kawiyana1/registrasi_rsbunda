﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registrasi.Models
{ 
    public class TujuanViewModel
    {
        [Display(Name = "ID Poli")]
        public string SectionID { get; set; }

        [Display(Name = "ID Dokter")]
        public string DokterID { get; set; }

        [Display(Name = "ID Waktu Praktek")]
        public int WaktuID { get; set; }

        [Display(Name = "Tanggal")]
        public DateTime Tanggal { get; set; }

        public string Tanggal_view { get; set; }

        [Display(Name = "Poli")]
        public string NamaSection { get; set; }

        [Display(Name = "Antrian")]
        public int Antrian { get; set; }

        [Display(Name = "Dokter")]
        public string NamaDOkter { get; set; }

        [Display(Name = "Waktu")]
        public string Keterangan { get; set; }

        [Display(Name = "Hari")]
        public string Hari { get; set; }

    }
}
