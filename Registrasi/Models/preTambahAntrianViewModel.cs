﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class Response
    {
    }

    public class preTambahAntrianViewModel
    {
        public preTambahAntrianViewModel()
        {
            metaData = new MetaData();
        }
        public MetaData metaData { get; set; }
        public Response response { get; set; }
        public bool isSuccess { get; set; }
    }
}