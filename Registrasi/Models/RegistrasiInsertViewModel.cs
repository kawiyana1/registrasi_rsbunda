﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class RegistrasiInsertViewModel
    {
        [Display(Name = "No Registrasi")]
        public string NoReg { get; set; }

        [Display(Name = "Tgl Registrasi ")]
        public DateTime TglReg { get; set; }
        public string TglReg_View { get; set; }

        [Display(Name = "Jam Registrasi")]
        public DateTime? JamReg { get; set; }
        public string JamReg_View { get; set; }

        [Display(Name = "No Reservasi")]
        public string NoReservasi { get; set; }

        [Display(Name = "Pasien Baru")]
        public bool PasienBaru { get; set; }

        [Required]
        [Display(Name = "NRM")]
        public string NRM { get; set; }

        [Display(Name = "Nama")]
        public string NamaPasien { get; set; }

        [Display(Name = "No Identitas")]
        public string NoIdentitas { get; set; }

        [Display(Name = "Gender")]
        public string JenisKelamin { get; set; }

        [Display(Name = "Umur")]
        public int? Umur { get; set; }

        public string UmurView { get; set; }

        [Display(Name = "Tgl Lahir")]
        public DateTime? TglLahir { get; set; }
        public string TglLahir_View { get; set; }

        [Display(Name = "Tgl Lahir Diketahui")]
        public bool TglLahirDiketahui { get; set; }

        [Display(Name = "Alamat")]
        public string Alamat { get; set; }

        [Display(Name = "Kabupaten")]
        public string Nama_Kabupaten { get; set; }

        [Display(Name = "Phone")]
        public string Phone { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Tipe Pasien")]
        public int JenisPasienKerjasamaID { get; set; }

        public string JenisPasienKerjasama { get; set; }

        [Display(Name = "Pasien KTP")]
        public bool PasienKTP { get; set; }

        [Display(Name = "Anggota Baru")]
        public bool AnggotaBaruReg { get; set; }

        [Display(Name = "Kode Perusahaan")]
        public string Kode_Customer { get; set; }

        [Display(Name = "Perusahaan")]
        public string Nama_Customer { get; set; }

        [Display(Name = "Kelas")]
        public string Kelas { get; set; }

        [Display(Name = "No Kartu")]
        public string NoKartu { get; set; }

        public string SectionID { get; set; }

        public string Dokter_ID { get; set; }

        public int? WaktuPraktek_ID { get; set; }

        public string KelasID { get; set; }

        public DateTime? Tanggal { get; set; }
        public DateTime? ClickPasienBaru { get; set; }

        public string Tanggal_view { get; set; }

        [Required]
        [Display(Name = "Section")]
        public string NamaSection { get; set; }

        [Display(Name = "No Antrian")]
        public int Antrian { get; set; }

        public string Dokter { get; set; }

        public string Waktu { get; set; }

        [Display(Name = "Block Vitamin")]
        public bool BlockVitamin { get; set; }

        [Display(Name = "Block Suplemen")]
        public bool BlockSuplemen { get; set; }

        [Display(Name = "Block Obat Herbal")]
        public bool BlockObatHerbal { get; set; }

        [Display(Name = "Block Obat Kosmetik")]
        public bool BlockObatKosmetik { get; set; }

        [Display(Name = "Block Obat Jiwa")]
        public bool BlockObatJiwa { get; set; }

        public bool Rujukan { get; set; }

        public bool PasienPribadi { get; set; }

        public bool CP { get; set; }

        public string DokterPengirim_ID { get; set; }

        [Display(Name = "Dokter Pengirim")]
        public string DokterPengirim { get; set; }

        public string VendorID { get; set; }

        [Display(Name = "Vandor Pengirim")]
        public string VendorPengirim { get; set; }

        public string Memo { get; set; }

        [Display(Name = "Cara Datang")]
        public string CaraDatang { get; set; }

        [Display(Name = "No Rujukan")]
        public string NoRujukanBPJS { get; set; }

        [Display(Name = "Diagnosa")]
        public string Diagnosa { get; set; }
        public string DiagnosaNama { get; set; }

        [Display(Name = "Pasien RS")]
        public bool PasienRS { get; set; }

        [Display(Name = "NRM")]
        public string PenanggungNRM { get; set; }

        [Required]
        [Display(Name = "Nama")]
        public string PenanggungNama { get; set; }

        [Required]
        [Display(Name = "Alamat")]
        public string PenanggungAlamat { get; set; }

        [Display(Name = "Phone")]
        public string PenanggungPhone { get; set; }

        [Required]
        [Display(Name = "Hubungan")]
        public string PenanggungHubungan { get; set; }

        [Display(Name = "ID ICD")]
        public string ICD_ID { get; set; }

        [Display(Name = "Deskripsi")]
        public string Deskripsi { get; set; }

        [Display(Name = "Keterangan")]
        public string Keterangan { get; set; }

        [Display(Name = "Pasien Asuransi")]
        public bool PasienAsuransi { get; set; }

        [Display(Name = "HC/SHE")]
        public bool HC_SHE { get; set; }

        [Display(Name = "IKS")]
        public bool IKS { get; set; }

        public string AsuransiID { get; set; }

        [Display(Name = "Payor Company")]
        public string PayorCompany { get; set; }

        [Display(Name = "First Payor")]
        public bool FisrtPayor { get; set; }

        public string AssCompany_ID { get; set; }

        [Display(Name = "Assisting Company")]
        public string AssistingCompany { get; set; }

        [Display(Name = "No Kartu Asuransi")]
        public string NoKartuAsuransi { get; set; }

        [Display(Name = "No SJP")]
        public string NoSJP { get; set; }

        [Display(Name = "No SEP")]
        public string NoSep { get; set; }

        [Display(Name = "Pasien Paket")]
        public bool PasienPaket { get; set; }

        public string PaketJasa_ID { get; set; }

        [Display(Name = "Paket")]
        public string Paket { get; set; }

        public bool HD { get; set; }

        public bool Diabetes { get; set; }

        [Display(Name = "Tipe Pembayaran")]
        public string TipePembayaran { get; set; }

        public bool PasienBayi { get; set; }

        public bool PasienCovid { get; set; }

        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public bool PasienPanjer { get; set; }
        public string DiagnosaKet { get; set; }
        public string Faskes { get; set; }
        public string FaskesNama { get; set; }
        public string NIK { get; set; }
        public string getRujukanFrom { get; set; }
        public string PoliRujukan { get; set; }
        public string PoliRujukanNama { get; set; }
    }
}