﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class PasienBermasalahViewModel
    {
        public List<PasienBermasalahViewModel> PasienBermasalahDetail { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string KategoriMasalah { get; set; }
        public string DeskripsiMasalah { get; set; }
        public string Emoticon { get; set; }
        public System.DateTime TglInput { get; set; }
        public string TglInput_View { get; set; }
        public Nullable<short> UserID { get; set; }
    }
}