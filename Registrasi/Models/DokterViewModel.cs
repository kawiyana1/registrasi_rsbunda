﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registrasi.Models
{
    public class DokterViewModel
    {
        [Display(Name = "ID Dokter")]
        public string DokterID { get; set; }

        [Display(Name = "Nama Dokter")]
        public string NamaDOkter { get; set; }

        [Display(Name = "Alamat")]
        public string Alamat { get; set; }
    }
}
