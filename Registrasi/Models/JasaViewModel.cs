﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class JasaViewModel
    {
        public string JasaID { get; set; }
        public string JasaName { get; set; }
    }
}