﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registrasi.Models
{
    public class AsuransiViewModel
    {
        [Display(Name = "Kode")]
        public string Kode_Customer { get; set; }

        [Display(Name = "Nama")]
        public string Nama_Customer { get; set; }
    }
}
