﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class AnggotaViewModel
    {
        public string NoAnggota { get; set; }
        public string Nama { get; set; }
    }

}