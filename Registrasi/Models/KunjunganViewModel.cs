﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class KunjunganViewModel
    {
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string Dokter { get; set; }
        public Nullable<System.DateTime> TglTerakhirKunjungan { get; set; }
        public string TglTerakhirKunjungan_View { get; set; }
    }
}