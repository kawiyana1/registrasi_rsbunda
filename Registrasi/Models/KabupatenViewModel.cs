﻿using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class KabupatenViewModel
    {
        public string Kode_Kabupaten { get; set; }
        public string Nama_Kabupaten { get; set; }
        public Nullable<short> PropinsiID { get; set; }
        public string Nama_Propinsi { get; set; }
    }
}