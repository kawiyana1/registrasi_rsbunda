﻿using LZStringCSharp;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using static Registrasi.Models.BpjsModel;

namespace Registrasi.Models
{
    public class BpjsSEPService : BpjsService
    {
        #region REFERENSI
        public ReferensiDiagnosaModel ReferensiDiagnosa(string param)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/referensi/diagnosa/{param}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new ReferensiDiagnosaModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<ReferensiDiagnosaResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public ReferensiProcedureModel ReferensiProcedure(string param)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/referensi/procedure/{param}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new ReferensiProcedureModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<ReferensiProcedureResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public ReferensiPoliModel ReferensiPoli(string param)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/referensi/poli/{param}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new ReferensiPoliModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<ReferensiPoliResponse>(responseDecrypt);
                }
            }
            return response;
        }

        //Jenis Faskes (1. Faskes 1, 2. Faskes 2/RS)
        public ReferensiFasilitasKesehatanModel ReferensiFasilitasKesehatan(string parameter, int? jenisfaskes)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/referensi/faskes/{parameter}/{jenisfaskes}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new ReferensiFasilitasKesehatanModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<ReferensiFasilitasKesehatanResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public ReferensiTindakanModel ReferensiTindakan(string parameter)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/referensi/procedure/{parameter}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new ReferensiTindakanModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<ReferensiTindakanResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public ReferensiKelasRawatModel ReferensiKelasRawat()
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/referensi/kelasrawat", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new ReferensiKelasRawatModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<ReferensiKelasRawatResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public ReferensiDokterModel ReferensiDokter(string parameter)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/referensi/dokter/{parameter}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new ReferensiDokterModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<ReferensiDokterResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public ReferensiSpesialistikModel ReferensiSpesialistik()
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/referensi/spesialistik", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new ReferensiSpesialistikModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<ReferensiSpesialistikResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public ReferensiRuangRawatModel ReferensiRuangRawat()
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/referensi/ruangrawat", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new ReferensiRuangRawatModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<ReferensiRuangRawatResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public ReferensiCaraKeluarModel ReferensiCaraKeluar()
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/referensi/carakeluar", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new ReferensiCaraKeluarModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<ReferensiCaraKeluarResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public ReferensiPascaPulangModel ReferensiPascaPulang()
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/referensi/pascapulang", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new ReferensiPascaPulangModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<ReferensiPascaPulangResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public ReferensiPropinsiModel ReferensiPropinsi()
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/referensi/propinsi", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new ReferensiPropinsiModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<ReferensiPropinsiResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public ReferensiKabupatenModel ReferensiKabupaten(string parameter)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/referensi/kabupaten/propinsi/{parameter}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new ReferensiKabupatenModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<ReferensiKabupatenResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public ReferensiKecamatanModel ReferensiKecamatan(string parameter)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/referensi/kecamatan/kabupaten/{parameter}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new ReferensiKecamatanModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<ReferensiKecamatanResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public ReferensiDPJPModel ReferensiDPJP(string parameter, string tglpelayanan, string spesialis)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/referensi/dokter/pelayanan/{parameter}/tglPelayanan/{tglpelayanan}/Spesialis/{spesialis}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new ReferensiDPJPModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<ReferensiDPJPResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public ReferensiDiagnosaPRBModel ReferensiDiagnosaPRB()
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/referensi/diagnosaprb", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new ReferensiDiagnosaPRBModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<ReferensiDiagnosaPRBResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public ReferensiObatPRBModel ReferensiObatPRB(string param)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/referensi/obatprb/{param}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new ReferensiObatPRBModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<ReferensiObatPRBResponse>(responseDecrypt);
                }
            }
            return response;
        }
        #endregion

        #region SEP
        public SEPResponseModel SEPInsert(SEPDataModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/SEP/2.0/insert", Method.POST);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            var item = new { request = new { t_sep = model } };
            request.RequestFormat = DataFormat.Json;
#pragma warning disable CS0618 // Type or member is obsolete
            request.AddBody(item);
#pragma warning restore CS0618 // Type or member is obsolete
            var a = JsonConvert.SerializeObject(item);
            var response = new SEPResponseModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<SEPResponse>(responseDecrypt);
                }
            }

            return response;
        }

        public SEPUpdateDeleteResponseModel SEPUpdate(SEPUpdateModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/SEP/2.0/update", Method.PUT);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            var item = new { request = new { t_sep = model } };
            request.RequestFormat = DataFormat.Json;
            request.AddBody(item);
            var a = JsonConvert.SerializeObject(item);
            var response = new SEPUpdateDeleteResponseModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    response.response = responseEncrypt.Data.response;
                }
            }
            return response;
        }

        public SEPUpdateDeleteResponseModel SEPHapus(SEPDeleteModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/SEP/2.0/delete", Method.DELETE);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            var item = new { request = new { t_sep = model } };
            request.RequestFormat = DataFormat.Json;
            var response = new SEPUpdateDeleteResponseModel();
            var json = new JavaScriptSerializer().Serialize(item);
            request.AddParameter("application/x-www-form-urlencoded", json, ParameterType.RequestBody);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    //var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    //var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    //response.response = JsonConvert.DeserializeObject<string>(responseDecrypt);
                    response.response = responseEncrypt.Data.response;
                }
            }
            return response;
        }

        public SEPUpdateDeleteResponseModel SEPUpdateTanggalPulang(SEPUpdateTanggalPulangModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/Sep/updtglplg", Method.PUT);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            var item = new { request = new { t_sep = model } };
            request.RequestFormat = DataFormat.Json;
            request.AddBody(item);
            var response = new SEPUpdateDeleteResponseModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<string>(responseDecrypt);
                }
            }
            return response;
        }

        public SEPCariModel SEPCari(string parameter)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/SEP/{parameter}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new SEPCariModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<SEPCariResponse>(responseDecrypt);
                }
            }
            return response;
        }


        #endregion

        #region SEP INTERNAL
        public SEPInternalResponseModel SEPInternal(string parameter)
        {
            var client = new RestClient(BaseUrl);
            var tanggalpelayanan = DateTime.Today.ToString("yyyy-MM-dd");
            var request = new RestRequest($"{ServiceName}/SEP/Internal/{parameter}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new SEPInternalResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<SEPInternalResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public SEPInternalDeleteResponseModel SEPInternalHapus(SEPInternalDeleteModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/SEP/Internal/delete", Method.DELETE);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            var item = new { request = new { t_sep = model } };
            request.RequestFormat = DataFormat.Json;
            var response = new SEPInternalDeleteResponseModel();
            var json = new JavaScriptSerializer().Serialize(item);
            request.AddParameter("application/x-www-form-urlencoded", json, ParameterType.RequestBody);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<string>(responseDecrypt);
                }
            }
            return response;
        }

        #endregion

        #region APPROVAL
        public StringResponseModel SEPPengajuan(ApprovalDataModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/Sep/pengajuanSEP", Method.POST);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            request.AddHeader("user_key", UserKey);
            var item = new { request = new { t_sep = model } };
            request.RequestFormat = DataFormat.Json;
            request.AddBody(item);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new StringResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<string>(responseDecrypt);
                }
            }
            return response;
        }

        public StringResponseModel ApprovalPengajuan(ApprovalDataModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/Sep/aprovalSEP", Method.POST);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            request.AddHeader("user_key", UserKey);
            var item = new { request = new { t_sep = model } };
            request.RequestFormat = DataFormat.Json;
            request.AddBody(item);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new StringResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<string>(responseDecrypt);
                }
            }
            return response;
        }
        #endregion

        #region PESERTA
        public PesertaNoKartuBPJSModel PesertaNoKartuBPJS(string parameter)
        {
            var client = new RestClient(BaseUrl);
            var tanggalpelayanan = DateTime.Today.ToString("yyyy-MM-dd");
            var request = new RestRequest($"{ServiceName}/Peserta/nokartu/{parameter}/tglSEP/{tanggalpelayanan}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new PesertaNoKartuBPJSModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<PesertaResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public PesertaNIKModel PesertaNIK(string parameter)
        {
            var client = new RestClient(BaseUrl);
            var tanggalpelayanan = DateTime.Today.ToString("yyyy-MM-dd");
            var request = new RestRequest($"{ServiceName}/Peserta/nik/{parameter}/tglSEP/{tanggalpelayanan}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new PesertaNIKModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<PesertaResponse>(responseDecrypt);
                }
            }
            return response;
        }
        #endregion

        #region RUJUKAN

        public SpesialistikRujukanModel SpesialistikRujukan(string ppkrujukan, string tglrujukan)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/Rujukan/ListSpesialistik/PPKRujukan/{ppkrujukan}/TglRujukan/{tglrujukan}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new SpesialistikRujukanModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<SpesialistikRujukanResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public SarananModel Sarana(string ppkrujukan)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/Rujukan/ListSarana/PPKRujukan/{ppkrujukan}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new SarananModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<SaranaResponse>(responseDecrypt);
                }
            }
            return response;
        }


        public RujukanModel RujukanBerdasarkanNomorRujukan(string parameter)
        {
            var client = new RestClient(BaseUrl);
            RestRequest request;
            request = new RestRequest($"{ServiceName}/Rujukan/{parameter}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new RujukanModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {

                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<RujukanResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public RujukanModel RujukanBerdasarkanNomorRujukanRS(string parameter)
        {
            var client = new RestClient(BaseUrl);
            RestRequest request;
            request = new RestRequest($"{ServiceName}/Rujukan/RS/{parameter}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new RujukanModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<RujukanResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public RujukanModel RujukanBerdasarkanNomorkartu(string parameter)
        {
            var client = new RestClient(BaseUrl);
            RestRequest request;

            request = new RestRequest($"{ServiceName}/Rujukan/Peserta/{parameter}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new RujukanModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<RujukanResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public RujukanListModel RujukanBerdasarkanNoKartuListPCare(string parameter)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/Rujukan/List/Peserta/{parameter}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new RujukanListModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<RujukanListResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public RujukanListModel RujukanBerdasarkanNoKartuListRS(string parameter)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/Rujukan/RS/List/Peserta/{parameter}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new RujukanListModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<RujukanListResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public RujukanListModel RujukanBerdasarkanTglRujukanList(string parameter)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/Rujukan/List/TglRujukan/{parameter}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new RujukanListModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<RujukanListResponse>(responseDecrypt);
                }
            }

            return response;
        }

        public RujukanResponseModel RujukanInsert(RujukanInsertModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/Rujukan/2.0/insert", Method.POST);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            var item = new { request = new { t_rujukan = model } };
            request.RequestFormat = DataFormat.Json;
            request.AddBody(item);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new RujukanResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<RujukanDataResponse>(responseDecrypt);
                }
            }

            return response;
        }

        public RujukanResponseUpdateDeleteModel RujukanUpdate(RujukanUpdateModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/Rujukan/2.0/Update", Method.PUT);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            var item = new { request = new { t_rujukan = model } };
            request.RequestFormat = DataFormat.Json;
            request.AddBody(item);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new RujukanResponseUpdateDeleteModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<string>(responseDecrypt);
                }
            }

            return response;
        }

        public RujukanResponseUpdateDeleteModel RujukanHapus(RujukanDeleteModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/Rujukan/delete", Method.DELETE);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            var item = new { request = new { t_rujukan = model } };
            request.RequestFormat = DataFormat.Json;
            var json = new JavaScriptSerializer().Serialize(item);
            request.AddParameter("application/x-www-form-urlencoded", json, ParameterType.RequestBody);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new RujukanResponseUpdateDeleteModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<string>(responseDecrypt);
                }
            }

            return response;
        }
        #endregion

        #region HOSTORI
        public MonitoringHistoriModel MonitoringHistori(string nokartu, string tglmulaipencarian, string tglakhirpencarian)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/monitoring/HistoriPelayanan/NoKartu/{nokartu}/tglMulai/{tglmulaipencarian}/tglAkhir/{tglakhirpencarian}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new MonitoringHistoriModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<MonitoringHistoriResponse>(responseDecrypt);
                }
            }
            return response;
        }
        #endregion

        #region DATA KUNJUNGAN
        public MonitoringDataKunjunganModel MonitoringDataKunjungan(string tanggal, string jnsPelayanan)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/Monitoring/Kunjungan/Tanggal/{tanggal}/JnsPelayanan/{jnsPelayanan}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new MonitoringDataKunjunganModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<MonitoringDataKunjunganResponse>(responseDecrypt);
                }
            }
            return response;
        }
        #endregion

        #region DATA KLAIM
        public MonitoringDataKlaimModel MonitoringDataKlaim(string tanggal, string jnsPelayanan, string status)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/Monitoring/Klaim/Tanggal/{tanggal}/JnsPelayanan/{jnsPelayanan}/Status/{status}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new MonitoringDataKlaimModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<MonitoringDataKlaimResponse>(responseDecrypt);
                }
            }
            return response;
        }
        #endregion

        #region DATA JASA RAHARJA
        public MonitoringDataJasaRaharjaModel MonitoringDataJasaRaharja(string tanggalMulai, string tanggalAkhir, string jnsPelayanan)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/monitoring/JasaRaharja/JnsPelayaan/{jnsPelayanan}/tglMulai/{tanggalMulai}/tglAkhir/{tanggalAkhir}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new MonitoringDataJasaRaharjaModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<MonitoringDataJasaRaharjaResponse>(responseDecrypt);
                }
            }
            return response;
        }
        #endregion

        #region RENCANA KONTROL
        public RencanaKontrolResponseModel RencanaKontrolInsert(RencanaKontrolDataModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/RencanaKontrol/insert", Method.POST);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            var item = new { request = model };
            request.RequestFormat = DataFormat.Json;
            request.AddBody(item);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new RencanaKontrolResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<RencanaKontrolResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public RencanaKontrolResponseModel RencanaKontrolUpdate(RencanaKontrolUpdateDataModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/RencanaKontrol/Update", Method.PUT);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            var item = new { request = model };
            request.RequestFormat = DataFormat.Json;
            request.AddBody(item);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new RencanaKontrolResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<RencanaKontrolResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public RencanaKontrolDeleteResponseModel RencanaKontrolHapus(RencanaKontrolDeleteModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/RencanaKontrol/Delete", Method.DELETE);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            var item = new { request = model };
            request.RequestFormat = DataFormat.Json;
            var json = new JavaScriptSerializer().Serialize(item);
            request.AddParameter("application/x-www-form-urlencoded", json, ParameterType.RequestBody);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new RencanaKontrolDeleteResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<string>(responseDecrypt);
                }
            }
            return response;
        }

        public CariNomorSuratKontrolResponseModel CariNomorSuratKontrol(string noSuratKontrol)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/RencanaKontrol/noSuratKontrol/{noSuratKontrol}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new CariNomorSuratKontrolResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<CariNomorSuratKontrolResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public SEPRencanaKontrolResponseModel SEPRencanaKontrol(string nosep)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/RencanaKontrol/nosep/{nosep}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new SEPRencanaKontrolResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<SEPRencanaKontrolResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public DataNomorSuratKontrolResponseModel DataNomorSuratKontrol(string tglAwal, string tglAkhir, string filter)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/RencanaKontrol/ListRencanaKontrol/tglAwal/{tglAwal}/tglAkhir/{tglAkhir}/filter/{filter}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new DataNomorSuratKontrolResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<DataNomorSuratKontrolResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public DataPoliResponseModel DataPoli(string jnsKontrol, string nomor, string tglRencanaKontrol)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/RencanaKontrol/ListSpesialistik/JnsKontrol/{jnsKontrol}/nomor/{nomor}/TglRencanaKontrol/{tglRencanaKontrol}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new DataPoliResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<DataPoliResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public DataDokterResponseModel DataDokter(string jnsKontrol, string kdPoli, string tglRencanaKontrol)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/RencanaKontrol/JadwalPraktekDokter/JnsKontrol/{jnsKontrol}/KdPoli/{kdPoli}/TglRencanaKontrol/{tglRencanaKontrol}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new DataDokterResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<DataDokterResponse>(responseDecrypt);
                }
            }
            return response;
        }
        #endregion

        #region SPRI
        public SPRIResponseModel SPRIInsert(SPRIDataModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/RencanaKontrol/InsertSPRI", Method.POST);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            var item = new { request = model };
            request.RequestFormat = DataFormat.Json;
            request.AddBody(item);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new SPRIResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<SPRIResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public SPRIResponseModel SPRIUpdate(SPRIUpdateDataModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/RencanaKontrol/UpdateSPRI", Method.PUT);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            var item = new { request = model };
            request.RequestFormat = DataFormat.Json;
            request.AddBody(item);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new SPRIResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<SPRIResponse>(responseDecrypt);
                }
            }
            return response;
        }
        #endregion

        #region RUJUK BALIK
        public RujukanBalikResponseModel RujukanBalikInsert(RujukanBalikDataModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/PRB/insert", Method.POST);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            var item = new { request = new { t_prb = model } };
            request.RequestFormat = DataFormat.Json;
            request.AddBody(item);
            var json = new JavaScriptSerializer().Serialize(item);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new RujukanBalikResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<RujukanBalikResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public RujukanBalikUpdateResponseModel RujukanBalikUpdate(RujukanBalikDataUpdateModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/PRB/Update", Method.PUT);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            var item = new { request = new { t_prb = model } };
            request.RequestFormat = DataFormat.Json;
            request.AddBody(item);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new RujukanBalikUpdateResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<string>(responseDecrypt);
                }
            }
            return response;
        }

        public RujukanBalikDeleteResponseModel RujukanBalikHapus(RujukanBalikDeleteModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/PRB/Delete", Method.DELETE);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            var item = new { request = new { t_prb = model } };
            request.RequestFormat = DataFormat.Json;
            var json = new JavaScriptSerializer().Serialize(item);
            request.AddParameter("application/x-www-form-urlencoded", json, ParameterType.RequestBody);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new RujukanBalikDeleteResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<string>(responseDecrypt);
                }
            }
            return response;
        }

        public NomorSRBResponseModel NomorSRB(string noSRB, string nosep)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/prb/{noSRB}/nosep/{nosep}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new NomorSRBResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<NomorSRBResponse>(responseDecrypt);
                }
            }
            return response;
        }
        public DataPRBResponseModel DataPRB(string tglAwal, string tglAkhir)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/prb/tglMulai/{tglAwal}/tglAkhir/{tglAkhir}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new DataPRBResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<DataPRBResponse>(responseDecrypt);
                }
            }
            return response;
        }
        #endregion

        #region RUJUKAN KHUSUS
        public RujukanKhususResponseModel RujukanKhususInsert(RujukanKhususDataModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/Rujukan/Khusus/insert", Method.POST);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            request.RequestFormat = DataFormat.Json;
            request.AddBody(model);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new RujukanKhususResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<RujukanKhususResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public RujukanKhususDeleteResponseModel RujukanKhususHapus(RujukanKhususDeleteModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/Rujukan/Khusus/delete", Method.DELETE);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            var item = new { request = new { t_rujukan = model } };
            request.RequestFormat = DataFormat.Json;
            var json = new JavaScriptSerializer().Serialize(item);
            request.AddParameter("application/x-www-form-urlencoded", json, ParameterType.RequestBody);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new RujukanKhususDeleteResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<string>(responseDecrypt);
                }
            }
            return response;
        }

        public ListRujukanKhususResponseModel ListRujukanKhusus(string bulan, string tahun)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/Rujukan/Khusus/List/Bulan/{bulan}/Tahun/{tahun}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new ListRujukanKhususResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<ListRujukanKhususResponse>(responseDecrypt);
                }
            }
            return response;
        }
        #endregion

        #region FINGER PRINT
        public FingerPrintResponseModel FingerPrint(string peserta, string tglpelayanan)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/SEP/FingerPrint/Peserta/{peserta}/TglPelayanan/{tglpelayanan}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new FingerPrintResponseModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<FingerPrintResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public ListFingerPrintResponseModel ListFingerPrint(string tglpelayanan)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/SEP/FingerPrint/List/Peserta/TglPelayanan/{tglpelayanan}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new ListFingerPrintResponseModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<ListFingerPrintResponse>(responseDecrypt);
                }
            }
            return response;
        }
        #endregion

        #region SUPLESI
        public SuplesiResponseModel Suplesi(string nokartu, string tglpelayanan)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/sep/JasaRaharja/Suplesi/{nokartu}/tglPelayanan/{tglpelayanan}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new SuplesiResponseModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<SuplesiResponse>(responseDecrypt);
                }
            }
            return response;
        }

        public DataIndukKecelakaanResponseModel DataIndukKecelakaan(string nokartu)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceName}/sep/KllInduk/List/{nokartu}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKey);
            var response = new DataIndukKecelakaanResponseModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<DataIndukKecelakaanResponse>(responseDecrypt);
                }
            }
            return response;
        }
        #endregion

        #region JADWAL
        public JadwalDokterResponseModel JadwalDokter(string kodepoli, string tanggal)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceNameWsBPJS}/jadwaldokter/kodepoli/{kodepoli}/tanggal/{tanggal}", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKeyWsBPJS);
            var response = new JadwalDokterResponseModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<List<JadwalDokterResponse>>(responseDecrypt);
                }
            }
            return response;
        }

        public JadwalDokterUpdateResponseModel JadwalDokterUpdate(JadwalDokterDataUpdateModel model)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceNameWsBPJS}/jadwaldokter/updatejadwaldokter", Method.POST);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKeyWsBPJS);
            request.AddHeader("Content-Type", "Application/x-www-form-urlencoded");
            request.RequestFormat = DataFormat.Json;
            request.AddBody(model);
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            var response = new JadwalDokterUpdateResponseModel();
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
            }
            return response;
        }

        public PoliResponseModel Poli()
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest($"{ServiceNameWsBPJS}/ref/poli", Method.GET);
            request.AddHeader("X-cons-id", CustomerId);
            request.AddHeader("X-timestamp", Timestamp.ToString());
            request.AddHeader("X-signature", GetSignature);
            request.AddHeader("user_key", UserKeyWsBPJS);
            var response = new PoliResponseModel();
            var responseEncrypt = client.Execute<BpjsApiEncryptedResponse>(request);
            if (responseEncrypt.Data != null)
            {
                response.metaData = responseEncrypt.Data.metaData;
                if (responseEncrypt.Data.response != null)
                {
                    var lzstring = Decrypt(keyDecrypt, responseEncrypt.Data.response);
                    var responseDecrypt = LZString.DecompressFromEncodedURIComponent(lzstring);
                    response.response = JsonConvert.DeserializeObject<List<PoliResponse>>(responseDecrypt);
                }
            }
            return response;
        }
        #endregion

        #region ===== Tambah Antrian
        public IRestResponse<preTambahAntrianViewModel> preTambahAntrian(string BaseUrl, string ServiceName)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest(ServiceName, Method.GET);
            var response = client.Execute<preTambahAntrianViewModel>(request);
            return response;
        }
        #endregion
    }

}