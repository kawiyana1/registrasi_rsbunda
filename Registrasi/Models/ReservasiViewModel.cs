﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class ReservasiViewModel
    {
        public string NoReservasi { get; set; }
        public string NoBukti { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime Jam { get; set; }
        public string Jam_View { get; set; }
        public bool PasienBaru { get; set; }
        public string NRM { get; set; }
        [Required]
        public string Nama { get; set; }
        [Required]
        public string Alamat { get; set; }
        public string Phone { get; set; }
        public string UntukSectionID { get; set; }
        public string SectionName { get; set; }
        public string UntukDokterID { get; set; }
        public string NamaDOkter { get; set; }
        public string UntukHari { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime UntukTanggal { get; set; }
        public string UntukTanggal_View { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime UntukJam { get; set; }
        public int NoUrut { get; set; }
        public short User_ID { get; set; }
        public bool Registrasi { get; set; }
        public Nullable<double> JmlAntrian { get; set; }
        public Nullable<int> WaktuID { get; set; }
        public string Keterangan { get; set; }
        public Nullable<int> JenisKerjasamaID { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TanggalLahir { get; set; }
        public string Memo { get; set; }
        public string Email { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglPerkiraan_1 { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> TglPerkiraan_2 { get; set; }
        public Nullable<bool> Tindakan_SC { get; set; }
        public Nullable<bool> Tindakan_Normal { get; set; }
        public string KelasID { get; set; }
        public Nullable<decimal> Deposit { get; set; }
        public string PermintaanKhusus { get; set; }
        public Nullable<bool> Batal { get; set; }
        public Nullable<bool> Paid { get; set; }
        public string PaidNoBukti { get; set; }
        public Nullable<short> UserIDPaid { get; set; }
        public string TipeReservasi { get; set; }
        public string NamaSuami { get; set; }
        public string KodeCUstomerIKS { get; set; }
        public string NRM_AKUN { get; set; }
        public string MobileUserId { get; set; }
        public string CaraReservasi { get; set; }
        public Nullable<bool> SudahLengkap { get; set; }
        public string StatusPeriksa { get; set; }
        public string StatusBayar { get; set; }
        public bool JadwalManual { get; set; }
    }
}