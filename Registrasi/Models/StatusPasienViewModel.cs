﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class StatusPasienViewModel
    {
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string Alamat { get; set; }
        public string JenisPasien { get; set; }
        public string NoReservasi { get; set; }
        public DateTime TglReservasi { get; set; }
        public string TglReservasi_View { get; set; }
        public string NoReg { get; set; }
        public string SectionName { get; set; }
        public DateTime TglReg { get; set; }
        public string TglReg_View { get; set; }
        public string StatusPeriksa { get; set; }
        public string StatusBayar { get; set; }
        public string StatusReservasi { get; set; }
        public string Dokter { get; set; }
    }
}