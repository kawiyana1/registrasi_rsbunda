﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class SettingViewModel
    {
        [Display(Name = "Label")]
        public string IPLabel { get; set; }
        public string PortLabel { get; set; }

        [Display(Name = "Kartu")]
        public string IPKartu { get; set; }
        public string PortKartu { get; set; }

        [Display(Name = "Formulir")]
        public string IPFormulir { get; set; }
        public string PortFormulir { get; set; }

        [Display(Name = "Tracer")]
        public string IPTracer { get; set; }
        public string PortTracer { get; set; }

        [Display(Name = "Tracer RM")]
        public string IPTracerRM { get; set; }
        public string PortTracerRM { get; set; }

        [Display(Name = "Identitas Pasien")]
        public string IPIdentitasPasien { get; set; }
        public string PortIdentitasPasien { get; set; }

        [Display(Name = "Slip Admission")]
        public string IPSlipAdmission { get; set; }
        public string PortSlipAdmission { get; set; }

        [Display(Name = "SEP")]
        public string IPSep { get; set; }
        public string PortSep { get; set; }

        [Display(Name = "Rujukan")]
        public string IPRujukan { get; set; }
        public string PortRujukan { get; set; }

        [Display(Name = "Gelang Dewasa")]
        public string IPGelangD { get; set; }
        public string PortGelangD { get; set; }

        [Display(Name = "Gelang Bayi")]
        public string IPGelangB { get; set; }
        public string PortGelangB { get; set; }

        [Display(Name = "Auto Print")]
        public bool AutoPrint { get; set; }
        public bool AutoPrintKartu { get; set; }
        public bool AutoPrintLabel { get; set; }
        public bool AutoPrintTracer { get; set; }

        [Display(Name = "Jumlah Print Label")]
        public int JumlahPrintLabel { get; set; }

        [Display(Name = "Jumlah Print Gelang")]
        public int JumlahPrintGelang{ get; set; }
    }
}