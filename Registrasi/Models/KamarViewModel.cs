﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class KamarViewModel
    {
        public string NoBed { get; set; }
        public string NoKamar { get; set; }
        public string NamaKamar { get; set; }
        public string SalID { get; set; }
        public byte NoLantai { get; set; }
        public string KelasID { get; set; }
    }
}