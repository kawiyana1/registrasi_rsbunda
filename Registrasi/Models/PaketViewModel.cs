﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registrasi.Models
{
    public class PaketViewModel
    {
        [Display(Name ="ID Jasa")]
        public string JasaID { get; set; }

        [Display(Name = "Nama Jasa")]
        public string JasaName { get; set; }
    }
}
