﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class JadwalPraktekViewModel
    {
        public Nullable<System.DateTime> PublishRegistrasiFromJam { get; set; }
        public Nullable<System.DateTime> PublishRegistrasiToJam { get; set; }
        public string DokterID { get; set; }
        public string SectionID { get; set; }
        public string NamaSection { get; set; }
        [DataType(DataType.Date)]
        public DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public int WaktuID { get; set; }
        public string NamaDOkter { get; set; }
        public string Keterangan { get; set; }
        public Nullable<int> NoAntrianTerakhir { get; set; }
        public string NoRuang { get; set; }
        public string DokterPenggantiID { get; set; }
        public Nullable<bool> StatusAktif { get; set; }
        public string TipePelayanan { get; set; }
        public Nullable<bool> DokterAktif { get; set; }
        public Nullable<bool> JadwalVerified { get; set; }
        public Nullable<bool> JadwalCancel { get; set; }
        public Nullable<System.DateTime> PublishReservasiToJam { get; set; }
        public Nullable<System.DateTime> PublishReservasiFromJam { get; set; }
        public int JmlAntrian { get; set; }
        public Nullable<int> NoAntrianSaatIni { get; set; }
        public Nullable<int> QuotaAntrian { get; set; }
        public string SpesialisName { get; set; }
        public System.DateTime FromJam { get; set; }
        public System.DateTime ToJam { get; set; }
        public string Hari { get; set; }
        public string StatusPraktek { get; set; }
        public string KeteranganStatusPraktek { get; set; }
        public string Expr1 { get; set; }
    }
}