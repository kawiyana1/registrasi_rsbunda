﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class CustomerViewModel
    {
        public decimal CustomerKerjasamaID { get; set; }
        public string Kode_Customer { get; set; }
        public short CustomerID { get; set; }
        public string Nama_Customer { get; set; }
        public string KelasID { get; set; }
        public string KelasKontrakID { get; set; }
        public string JenisKerjasama { get; set; }
        public string NamaKelas { get; set; }
        public bool Active { get; set; }
    }
}