﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Registrasi.Models
{
    public class DesaViewModel
    {
        public string DesaID { get; set; }
        public string DesaNama { get; set; }
        public string KecamatanID { get; set; }
        public string KecamatanNama { get; set; }
        public decimal Kabupaten_ID { get; set; }
        public string Nama_Kabupaten { get; set; }
        public string Kode_Propinsi { get; set; }
        public string Nama_Propinsi { get; set; }
        public string Kode_Kabupaten { get; set; }
        public short PropinsiID { get; set; }
    }
}