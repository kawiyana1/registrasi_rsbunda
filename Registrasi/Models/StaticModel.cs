﻿using Registrasi.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Registrasi.Models
{
    public static class StaticModel
    {
        public static string DbEntityValidationExceptionToString(DbEntityValidationException ex)
        {
            var msgs = new List<string>();
            foreach (var eve in ex.EntityValidationErrors)
            {
                var msg = $"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors:";
                foreach (var ve in eve.ValidationErrors) { msg += $"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\""; }
                msgs.Add(msg);
            }
            return string.Join("\n", msgs.ToArray());
        }

        public static List<SelectListItem> ListSectionStok
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMmSection.Where(x => (x.TipePelayanan == "FARMASI" || x.TipePelayanan == "GUDANG") && x.StatusAktif == true).ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID.ToString(),
                            Text = item.SectionName
                        });
                    }
                    r.Insert(0, new SelectListItem());
                }
                return r;
            }
        }

        public static List<SelectListItem> SelectListNationality
        {
            get
            {
                List<SelectListItem> result;
                using (var s = new SIM_Entities())
                {
                    result = s.mNationality.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nationality,
                        Value = x.NationalityID,
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListJenisKerjasama
        {
            get
            {
                List<SelectListItem> result;
                using (var s = new SIM_Entities())
                {
                    result = s.SIMmJenisKerjasama.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.JenisKerjasama,
                        Value = x.JenisKerjasamaID.ToString(),
                        Selected = x.JenisKerjasamaID == 3
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListDiagnosaPasien
        {
            get
            {
                List<SelectListItem> result;
                using (var s = new SIM_Entities())
                {
                    result = s.mICD.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Descriptions,
                        Value = x.KodeICD.ToString(),
                        //Selected = x.JenisKerjasamaID == 3
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListJenisKerjasamaTarif
        {
            get
            {
                List<SelectListItem> result;
                using (var s = new SIM_Entities())
                {
                    result = s.SIMmJenisKerjasama.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.JenisKerjasama,
                        Value = x.JenisKerjasamaID.ToString(),
                        Selected = x.JenisKerjasamaID == 3
                    });
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListPerusahaan
        {
            get
            {
                List<SelectListItem> result;
                using (var s = new SIM_Entities())
                {
                    result = s.VW_CustomerKerjasama.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama_Customer+" - "+ x.NamaKelas,
                        Value = x.CustomerKerjasamaID.ToString()
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListAnggota
        {
            get
            {
                var result = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {

                    result = s.SIMdAnggotaKerjasama.Take(15).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama,
                        Value = x.NoAnggota
                    });
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListKlp
        {
            get
            {
                List<SelectListItem> result;
                using (var s = new SIM_Entities())
                {
                    result = s.mKlp.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Klp,
                        Value = x.Klp,
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListKelasPertanggungan
        {
            get
            {
                var result = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    result = s.SIMmKelas.Where(x => x.Active == true).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.NamaKelas,
                        Value = x.KelasID,
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListKelasPertanggunganTarif
        {
            get
            {
                var result = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    result = s.SIMmKelas.Where(x => x.Active == true).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.NamaKelas,
                        Value = x.KelasID,
                        Selected = x.KelasID == "xx"
                    });
                }
                return result;
            }
        }


        public static List<SelectListItem> SelectListGender
        {
            get
            {
                return new List<SelectListItem>() {
                    new SelectListItem() { Text = "", Value = ""},
                    new SelectListItem() { Text = "Laki - Laki", Value = "M"},
                    new SelectListItem() { Text = "Perempuan", Value = "F"},
                    new SelectListItem() { Text = "Lainnya", Value = "O"}
                };
            }
        }

        public static List<SelectListItem> SelectListGolda
        {
            get
            {
                var result = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    result = s.mGolda.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama,
                        Value = x.ID,
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListStatusKawin
        {
            get
            {
                var result = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    result = s.mStatusKawin.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama,
                        Value = x.ID,
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListProvinsi
        {
            get
            {
                List<SelectListItem> result;

                using (var s = new SIM_Entities())
                {
                    result = s.mPropinsi.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama_Propinsi,
                        Value = x.Kode_Propinsi
                    });
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListPekerjaan
        {
            get
            {
                List<SelectListItem> result;

                using (var s = new SIM_Entities())
                {
                    result = s.SIMmPekerjaan.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.NamaPekerjaan,
                        Value = x.NamaPekerjaan,
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListStatus
        {
            get
            {
                List<SelectListItem> result;

                using (var s = new SIM_Entities())
                {
                    result = s.SIMmStatus.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Status,
                        Value = x.Status
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListPendidikan
        {
            get
            {
                List<SelectListItem> result;

                using (var s = new SIM_Entities())
                {
                    result = s.SIMmPendidikan.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Pendidikan,
                        Value = x.Pendidikan
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListCaraDatangPertama
        {
            get
            {
                List<SelectListItem> result;

                using (var s = new SIM_Entities())
                {
                    result = s.mCaraDatangPertama.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.CaraDatangPertama,
                        Value = x.CaraDatangPertama
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListAgama
        {
            get
            {
                List<SelectListItem> result;

                using (var s = new SIM_Entities())
                {
                    result = s.mAgama.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Agama,
                        Value = x.AgamaID,
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListDokterPertama
        {
            get
            {
                List<SelectListItem> result;

                using (var s = new SIM_Entities())
                {
                    result = s.mSupplier.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Nama_Supplier,
                        Value = x.Kode_Supplier
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListDokter
        {
            get
            {
                List<SelectListItem> result;

                using (var s = new SIM_Entities())
                {
                    result = s.mDokter.Where(x => x.Active == true).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.NamaDOkter,
                        Value = x.DokterID
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListPenanggungHubungan
        {
            get
            {
                List<SelectListItem> result;

                using (var s = new SIM_Entities())
                {
                    result = s.SIMmPenanggungHubungan.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.PenanggungHubungan,
                        Value = x.PenanggungHubungan,
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListKategoriPasienBermasalah
        {
            get
            {
                List<SelectListItem> result;

                using (var s = new SIM_Entities())
                {
                    result = s.SIMmKategoriPasienBermasalah.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.KategoriMasalah,
                        Value = x.KategoriMasalah,
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListKategoriVendor
        {
            get
            {
                List<SelectListItem> result;

                using (var s = new SIM_Entities())
                {
                    result = s.mKategori_Vendor.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Kategori_Name,
                        Value = x.Kode_Kategori,
                        Selected = x.Kode_Kategori == "V-023"
                    });
                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListTipePelayanan
        {
            get
            {
                return new List<SelectListItem>() {
                    new SelectListItem() { Text = "ALL", Value = "ALL"},
                    new SelectListItem() { Text = "Rawat Jalan", Value = "RJ", Selected = true},
                    new SelectListItem() { Text = "Rawat Inap", Value = "RI"}
                };
            }
        }

        public static List<SelectListItem> SelectListHari
        {
            get
            {
                return new List<SelectListItem>() {
                    new SelectListItem() { Text = "", Value = ""},
                    new SelectListItem() { Text = "Senin", Value = "Senin"},
                    new SelectListItem() { Text = "Selasa", Value = "Selasa"},
                    new SelectListItem() { Text = "Rabu", Value = "Rabu"},
                    new SelectListItem() { Text = "Kamis", Value = "Kamis"},
                    new SelectListItem() { Text = "Jumat", Value = "Jumat"},
                    new SelectListItem() { Text = "Sabtu", Value = "Sabtu"},
                    new SelectListItem() { Text = "Minggu", Value = "Minggu"}
                };
            }
        }

        public static List<SelectListItem> ListSectionRI
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var m = s.SIMmSection.Where(x => x.RIOnly == true).ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID.ToString(),
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSection
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    var h = s.Pelayanan_TipePelayanan_Get_New.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.NamaPelayanan,
                        Value = x.TipePelayanan
                    });
                    var m = s.SIMmSection.Where(x => x.StatusAktif == true).ToList();
                    foreach (var group in h)
                    {
                        var optionGroup = new SelectListGroup() { Name = group.Text };
                        foreach (var item in m.Where(x => x.TipePelayanan == group.Value).ToList())
                        {
                            r.Add(new SelectListItem()
                            {
                                Value = item.SectionID.ToString(),
                                Text = item.SectionName,
                                Group = optionGroup
                            });
                        }
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> SelectListSection
        {
            get
            {
                var result = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    result = s.SIMmSection.Where(x => x.StatusAktif == true && (x.TipePelayanan == "RJ" || x.TipePelayanan == "RI" || x.TipePelayanan == "Penunjang" || x.TipePelayanan == "Penunjang2") && x.RIOnly == false).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.SectionName,
                        Value = x.SectionID,
                    });

                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListSectionAll
        {
            get
            {
                var result = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    result = s.SIMmSection.Where(x => x.StatusAktif == true).ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.SectionName,
                        Value = x.SectionID,
                    });

                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListSectionRegistrasi
        {
            get
            {
                var result = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    result = s.SIMmSection.Where(x => x.StatusAktif == true && x.TipePelayanan == "REGISTRASI").ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.SectionName,
                        Value = x.SectionID,
                    });
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListKategori
        {
            get
            {
                var result = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    result = s.SIMmKategoriOperasi.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.KategoriName,
                        Value = x.KategoriID.ToString(),
                        Selected = x.KategoriID == 1
                    });
                }
                return result;
            }
        }

        public static List<SelectListItem> SelectListCaraDatang
        {
            get
            {
                var result = new List<SelectListItem>();
                using (var s = new SIM_Entities())
                {
                    result = s.mCaraDatangPertama.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.CaraDatangPertama,
                        Value = x.CaraDatangPertama,
                    });

                    result.Insert(0, new SelectListItem());
                }
                return result;
            }
        }

        //REPORT
        public static List<SelectListItem> paramReportSection
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIM_Entities())
                {
                    r = s.SIMmSection.Where(e => e.TipePelayanan == "RI" || e.TipePelayanan == "RJ" || e.TipePelayanan == "PENUNJANG" || e.TipePelayanan == "PENUNJANG2").ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.SectionName,
                        Value = x.SectionID
                    });
                }
                return r;
            }
        }

        public static List<SelectListItem> ListStatusKamar
        {
            get
            {
                var r = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "Avalaible", Value = "AV"},
                    //new SelectListItem(){ Text = "Booking", Value = "BO"},
                    new SelectListItem(){ Text = "Cleaning", Value = "CL"},
                    //new SelectListItem(){ Text = "Check Out", Value = "CO"},
                    new SelectListItem(){ Text = "Filled", Value = "FD"},
                    new SelectListItem(){ Text = "Renovation", Value = "RV"},
                    new SelectListItem(){ Text = "Vacant Dirty", Value = "VD"},
                };
                return r;
            }
        }

    }
}