﻿using iHos.MVC.Master;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Registrasi.Models
{
    public class SqlCon_SIM : MasterSqlCon
    {
        public SqlCon_SIM(string sp_name, string typeTable_name, DataTable table)
        {
            ConString = ConfigurationManager.ConnectionStrings["SIMConnection"].ConnectionString;
            SP_Name = sp_name;
            TypeTable_Name = typeTable_name;
            Table = table;
        }

        public SqlCon_SIM()
        {
            ConString = ConfigurationManager.ConnectionStrings["SIMConnection"].ConnectionString;
        }
    }
}