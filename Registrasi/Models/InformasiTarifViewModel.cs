﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Registrasi.Models
{
    public class InformasiTarifViewModel
    {
        public Nullable<int> JenisPasienID { get; set; }
        public string TipePasien { get; set; }
        public string Kelas { get; set; }
        public bool PasienKTP { get; set; }
        public bool Cyto { get; set; }
        public string Ketegori { get; set; }
        public byte GroupJasaID { get; set; }
        public decimal ListHargaID { get; set; }
        public string JasaID { get; set; }
        public string JasaName { get; set; }
        public string KategoriJasaName { get; set; }
        public Nullable<decimal> Harga { get; set; }
        public string Harga_View { get; set; }
        [DataType(DataType.Date)]
        public DateTime TglBerlaku { get; set; }
    }
}