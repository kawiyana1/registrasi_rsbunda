//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Registrasi.Entities
{
    using System;
    
    public partial class GetDataObatUmum_Pelayanan_Result
    {
        public Nullable<int> Barang_ID { get; set; }
        public string Kode_Barang { get; set; }
        public string Nama_Barang { get; set; }
        public string Satuan { get; set; }
        public string SubKategori { get; set; }
        public Nullable<double> Stok { get; set; }
        public string JenisBarang { get; set; }
        public Nullable<int> Kategori_ID { get; set; }
        public string SatuanBeli { get; set; }
        public Nullable<int> Konversi { get; set; }
    }
}
