//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Registrasi.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class SIMtrMemo
    {
        public string NOReg { get; set; }
        public string NoUrut { get; set; }
        public System.DateTime Tanggal { get; set; }
        public System.DateTime Jam { get; set; }
        public string SectionID { get; set; }
        public string Memo { get; set; }
        public short User_ID { get; set; }
    }
}
