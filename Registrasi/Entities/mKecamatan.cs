//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Registrasi.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class mKecamatan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public mKecamatan()
        {
            this.mDesa = new HashSet<mDesa>();
        }
    
        public string KecamatanID { get; set; }
        public string KecamatanNama { get; set; }
        public string KabupatenID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<mDesa> mDesa { get; set; }
        public virtual mKabupaten mKabupaten { get; set; }
    }
}
