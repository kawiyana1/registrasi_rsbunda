//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Registrasi.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Registrasi_GetListTujuan
    {
        public string SectionID { get; set; }
        public string DokterID { get; set; }
        public Nullable<int> WaktuID { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string NamaSection { get; set; }
        public string NamaDokter { get; set; }
        public string NamaWaktu { get; set; }
        public string NoReg { get; set; }
    }
}
