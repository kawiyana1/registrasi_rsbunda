//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Registrasi.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class SIMtrReservasi
    {
        public string NoReservasi { get; set; }
        public System.DateTime Tanggal { get; set; }
        public System.DateTime Jam { get; set; }
        public bool PasienBaru { get; set; }
        public string NRM { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public string Phone { get; set; }
        public string UntukSectionID { get; set; }
        public string UntukDokterID { get; set; }
        public string UntukHari { get; set; }
        public Nullable<System.DateTime> UntukTanggal { get; set; }
        public Nullable<System.DateTime> UntukJam { get; set; }
        public int NoUrut { get; set; }
        public short User_ID { get; set; }
        public bool Registrasi { get; set; }
        public Nullable<double> JmlAntrian { get; set; }
        public Nullable<int> WaktuID { get; set; }
        public Nullable<int> JenisKerjasamaID { get; set; }
        public Nullable<System.DateTime> TanggalLahir { get; set; }
        public string Memo { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> TglPerkiraan_1 { get; set; }
        public Nullable<System.DateTime> TglPerkiraan_2 { get; set; }
        public Nullable<bool> Tindakan_SC { get; set; }
        public Nullable<bool> Tindakan_Normal { get; set; }
        public string KelasID { get; set; }
        public Nullable<decimal> Deposit { get; set; }
        public string PermintaanKhusus { get; set; }
        public Nullable<bool> Batal { get; set; }
        public Nullable<bool> Paid { get; set; }
        public string PaidNoBukti { get; set; }
        public Nullable<short> UserIDPaid { get; set; }
        public string TipeReservasi { get; set; }
        public string NamaSuami { get; set; }
        public string KodeCUstomerIKS { get; set; }
        public string JasaID { get; set; }
        public string NRM_AKUN { get; set; }
        public string MobileUserId { get; set; }
        public string AlasanBatal { get; set; }
        public string HaiMedRelasiId { get; set; }
        public string HaiMedUserId { get; set; }
        public string ReservasiBy { get; set; }
        public string KeteranganBatal { get; set; }
        public string NoAnggotaIKS { get; set; }
        public Nullable<bool> MobileKeteranganPasienBaru { get; set; }
        public string MobileKeteranganNRM { get; set; }
        public string MobileKeteranganNoAnggota { get; set; }
        public string MobileKeteranganNoRujukan { get; set; }
        public Nullable<System.DateTime> MobileTglLahirPasien { get; set; }
        public Nullable<bool> MobileStatusSudahDiperiksa { get; set; }
        public Nullable<bool> MobileNotifikasiAktif { get; set; }
        public string NoRujukanBPJS { get; set; }
        public string MobileKeteranganKerjasama { get; set; }
        public string NoNIK { get; set; }
        public Nullable<int> JenisKunjungan { get; set; }
        public Nullable<bool> Checkin { get; set; }
        public string BpjsKeteranganNRM { get; set; }
    
        public virtual SIMmSection SIMmSection { get; set; }
    }
}
