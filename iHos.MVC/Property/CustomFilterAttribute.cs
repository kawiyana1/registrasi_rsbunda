﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iHos.MVC.Property
{
    public class CustomFilterAttribute : Attribute
    {
        public IEnum.Comparison Comparison { get; set; }

        public CustomFilterAttribute(IEnum.Comparison comparison)
        {
            Comparison = comparison;
        }
    }
}
